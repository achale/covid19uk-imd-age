# covid19uk-imd-age: Bayesian stochastic apporach for infectious disease modelling

This Python package implements a deprivation-age metapopulation appraoch using a stochastic infectious disease SEIR model: it uses positive case data, population data, and social mixing data.



## Workflow
This repository contains code that produces Monte Carlo samples of the Bayesian posterior distribution given the model and case timeseries data and associated inference and prediction parameters.

Users requiring an end-to-end pipeline implementation should refer to the example `HEC.sh` shell along with `config.yaml`.

For development users, the recommended package management system is [`poetry`](https://python-poetry.org).  Follow the instructions in the `poetry` documentation to install it.  

From a `bash`-like command line, clone the `imd-age-covid19uk` repo and install dependencies:
```bash
git clone <path to this repo>
cd covid19uk-imd-age
poetry install
```

To run the various algorithms, a general configuration file must be specified, as exemplified in `demo_config.yaml`.  The configuration file specifies the location of raw data (e.g. `data/demo_case_data.csv`), which we assemble into a NetCDF4 file:
```bash
mkdir demo_results
poetry run python -m covid19uk.data.assemble demo_config.yaml demo_results/inferencedata.nc
```

The inference algorithm may then be run using the assembled data:
```bash
poetry run python -m covid19uk.inference.inference \
    -c demo_config.yaml \
    -o demo_results/posterior.hd5 \
    demo_results/inferencedata.nc
```
The resulting HDF5 file `demo_results/posterior.hd5` contains the posterior samples.
When `data/demo_case_data.csv` is used to construct `inferencedata.nc` then the inference algorithm takes around 5 to 15 minutes to run when using `demo_config.yaml`.
This is only intended to demonstrate the workflow of this package hence `demo_config.yaml` defines a very short run with only 5000 samples which is insufficient for the mcmc to burnin.
See `config.yaml` for a more realistic setup.
Ideally the "Acceptance move" and "Acceptance occult" rates should be around 0.3 to 0.4 and tuned using the "Mcmc" parameters in the yaml file: for example see `config.yaml` which has previously been used on 12 weeks of case data.


Thin the posterior hd5 file using `demo_config.yaml`:
```bash
poetry run python -m covid19uk.posterior.thin \
    -c demo_config.yaml \
    -o demo_results/thin_samples.pkl \
    demo_results/posterior.hd5
```
The resulting HDF5 file `demo_results/thin_samples.pkl` contains the posterior samples. 


## Postprocessing
We assume that a developer user will write their own code to analyse the raw data and posterior samples however the following functions are included as they may be of some assistance.


### Raw data
Compute the incidence of the raw data in `demo_case_data.csv`:
```bash
poetry run python -m covid19uk.data.daily_incidence \
    demo_config.yaml \
    demo_results/
```
The resulting `demo_results/counts.nc` file contains the daily incidence.


Compute the cumulative incidence of the raw data in `demo_case_data.csv`:
```bash
poetry run python -m covid19uk.data.cum_incidence \
    demo_config.yaml \
    demo_results/
```
The resulting `demo_results/cumsums.nc` file contains the daily cumulative sum.


Plot the raw data in `demo_case_data.csv`:
```bash
poetry run python -m covid19uk.posterior.plot_raw_data \
    demo_results/inferencedata.nc \
    demo_results/cumsums.nc \
    demo_results/counts.nc \
    demo_results/
```
The resulting `demo_results/plot_raw_..._.png` files give various views of the data.  Note that the grey background on the time series figures indicates the time range of the training data.


### Posterior samples
Compute the reproduction number:
```bash
poetry run python -m covid19uk.posterior.reproduction_number \
    demo_results/thin_samples.pkl \
    -d demo_results/inferencedata.nc \
    -o demo_results/reproduction_number.nc
```
The resulting `demo_results/reproduction_number.nc` file contains the estimated daily reproduction number per metapopulation.


Forecast the case incidence per metapopulation:
```bash
poetry run python -m covid19uk.posterior.predict \
    -i -1 \
    -n 30 \
    -o \
    demo_results/inferencedata.nc \
    demo_results/thin_samples.pkl \
    demo_results/medium_term.nc
```
The resulting `demo_results/medium_term.nc` file contains estimates of a 30-day forecast for each metapopulation.


Insample predictions per metapopulation:
```bash
poetry run python -m covid19uk.posterior.predict -i -7 -n 28 demo_results/inferencedata.nc demo_results/thin_samples.pkl demo_results/insample7.nc
poetry run python -m covid19uk.posterior.predict -i -14 -n 28 demo_results/inferencedata.nc demo_results/thin_samples.pkl demo_results/insample14.nc
poetry run python -m covid19uk.posterior.predict -i -28 -n 30 demo_results/inferencedata.nc demo_results/thin_samples.pkl demo_results/insample28.nc
poetry run python -m covid19uk.posterior.predict -i 0 -n 0 demo_results/inferencedata.nc demo_results/thin_samples.pkl demo_results/insampleAll.nc
```
The resulting `demo_results/insampleX.nc` files contain in-sample estimates for time lengths of 7, 14, 28 and "all" days, where "all" refers to full time period for which there are predictions.
Note that the `demo_results/inferencedata.nc` file only contains 31 days of training data.


Plot the posterior estimates:
```bash
poetry run python -m covid19uk.posterior.plot_fitted_results \
    demo_config.yaml \
    demo_results/inferencedata.nc \
    demo_results/insample7.nc \
    demo_results/insample14.nc \
    demo_results/insample28.nc \
    demo_results/insampleAll.nc \
    demo_results/medium_term.nc \
    demo_results/reproduction_number.nc \
    demo_results/ \
    demo_results/posterior.hd5 \
    Yes
```
This results in a number of `demo_results/plot_fitted_..._.png` graphics files.
Figures with both left and right panels are constructed such that for each metapopulation the left panel is NOT normalised by 100K population whereas the right panel is normalised by 100K population.
Note that `plot_fitted_data_chi.png` is the estimate of the chi term from the model and `plot_fitted_data_RPS.png` is an estimate of the continuous ranked probability score.


### Diagnostics
For MCMC diagnostic purposes the posterior samples may be plotted:
```bash
poetry run python -m covid19uk.posterior.plot_samples \
    demo_config.yaml \
    0 \
    0 \
    0 \
    demo_results/posterior.hd5 \
    "demo_results/"
poetry run python -m covid19uk.posterior.plot_samples_rho_and_psi \
    demo_config.yaml \
    demo_config.yaml \
    0 \
    0 \
    0 \
    demo_results/posterior.hd5 \
    demo_results/ \
    rho
poetry run python -m covid19uk.posterior.plot_samples_rho_and_psi \
    demo_config.yaml \
    demo_config.yaml \
    0 \
    0 \
    0 \
    demo_results/posterior.hd5 \
    demo_results/ \
    psi
```
The resulting `demo_results/diagnostic_..._.png` files contain various views of the posterior samples which may aid mcmc chain diagnostics.
Note that for the purposes of demonstrating the package using `demo_config.yaml` too few samples were computed as a result the mcmc run is too short to reliably get beyond burnin.



## Lancaster University data statement
__Data contained in the data directory is all publicly available from UK government agencies or previous studies.
No personally identifiable information is stored.__


### Example data files
* The `data/England_imd_decile_age_population_0-70plus.csv` file derives from the UK 2019 Office for National Statistics population estimates.
* The `POLYMOD` survey data was used to estimate the age-structured social mixing matrix with the R-package `socialmixr` (https://cran.r-project.org/web/packages/socialmixr/) using R-code `data/socialmixr.r`. The resulting dataset is in the file `data/socialmixr.csv`.
* The case data `data/demo_case_data.csv` was constructed for demonstrating this package as such it does not correspond to any observed data.
