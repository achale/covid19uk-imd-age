#!/bin/bash

#$ -S /bin/bash
#$ -cwd
#$ -j y  # join stderr and stdout
#$ -q short
#$ -l ngpus=1
#$ -l ncpus=4
#$ -l h_vmem=64G
#$ -l h_rt=07:00:00

. /etc/profile

# . $HOME/.bashrc
. $HOME/.bash_profile

module add cuda

rm -f results/inferencedata.nc
rm -f results/posterior.hd5
rm -f results/thin_samples.pkl
rm -f results/reproduction_number.nc
rm -f results/medium_term.nc
rm -f results/insample7.nc
rm -f results/insample14.nc
rm -f results/insample28.nc
rm -f results/insampleAll.nc
rm -f -r results/data
rm -f -r results/js
rm -f results/*.png


poetry run python -m covid19uk.data.assemble \
    config.yaml \
    results/inferencedata.nc

poetry run python -m covid19uk.data.cum_incidence \
    config.yaml \
    results/

poetry run python -m covid19uk.data.daily_incidence \
    config.yaml \
    results/

poetry run python -m covid19uk.inference.inference \
    -c config.yaml \
    -o results/posterior.hd5 \
    results/inferencedata.nc

poetry run python -m covid19uk.posterior.thin \
    -c config.yaml \
    -o results/thin_samples.pkl \
    results/posterior.hd5

poetry run python -m covid19uk.posterior.reproduction_number \
    results/thin_samples.pkl \
    -d results/inferencedata.nc \
    -o results/reproduction_number.nc

poetry run python -m covid19uk.posterior.predict \
    -i -1 \
    -n 84 \
    -o \
    results/inferencedata.nc \
    results/thin_samples.pkl \
    results/medium_term.nc

poetry run python -m covid19uk.posterior.predict \
    -i -7 \
    -n 28 \
    results/inferencedata.nc \
    results/thin_samples.pkl \
    results/insample7.nc

poetry run python -m covid19uk.posterior.predict \
    -i -14 \
    -n 28 \
    results/inferencedata.nc \
    results/thin_samples.pkl \
    results/insample14.nc

poetry run python -m covid19uk.posterior.predict \
    -i -28 \
    -n 56 \
    results/inferencedata.nc \
    results/thin_samples.pkl \
    results/insample28.nc

poetry run python -m covid19uk.posterior.predict \
    -i 0 \
    -n 0 \
    results/inferencedata.nc \
    results/thin_samples.pkl \
    results/insampleAll.nc

poetry run python -m covid19uk.posterior.make_dha \
    results/inferencedata.nc \
    results/insample7.nc \
    results/insample14.nc \
    results/insample28.nc \
    results/insampleAll.nc \
    results/medium_term.nc \
    results/reproduction_number.nc \
    "results/" \
    8 \
    0.05 \
    0.95 \
    "" \
    ""

poetry run python -m covid19uk.data.cum_incidence \
    config.yaml \
    results/

poetry run python -m covid19uk.posterior.plot_raw_data \
    results/inferencedata.nc \
    results/cumsums.nc \
    results/counts.nc \
    results/

poetry run python -m covid19uk.posterior.plot_fitted_results \
    config.yaml \
    results/inferencedata.nc \
    results/insample7.nc \
    results/insample14.nc \
    results/insample28.nc \
    results/insampleAll.nc \
    results/medium_term.nc \
    results/reproduction_number.nc \
    results/ \
    results/posterior.hd5 \
    Yes

poetry run python -m covid19uk.posterior.plot_samples \
    config.yaml \
    0 \
    0 \
    0 \
    results/posterior.hd5 \
    "results/"

poetry run python -m covid19uk.posterior.plot_samples_rho_and_psi \
    config.yaml \
    config.yaml \
    0 \
    0 \
    0 \
    results/posterior.hd5 \
    results/ \
    rho

poetry run python -m covid19uk.posterior.plot_samples_rho_and_psi \
    config.yaml \
    config.yaml \
    0 \
    0 \
    0 \
    results/posterior.hd5 \
    results/ \
    psi

poetry run python -m covid19uk.posterior.plot_samples \
    none \
    0 \
    1 \
    50 \
    results/posterior.hd5 \
    "results/"

poetry run python -m covid19uk.posterior.plot_samples_rho_and_psi \
    config.yaml \
    none \
    0 \
    1 \
    50 \
    results/posterior.hd5 \
    results/ \
    rho

poetry run python -m covid19uk.posterior.plot_samples_rho_and_psi \
    config.yaml \
    none \
    0 \
    1 \
    50 \
    results/posterior.hd5 \
    results/ \
    psi



