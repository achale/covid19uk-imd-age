""" Simlates imd switch using given model and previously fitted parameters """

from datetime import timedelta
import matplotlib.pylab as pylab
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pickle as pkl
import tensorflow as tf
import xarray

from covid19uk import model_spec
from covid19uk.data.util import imdage_idx
from covid19uk.data.util import imdage_idx_names
from covid19uk.posterior.make_dha import xarray2summarydf
from covid19uk.posterior.plot_fitted_results import df_split_index
from covid19uk.posterior.plot_raw_data import make_timeseries
from covid19uk.posterior.predict import predicted_incidence
from covid19uk.util import copy_nc_attrs

from gemlib.util import compute_state


params = {
    "figure.figsize": (14, 6),
    "legend.loc": "best",
}
pylab.rcParams.update(params)


def plot_simulations(
    simulation_file, origin_date_str, N_xarray, output_folder, bias, log
):
    #
    # incidence per 100k
    #
    sim = xarray.open_dataset(simulation_file, group="predictions")
    sim = sim.rename({"age_imd_coords": "age_imd"})
    sim_df = xarray2summarydf(sim["events"][..., 2].reset_coords(drop=True))
    sim_df = df_split_index(sim_df)

    N = imdage_idx(N_xarray)
    N = N.to_dataframe().reset_index()
    N_xarray = N.set_index(["age_group", "imd_decile"]).to_xarray()["N"]

    d_incidence_ts = (
        sim_df[sim_df["time"] >= origin_date_str]
        .reset_index()
        .set_index(["age_group", "imd_decile", "time"])
    )
    d_incidence_ts = d_incidence_ts[["value"]].to_xarray()
    print(
        "Date range of simulations: "
        + str(min(d_incidence_ts.coords["time"].values))
        + " - "
        + str(max(d_incidence_ts.coords["time"].values))
    )
    d_incidence_ts_age = (
        d_incidence_ts.sum(dim="imd_decile")["value"]
        / N_xarray.sum(dim="imd_decile")
        * 1e5
    )
    d_incidence_ts_imd = (
        d_incidence_ts.sum(dim="age_group")["value"]
        / N_xarray.sum(dim="age_group")
        * 1e5
    )

    fig, ax = plt.subplots(ncols=2)
    make_timeseries(
        d_incidence_ts_age,
        "age_group",
        ax,
        0,
        log,  # title="daily case count"
    )
    make_timeseries(
        d_incidence_ts_imd,
        "imd_decile",
        ax,
        1,
        log,  # title="daily case count"
    )
    fig.tight_layout()
    fig.autofmt_xdate()
    plt.savefig(
        output_folder
        + "plot_simulated_timeseries_incidence_100k_bias_"
        + str(bias)
        + ".png"
    )
    plt.show()
    plt.close(fig)


def read_pkl(filename):
    with open(filename, "rb") as f:
        return pkl.load(f)


def simulate(
    data,
    posterior_samples,
    cumsums_file,
    output_file,
    initial_step,
    num_steps,
    stratum_size,
    case_size,
    output_folder,
    bias,
    log,
    recovered_size,
    out_of_sample=False,
):

    # construct covariate data
    cumsum_age_imd = xarray.open_dataset(cumsums_file, group="cumsum_age_imd")[
        "cumsum_age_imd"
    ].fillna(0)
    cases_org = xarray.open_dataset(data, group="observations")["cases"]
    origin_date = np.array(cases_org.coords["time"][initial_step])
    origin_date_str = str(np.datetime_as_string(origin_date, unit="D"))
    dates = np.arange(
        origin_date,
        origin_date + np.timedelta64(num_steps, "D"),
        np.timedelta64(1, "D"),
    )
    weekday = 1.0 * (
        (pd.date_range(start=min(dates), end=max(dates)).weekday < 5).astype(
            model_spec.DTYPE
        )
    )
    print("origin_date", origin_date_str)
    print("date range", min(dates), max(dates))

    covar_data_org = xarray.open_dataset(data, group="constant_data")
    dim_imd = covar_data_org.coords["imd_decile"].shape[0]
    dim_age = covar_data_org.coords["age_group"].shape[0]
    age_group_coords = np.array(
        covar_data_org.coords["age_group"].values
    ).astype(object)

    age_group = np.repeat(age_group_coords, dim_imd)
    imd_decile = np.tile(range(1, dim_imd + 1), dim_age)

    covar_weekday = xarray.DataArray(
        data=weekday,
        dims=["prediction_time"],
        coords=[dates],
        name="weekday",
    )

    covar_C = covar_data_org["C"].to_dataset()
    # covar_C = xarray.DataArray(data=np.ones((dim_age, dim_age)),dims=["age_group", "contact_age_group"],coords={"age_group": age_group_coords,"contact_age_group": age_group_coords,},name="C",)

    if stratum_size == 0:
        covar_N = covar_data_org["N"].to_dataset()
    else:
        covar_N = (
            pd.DataFrame(
                {
                    "imd_decile": imd_decile,
                    "age_group": age_group,
                    "N": np.ones(dim_imd * dim_age) * stratum_size,
                }
            )
            .set_index(["imd_decile", "age_group"])
            .to_xarray()
        )

    covar_data = xarray.merge([covar_C, covar_N, covar_weekday])
    covar_data_ = {
        "C": covar_data["C"],
        "N": imdage_idx(covar_data["N"]),
        "weekday": covar_data["weekday"],
        "IMD": covar_data.coords["imd_decile"].astype(model_spec.DTYPE),
    }

    # use samples generated by the given model
    samples = read_pkl(posterior_samples)

    initial_state_orig = samples["initial_state"]
    del samples["initial_state"]

    if bias == 1:  # highest current incidence imd==10
        init_imd = np.tile(
            range(case_size, (dim_imd + 1) * case_size, case_size), dim_age
        )
    if bias == 2:  # highest current incidence imd==1
        init_imd = np.tile(range(dim_imd * case_size, 0, -case_size), dim_age)

    if recovered_size > 0:
        if bias == 1:  # highest recovered in imd 10
            recover_imd = np.tile(
                range(
                    recovered_size,
                    (dim_imd + 1) * recovered_size,
                    recovered_size,
                ),
                dim_age,
            )
        if bias == 2:  # highest recovered in imd 1
            recover_imd = np.tile(
                range(
                    dim_imd * recovered_size,
                    0,
                    -recovered_size,
                ),
                dim_age,
            )
        print("recover_imd", recover_imd)
    elif recovered_size == 0:
        recover_imd = np.zeros(dim_age * dim_imd)
    elif recovered_size == -1:
        # imdage_idx(cumsum_age_imd.sel(time=origin_date_str))
        recover_imd = (
            imdage_idx(cumsum_age_imd.sel(time=origin_date_str))
            .to_dataframe()
            .reset_index()["cumsum_age_imd"]
            .to_numpy()
        )

    if recovered_size >= 0:
        initial_state = np.array(
            [
                np.ones(dim_age * dim_imd) * (stratum_size - 2 * init_imd)
                - recover_imd,
                np.ones(dim_age * dim_imd) * init_imd,
                np.ones(dim_age * dim_imd) * init_imd,
                recover_imd,
            ]
        ).transpose()
    elif recovered_size == -1:
        print(initial_state_orig[:, 0])
        initial_state = np.array(
            [
                initial_state_orig[:, 0] - recover_imd,
                initial_state_orig[:, 1],
                initial_state_orig[:, 2],
                initial_state_orig[:, 3] + recover_imd,
            ]
        ).transpose()

    with tf.device("CPU"):
        estimated_init_state, predicted_events = predicted_incidence(
            samples,
            initial_state,
            covar_data_,
            initial_step,
            num_steps,
            out_of_sample,
        )
    age_imd_coords = imdage_idx_names(covar_data)

    prediction = xarray.DataArray(
        predicted_events.numpy(),
        coords=[
            np.arange(predicted_events.shape[0]),
            age_imd_coords,
            dates,  # dates[initial_step:],
            np.arange(predicted_events.shape[3]),
        ],
        dims=("iteration", "age_imd_coords", "time", "event"),
    )
    estimated_init_state = xarray.DataArray(
        estimated_init_state.numpy(),
        coords=[
            np.arange(estimated_init_state.shape[0]),
            age_imd_coords,
            np.arange(estimated_init_state.shape[-1]),
        ],
        dims=("iteration", "age_imd_coords", "state"),
    )
    ds = xarray.Dataset(
        {"events": prediction, "initial_state": estimated_init_state}
    )
    ds.to_netcdf(output_file, group="predictions")
    ds.close()
    copy_nc_attrs(data, output_file)

    plot_simulations(
        output_file, origin_date_str, covar_N, output_folder, bias, log
    )


if __name__ == "__main__":

    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument(
        "-i", "--initial-step", type=int, default=0, help="Initial step"
    )
    parser.add_argument(
        "-n", "--num-steps", type=int, default=1, help="Number of steps"
    )
    parser.add_argument(
        "-o",
        "--out-of-sample",
        action="store_true",
        help="Out of sample prediction (sample alpha_t)",
    )
    parser.add_argument("data_pkl", type=str, help="Covariate data nc")
    parser.add_argument(
        "cumsums_file",
        type=str,
        help="cumsums nc",
    )
    parser.add_argument(
        "posterior_samples_pkl",
        type=str,
        help="Posterior samples pickle",
    )
    parser.add_argument(
        "output_file",
        type=str,
        help="Output pkl file",
    )
    parser.add_argument(
        "output_folder",
        type=str,
        help="Output folder for figure",
    )
    parser.add_argument(
        "stratum_size", type=int, default=1000, help="stratum_size"
    )
    parser.add_argument(
        "recovered_size",
        type=int,
        default=0,
        help="number of recovered individuals",
    )
    parser.add_argument("case_size", type=int, default=1, help="case_size")
    parser.add_argument(
        "log",
        type=str,
        help="log or lin scale for figs",
    )
    parser.add_argument(
        "bias",
        type=int,
        default=1,
        help="imd bias towards low or high IMD deciles",
    )
    args = parser.parse_args()

    simulate(
        args.data_pkl,
        args.posterior_samples_pkl,
        args.cumsums_file,
        args.output_file,
        args.initial_step,
        args.num_steps,
        args.stratum_size,
        args.case_size,
        args.output_folder,
        args.bias,
        args.log,
        args.recovered_size,
        args.out_of_sample,
    )


"""
### Example of switching imd 10 from low to high through depletion of susceptibles
### note if there's no depletion of susceptibles this effect is not seen
poetry run python -m covid19uk.posterior.simulate_imd_switch \
    -i 0 \
    -n 56 \
    -o \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/inferencedata.nc \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/cumsums.nc \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/thin_samples.pkl \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/simulation.nc \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/ \
    800000 \
    20000 \
    15 \
    lin \
    2
"""
