import xarray
import matplotlib.pyplot as plt

cases = xarray.open_dataset("inferencedata.nc", group="observations")["cases"]
dd = cases.to_dataframe()

plt.plot(dd.loc[("60-69", 5), :], "-o")
plt.show()
