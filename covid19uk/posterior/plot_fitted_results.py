import datetime
import h5py
import importlib
import matplotlib.colors as clr
import matplotlib.pylab as pylab
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xarray

from pathlib import Path

from covid19uk.data.util import imdage_idx
from covid19uk.posterior.make_dha import crps_wrapper
from covid19uk.posterior.make_dha import xarray2summarydf
from covid19uk.posterior.plot_raw_data import make_choropleth
from covid19uk.posterior.plot_raw_data import make_timeseries


params = {
    "figure.figsize": (14, 6),
    "legend.loc": "best",
    "font.size": "12",  # comment out for supp mat
}
pylab.rcParams.update(params)


def to_100k_pop(data, N, group, thing):
    return data.sum(dim=group)[thing] / N.sum(dim=group) * 1e5


def plot_CI(
    lower, upper, thing, ax, ax_id, color, alpha, smoothed="smoothed_0"
):
    if smoothed != "smoothed_0":
        window_length = int(smoothed.split("_")[1])
        lower = (
            lower.rolling(time=window_length, center=True).mean().dropna("time")
        )
        upper = (
            upper.rolling(time=window_length, center=True).mean().dropna("time")
        )

    keys = lower.coords[thing].values
    for i in range(len(keys)):
        xx = lower.loc[{thing: keys[i]}]
        yy = upper.loc[{thing: keys[i]}]
        xx_df = xx.to_dataframe(name="low").drop([thing], axis=1)
        yy_df = yy.to_dataframe(name="upp").drop([thing], axis=1)
        zz_df = pd.merge(
            xx_df, yy_df, left_index=True, right_index=True
        ).reset_index()
        ax[ax_id].fill_between(
            zz_df["time"],
            zz_df["low"],
            zz_df["upp"],
            alpha=alpha,
            color=color,
        )


def df_split_index(dd):
    dd[["age_group", "imd_decile"]] = dd["age_imd"].str.split(
        "_", n=1, expand=True
    )
    dd["imd_decile"] = dd["imd_decile"].astype(int)
    return dd.set_index(["age_group", "imd_decile"])


def plot_fitted_results(run_type, input_files, output_folder, samples, HEC):
    data = xarray.open_dataset(input_files[0], group="constant_data")  # covars
    age, _, imd, dates = data.indexes.values()
    ylist = list(range(0, -len(age), -1))
    first_forecast_date = max(dates) + datetime.timedelta(days=1)
    print("Date range of data: " + str(min(dates)) + " - " + str(max(dates)))
    N = imdage_idx(data["N"])
    N = N.to_dataframe().reset_index()
    N_xarray = N.set_index(["age_group", "imd_decile"]).to_xarray()["N"]

    #
    # Rt
    #
    r_it = xarray.open_dataset(input_files[4], group="posterior_predictive")[
        "R_it"
    ]
    print(
        "Date range of Rt: "
        + str(min(r_it.coords["time"].values))
        + " - "
        + str(max(r_it.coords["time"].values))
    )
    rt_summary = xarray2summarydf(r_it.isel(time=-1))
    d_rt = df_split_index(rt_summary)
    d_rt = d_rt[["value"]].to_xarray()["value"]
    d_rt["age_index"] = ("age_group", ylist)

    rt = r_it.isel(time=-1).drop("time")
    rt_exceed = np.mean(rt > 1.0, axis=0)
    rt_exceed = (
        rt_exceed.to_dataframe()
        .reset_index()
        .rename({"R_it": "value"}, axis="columns")
        .round({"value": 2})
    )
    d_rt_exceed = df_split_index(rt_exceed)
    d_rt_exceed = d_rt_exceed[["value"]].to_xarray()["value"]
    d_rt_exceed["age_index"] = ("age_group", ylist)

    fig, ax = plt.subplots(ncols=2)
    make_choropleth(
        d_rt,
        imd,
        age,
        dates,
        ylist,
        ax,
        0,
        "",  # "$R_t$ on " + rt_summary["time"].iloc[0].strftime("%Y-%m-%d"),
        cmap=plt.cm.get_cmap("Spectral").reversed(),
    )
    make_choropleth(
        d_rt_exceed,
        imd,
        age,
        dates,
        ylist,
        ax,
        1,
        "",  # "Pr($R_t$>1) on " + rt_summary["time"].iloc[0].strftime("%Y-%m-%d"),
        cmap=plt.cm.get_cmap("Spectral").reversed(),
        vmin=0.0,
        vmax=1.0,
    )
    ax[1].set_xticks(np.arange(0.5, len(imd), 1), minor=True)
    ax[1].set_yticks(np.arange(-0.5, -len(age), -1), minor=True)
    ax[1].grid(which="minor", color="#444444", linestyle="-", linewidth=0.4)
    fig.tight_layout()
    plt.savefig(output_folder + "plot_fitted_data_choropleth_Rt.png")
    plt.close(fig)

    #
    # incidence per 100k
    #
    medium_term = xarray.open_dataset(input_files[3], group="predictions")
    medium_term = medium_term.rename({"age_imd_coords": "age_imd"})
    medium_df = xarray2summarydf(
        medium_term["events"][..., 2].reset_coords(drop=True)
    )
    medium_df = df_split_index(medium_df)
    d_incidence = medium_df[medium_df["time"] == first_forecast_date]
    d_incidence = d_incidence[["value"]].to_xarray()["value"]
    d_incidence["age_index"] = ("age_group", ylist)

    d_incidence_ts = (
        medium_df[
            (medium_df["time"] >= first_forecast_date)
            & (
                medium_df["time"]
                < first_forecast_date + np.timedelta64(56, "D")
            )
        ]
        .reset_index()
        .set_index(["age_group", "imd_decile", "time"])
    )
    print("\n", first_forecast_date + np.timedelta64(56, "D"), "\n")
    print("\n", first_forecast_date, "\n")

    d_incidence_ts_mu = d_incidence_ts[["value"]].to_xarray()
    d_incidence_ts_low = d_incidence_ts[["0.05"]].to_xarray()
    d_incidence_ts_upp = d_incidence_ts[["0.95"]].to_xarray()
    print(
        "Date range of incidence forecast: "
        + str(min(d_incidence_ts_mu.coords["time"].values))
        + " - "
        + str(max(d_incidence_ts_mu.coords["time"].values))
    )
    d_incidence_ts_age = to_100k_pop(
        d_incidence_ts_mu, N_xarray, "imd_decile", "value"
    )
    d_incidence_ts_age_low = to_100k_pop(
        d_incidence_ts_low, N_xarray, "imd_decile", "0.05"
    )
    d_incidence_ts_age_upp = to_100k_pop(
        d_incidence_ts_upp, N_xarray, "imd_decile", "0.95"
    )

    d_incidence_ts_imd = to_100k_pop(
        d_incidence_ts_mu, N_xarray, "age_group", "value"
    )
    d_incidence_ts_imd_low = to_100k_pop(
        d_incidence_ts_low, N_xarray, "age_group", "0.05"
    )
    d_incidence_ts_imd_upp = to_100k_pop(
        d_incidence_ts_upp, N_xarray, "age_group", "0.95"
    )

    fig, ax = plt.subplots(ncols=2)
    make_timeseries(d_incidence_ts_age, "age_group", ax, 0)
    plot_CI(
        d_incidence_ts_age_low,
        d_incidence_ts_age_upp,
        "age_group",
        ax,
        0,
        "#777777",
        0.2,
    )
    make_timeseries(d_incidence_ts_imd, "imd_decile", ax, 1)
    plot_CI(
        d_incidence_ts_imd_low,
        d_incidence_ts_imd_upp,
        "imd_decile",
        ax,
        1,
        "#BBBBBB",
        0.3,
    )
    fig.tight_layout()
    plt.savefig(
        output_folder + "plot_fitted_data_timeseries_incidence_100k.png"
    )
    plt.close(fig)

    fig, ax = plt.subplots(ncols=2)
    vmin = 0
    vmax = 0
    # NULL model
    if HEC == "No":
        (
            index_ordering,
            crps_m_t,
            crps_m,
            crps_m_median,
            crps_t,
            crps,
        ) = crps_wrapper(
            (
                input_files[0].split("_")[0]
                + "_"
                + input_files[0].split("_")[1]
                + "_null/"
                + input_files[0].split("/")[-1],
                input_files[6].split("_")[0]
                + "_"
                + input_files[0].split("_")[1]
                + "_null/"
                + input_files[6].split("/")[-1],
            )
        )
        # RPS_m_t, RPS_m, RPS_t, RPS = ranked_prob_score((
        #        input_files[0].split("_")[0] + "_" + input_files[0].split("_")[1] + "_null/" + input_files[0].split("/")[-1],
        #        input_files[6].split("_")[0] + "_" + input_files[0].split("_")[1] + "_null/" + input_files[6].split("/")[-1], ))
        crps_m_df = pd.DataFrame(
            {"value": crps_m_median, "age_imd": index_ordering},
        )
        vmax = crps_m_df["value"].max()
        crps_m_df = df_split_index(crps_m_df).reset_index()
        crps_m_df = crps_m_df.sort_values(["age_group", "imd_decile"])
        crps_m_df = crps_m_df.set_index(["age_group", "imd_decile"])
        crps_m_df = crps_m_df[["value"]].to_xarray()["value"]
        crps_m_df["age_index"] = ("age_group", ylist)
        make_choropleth(
            crps_m_df,
            imd,
            age,
            dates,
            ylist,
            ax,
            0,
            "",  # "total crps = " + str(round(crps)),
            cmap=plt.cm.get_cmap("Purples").reversed(),
            vmin=vmin,
            vmax=vmax,
        )

    # Full model
    (
        index_ordering,
        crps_m_t,
        crps_m,
        crps_m_median,
        crps_t,
        crps,
    ) = crps_wrapper(
        (input_files[0], input_files[6])
    )  # RPS_m_t, RPS_m, RPS_t, RPS = ranked_prob_score((input_files[0], input_files[6]))
    crps_m_df = pd.DataFrame(
        {"value": crps_m_median, "age_imd": index_ordering},
    )
    vmax = vmax if crps_m_df["value"].max() < vmax else crps_m_df["value"].max()
    crps_m_df = df_split_index(crps_m_df).reset_index()
    crps_m_df = crps_m_df.sort_values(["age_group", "imd_decile"])
    crps_m_df = crps_m_df.set_index(["age_group", "imd_decile"])
    crps_m_df = crps_m_df[["value"]].to_xarray()["value"]
    crps_m_df["age_index"] = ("age_group", ylist)
    make_choropleth(
        crps_m_df,
        imd,
        age,
        dates,
        ylist,
        ax,
        1,
        "",  # "total crps = " + str(round(crps)),
        cmap=plt.cm.get_cmap("Purples").reversed(),
        vmin=vmin,
        vmax=vmax,
    )
    fig.tight_layout()
    plt.savefig(output_folder + "plot_fitted_data_RPS.png")
    plt.close(fig)
    latex_RPS = (
        str(round(np.quantile(crps_m_t, 0.25), 1))
        + " & "
        + str(round(np.quantile(crps_m_t, 0.5), 1))
        + " & "
        + str(round(np.quantile(crps_m_t, 0.75), 1))
        + " & "
        + str(round(crps, 1))
    )
    latex_RPS_file = open(output_folder + "latex_RPS_table_row.txt", "w")
    latex_RPS_file.write(latex_RPS)
    latex_RPS_file.close()
    print(latex_RPS)

    if run_type == "quickrun_config.yaml":
        burnin = 2000
        thin = 1
        alpha_t_time = 50
    elif run_type == "config.yaml":
        burnin = 20000
        thin = 30
        alpha_t_time = 50
    elif run_type == "demo_config.yaml":
        burnin = 2000
        thin = 3
        alpha_t_time = 15
    else:
        pass  # use parameters passed into function
    post = h5py.File(samples, "r")
    sample_keys = list(post["samples"].keys())
    print(sample_keys)
    param = {
        "rho": np.mean(post["samples/" + "rho"][burnin:, :], axis=0),
        "psi": np.mean(post["samples/" + "psi"][burnin:, :], axis=0),
    }
    if HEC == "No":
        loc1 = "results"
        loc2 = input_files[0].split("/")[1]
        loc3 = "model"
        loc4 = "model_spec"
        module = importlib.import_module(
            loc1 + "." + loc2 + "." + loc3 + "." + loc4
        )
    else:
        loc = "covid19uk.model_spec"
        module = importlib.import_module(loc)

    chi = module.chi_matrix(imd, age, param)
    chi_df = pd.DataFrame(
        {"value": chi},
        index=rt_exceed["age_imd"],  # same coords as rt_exceed
    ).reset_index()
    chi_df = df_split_index(chi_df)
    chi_df = chi_df[["value"]].to_xarray()["value"]
    chi_df["age_index"] = ("age_group", ylist)

    fig, ax = plt.subplots(ncols=2)
    cmap = clr.LinearSegmentedColormap.from_list(
        "custom blue", ["#5e4fa2", "#3b7cb7", "#84cea5", "#f5fbaf"], N=256
    )
    make_choropleth(
        chi_df,
        imd,
        age,
        dates,
        ylist,
        ax,
        0,
        "",
        cmap=cmap,
    )
    make_choropleth(
        chi_df / N_xarray,
        imd,
        age,
        dates,
        ylist,
        ax,
        1,
        "",
        cmap=cmap,
    )
    fig.tight_layout()
    plt.savefig(output_folder + "plot_fitted_data_chi.png")
    plt.close(fig)

    fig, ax = plt.subplots(ncols=2)
    make_choropleth(
        d_incidence,
        imd,
        age,
        dates,
        ylist,
        ax,
        0,
        "",  # "incidence on " + first_forecast_date.strftime("%Y-%m-%d"),
    )
    make_choropleth(
        d_incidence / N_xarray * 1e5,
        imd,
        age,
        dates,
        ylist,
        ax,
        1,
        "",  # "incidence per 100k on " + first_forecast_date.strftime("%Y-%m-%d"),
    )
    fig.tight_layout()
    plt.savefig(output_folder + "plot_fitted_data_choropleth_incidence.png")
    plt.close(fig)


if __name__ == "__main__":

    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument(
        "run_type",
        type=str,
        help="either quickrun_config.sh or config.sh overrules burning, thin and alpha_t_time",
    )
    parser.add_argument(
        "inferencedata",
        type=str,
        help="inference data case and covar data",
    )
    parser.add_argument(
        "insample7",
        type=str,
        help="insample 7 predictions",
    )
    parser.add_argument(
        "insample14",
        type=str,
        help="insample 14 predictions",
    )
    parser.add_argument(
        "insample28",
        type=str,
        help="insample 28 predictions",
    )
    parser.add_argument(
        "insampleAll",
        type=str,
        help="insample for all predictions",
    )
    parser.add_argument(
        "medium_term",
        type=str,
        help="medium term predictions",
    )
    parser.add_argument(
        "reproduction_number",
        type=str,
        help="reproduction number prediction",
    )
    parser.add_argument(
        "output_folder",
        type=str,
        help="root of web folder",
    )
    parser.add_argument(
        "posterior",
        type=str,
        help="posterior.hd5 predictions",
    )
    parser.add_argument(
        "HEC",
        type=str,
        help="is the script running on the HEC? Yes/No",
    )
    args = parser.parse_args()

    plot_fitted_results(
        run_type=args.run_type,
        input_files=[
            args.inferencedata,
            args.insample7,
            args.insample14,
            args.medium_term,
            args.reproduction_number,
            args.insample28,  # needs to be here othewise input_files[...] will be incorrect
            args.insampleAll,
        ],
        output_folder=args.output_folder,
        samples=args.posterior,
        HEC=args.HEC,
    )

"""
poetry run python -m covid19uk.posterior.plot_fitted_results \
    config.yaml \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/inferencedata.nc \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/insample7.nc \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/insample14.nc \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/insample28.nc \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/insampleAll.nc \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/medium_term.nc \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/reproduction_number.nc \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/ \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/posterior.hd5 \
    No
"""
