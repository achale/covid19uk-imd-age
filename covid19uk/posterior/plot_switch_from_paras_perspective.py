from turtle import color
import h5py
import matplotlib.colors as clr
import matplotlib.dates as mdates
import matplotlib.pylab as pylab
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xarray

params = {
    "figure.figsize": (14, 6),
    "legend.loc": "lower left",
    "font.size": "12",  # comment out for supp mat
}

pylab.rcParams.update(params)

CI_LOW = 0.01
CI_UPP = 0.99


def get_data_file(input_file, file_dates, i):
    data_file = input_file.replace("xx-xx-xx", file_dates[i])
    data = xarray.open_dataset(data_file, group="constant_data")
    return data.indexes.values()


def get_samples(input_file, file_dates, samples, burnin, thin, thing, i):
    age, _, imd, dates = get_data_file(input_file, file_dates, i)
    date = max(dates)
    ss = samples.replace("xx-xx-xx", file_dates[i])
    post = h5py.File(ss, "r")
    data = np.mean(post["samples/" + thing][burnin:, :][::thin], axis=0)
    if thing == "rho":
        # part of 2.0 * (0.5 + (rho_long - 0.5)* tf.math.tanh(xi_imd * (tf.reduce_mean(IMD, axis=-1) - IMD_long))
        data = data - 0.5
    elif thing == "psi":
        # part of 2.0 * (1.0 + param["psi"] - tf.reduce_mean(param["psi"]))
        data = data - np.mean(data)  # in general mean(data) != 0
    dd = list(data)
    dd.append(date)
    return dd


def get_CI(input_file, file_dates, samples, burnin, thin, thing, i, CI_level):
    age, _, imd, dates = get_data_file(input_file, file_dates, i)
    date = max(dates)
    ss = samples.replace("xx-xx-xx", file_dates[i])
    post = h5py.File(ss, "r")
    CI = np.quantile(
        post["samples/" + thing][burnin:, :][::thin], q=CI_level, axis=0
    )
    if thing == "rho":
        # part of 2.0 * (0.5 + (rho_long - 0.5)* tf.math.tanh(xi_imd * (tf.reduce_mean(IMD, axis=-1) - IMD_long))
        CI = CI - 0.5
    elif thing == "psi":
        # part ot 2.0 * (1.0 + param["psi"] - tf.reduce_mean(param["psi"]))
        data = np.mean(post["samples/" + thing][burnin:, :][::thin], axis=0)
        CI = CI - np.mean(data)  # in general mean(data) != 0
    dd = list(CI)
    dd.append(date)
    return dd


def plot_param(
    ax,
    df,
    CI_low,
    CI_upp,
    ylbl,
    mcmc_times,
    training_data_end_date,
    locator,
    fmt,
):
    df.plot(linewidth=2, style=".-", ax=ax, markersize=10, x_compat=True)
    for i in range(CI_low.shape[1]):
        ax.fill_between(
            mcmc_times,
            CI_low[CI_low.columns[i]].to_list(),
            CI_upp[CI_upp.columns[i]].to_list(),
            color="#000000",  # remove to have same colour as plot line
            alpha=0.2,
        )
    ax.set_xlabel("end date of 12-week training period")
    ax.set_ylabel(ylbl)
    ax.get_legend().set_title("age")
    ax.axvline(
        x=training_data_end_date,
        color="#000000",
        alpha=0.3,
        linestyle="dashed",
    )
    ax.axhline(y=0.0, color="#000000", alpha=0.3)
    ax.xaxis.set_major_locator(locator)
    ax.xaxis.set_major_formatter(fmt)
    return ax


def plot_fitted_results(
    run_type,
    input_file,
    output_folder,
    samples,
    file_dates,
    training_data_end_date,
):

    if run_type == "quickrun_config.yaml":
        burnin = 2000
        thin = 1
        alpha_t_time = 50
    elif run_type == "config.yaml":
        burnin = 20000
        thin = 30
        alpha_t_time = 50
    elif run_type == "demo_config.yaml":
        burnin = 2000
        thin = 3
        alpha_t_time = 15
    else:
        pass  # use parameters passed into function

    age, _, imd, dates = get_data_file(input_file, file_dates, 0)
    col_names = age.to_list()
    col_names.append("dates")
    df_rho = pd.DataFrame(columns=col_names)
    df_rho_CI_low = pd.DataFrame(columns=col_names)
    df_rho_CI_upp = pd.DataFrame(columns=col_names)
    df_psi = pd.DataFrame(columns=col_names)
    df_psi_CI_low = pd.DataFrame(columns=col_names)
    df_psi_CI_upp = pd.DataFrame(columns=col_names)

    for i in range(len(file_dates)):
        df_rho.loc[df_rho.shape[0]] = get_samples(
            input_file, file_dates, samples, burnin, thin, "rho", i
        )
        df_rho_CI_low.loc[df_rho_CI_low.shape[0]] = get_CI(
            input_file, file_dates, samples, burnin, thin, "rho", i, CI_LOW
        )
        df_rho_CI_upp.loc[df_rho_CI_upp.shape[0]] = get_CI(
            input_file, file_dates, samples, burnin, thin, "rho", i, CI_UPP
        )
        df_psi.loc[df_psi.shape[0]] = get_samples(
            input_file, file_dates, samples, burnin, thin, "psi", i
        )
        df_psi_CI_low.loc[df_psi_CI_low.shape[0]] = get_CI(
            input_file, file_dates, samples, burnin, thin, "psi", i, CI_LOW
        )
        df_psi_CI_upp.loc[df_psi_CI_upp.shape[0]] = get_CI(
            input_file, file_dates, samples, burnin, thin, "psi", i, CI_UPP
        )
    print(df_psi_CI_low)
    print(df_psi_CI_upp)

    df_rho = df_rho.set_index("dates")
    df_rho_CI_low = df_rho_CI_low.set_index("dates")
    df_rho_CI_upp = df_rho_CI_upp.set_index("dates")
    df_psi = df_psi.set_index("dates")
    df_psi_CI_low = df_psi_CI_low.set_index("dates")
    df_psi_CI_upp = df_psi_CI_upp.set_index("dates")
    mcmc_times = df_psi.index.values

    locator = mdates.MonthLocator()  # every month
    fmt = mdates.DateFormatter("%Y-%m-%d")

    fig, ax = plt.subplots(ncols=2)
    plot_param(
        ax[1],
        df_rho,
        df_rho_CI_low,
        df_rho_CI_upp,
        r"$\rho_K - 0.5$",
        mcmc_times,
        training_data_end_date,
        locator,
        fmt,
    )
    plot_param(
        ax[0],
        df_psi,
        df_psi_CI_low,
        df_psi_CI_upp,
        r"$\psi_K - \overline{\psi}$",
        mcmc_times,
        training_data_end_date,
        locator,
        fmt,
    )
    fig.autofmt_xdate()
    fig.savefig(
        output_folder + "plot_rho_and_psi_from_multiple_MCMC_runs" + ".png"
    )

    fig, ax = plt.subplots()
    plot_param(
        ax,
        df_rho,
        df_rho_CI_low,
        df_rho_CI_upp,
        r"$\rho_K - 0.5$",
        mcmc_times,
        training_data_end_date,
        locator,
        fmt,
    )
    fig.autofmt_xdate()
    fig.savefig(output_folder + "plot_rho_from_multiple_MCMC_runs" + ".png")
    plt.show()


if __name__ == "__main__":

    import json
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument(
        "run_type",
        type=str,
        help="either quickrun_config.sh or config.sh overrules burning, thin and alpha_t_time",
    )
    parser.add_argument(
        "inferencedata",
        type=str,
        help="inference data case and covar data",
    )
    parser.add_argument(
        "posterior",
        type=str,
        help="posterior.hd5 predictions",
    )
    parser.add_argument(
        "dates",
        type=str,
        help="dates for which data and posterior is available",
    )
    parser.add_argument(
        "output_folder",
        type=str,
        help="root of web folder",
    )
    parser.add_argument(
        "training_data_end_date",
        type=str,
        help="oringial training data end date used in most of paper",
    )
    args = parser.parse_args()

    plot_fitted_results(
        run_type=args.run_type,
        input_file=args.inferencedata,
        output_folder=args.output_folder,
        samples=args.posterior,
        file_dates=json.loads(args.dates),
        training_data_end_date=args.training_data_end_date,
    )

"""
poetry run python -m covid19uk.posterior.plot_switch_from_paras_perspective \
    config.yaml \
    results/model7_xx-xx-xx_Polymod_imd_suscept_rho_per_age_psi_per_age/inferencedata.nc \
    results/model7_xx-xx-xx_Polymod_imd_suscept_rho_per_age_psi_per_age/posterior.hd5 \
    "[\"21-07-05\", \"21-07-19\", \"21-08-02\", \"21-08-16\", \"21-08-30\", \"21-09-13\", \"21-09-27\", \"21-10-11\", \"21-10-25\", \"21-11-08\", \"21-11-22\", \"21-12-06\"]" \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/ \
    "2021-08-29"
"""
