import matplotlib.pylab as pylab
import matplotlib.pyplot as plt
import numpy as np
import xarray

params = {
    "figure.figsize": (14, 6),
    "legend.loc": "upper left",
    "font.size": "12",  # comment out for supp mat
}
pylab.rcParams.update(params)


def make_choropleth(
    dd,
    imd,
    age,
    dates,
    ylist,
    ax,
    i,
    title,
    zlabel="",
    cmap="YlOrRd",
    vmin=None,
    vmax=None,
):
    im = dd.plot(
        x="imd_decile",
        y="age_index",
        ax=ax[i],
        cmap=cmap,
        cbar_kwargs={"label": zlabel},
    )
    ax[i].set_xlabel("deprivation (IMD)")
    ax[i].set_ylabel("age")
    ax[i].set_title(title)
    ax[i].set_xticks(range(1, len(imd) + 1, 1))
    ax[i].set_yticks(ylist)
    ax[i].set_yticklabels(age)
    if (vmin is not None) and (vmax is not None):
        im.set_clim(vmin, vmax)


def make_timeseries(
    dd,
    thing,
    ax,
    i,
    log=None,
    title="daily incidence per 100k",
    linestyle="solid",
    smoothed="smoothed_0",  # default no smoothing - added 06-10-2022
):
    ax[i].set_prop_cycle(None)
    if smoothed != "smoothed_0":
        window_length = smoothed.split("_")[1]
        dd = (
            dd.rolling(time=int(window_length), center=True)
            .mean()
            .dropna("time")
        )

    lbl = thing.replace("_", " ").split(" ", 1)[0]
    lbl = lbl.upper() if lbl == "imd" else lbl
    dd.plot.line(
        hue=thing,
        ax=ax[i],
        linestyle=linestyle,
    )
    ax[i].set_xlabel("")
    ax[i].set_ylabel(title)
    ax[i].get_legend().set_title(lbl)
    if log == "log":
        ax[i].set_yscale("log")


def plot_raw_data(data_file, cumsums_file, counts_file, output_folder):
    N = xarray.open_dataset(data_file, group="constant_data")["N"]
    cumsum_imd = xarray.open_dataset(cumsums_file, group="cumsum_imd")[
        "cumsum_imd"
    ]
    cumsum_age = xarray.open_dataset(cumsums_file, group="cumsum_age")[
        "cumsum_age"
    ]
    count_imd = xarray.open_dataset(counts_file, group="count_imd")["count_imd"]
    count_age = xarray.open_dataset(counts_file, group="count_age")["count_age"]

    cases = xarray.open_dataset(data_file, group="observations")["cases"]
    data = xarray.Dataset({"cases": cases, "N": N})
    age, imd, dates = data.indexes.values()
    ylist = list(range(0, -len(age), -1))

    d0 = data["cases"].sum(dim="time")
    d0["age_index"] = ("age_group", ylist)
    # print("\n\ncase count:\n", d0, "\n\n")

    d1 = data["cases"].sum(dim="time") / data["N"] * 1e5
    d1["age_index"] = ("age_group", ylist)
    # print("\n\ncase count per 100K:\n", d1, "\n\n")

    fig, ax = plt.subplots(ncols=2)
    # plt.suptitle("COVID-19 data: + min(dates).strftime("%Y-%m-%d") + " to " + max(dates).strftime("%Y-%m-%d"))
    make_choropleth(d0, imd, age, dates, ylist, ax, 0, "")  # "case count"
    make_choropleth(
        d1, imd, age, dates, ylist, ax, 1, ""
    )  # "case count per 100k"
    fig.tight_layout()
    plt.savefig(output_folder + "plot_raw_data_choropleth.png")
    plt.close(fig)

    d3 = (
        data["cases"].sum(dim="imd_decile")
        / data["N"].sum(dim="imd_decile")
        * 1e5
    )
    d4 = (
        data["cases"].sum(dim="age_group")
        / data["N"].sum(dim="age_group")
        * 1e5
    )
    fig, ax = plt.subplots(ncols=2)
    # plt.suptitle("COVID-19 data: " + min(dates).strftime("%Y-%m-%d") + " to " + max(dates).strftime("%Y-%m-%d"))
    make_timeseries(d3, "age_group", ax, 0)
    make_timeseries(d4, "imd_decile", ax, 1)
    fig.tight_layout()
    plt.savefig(output_folder + "plot_raw_data_timeseries.png")
    plt.close(fig)
    plt.show()

    # cumsums
    fig, ax = plt.subplots(ncols=2)
    ax[0].axvspan(min(dates), max(dates), alpha=0.5, color="#CCCCCC")
    ax[1].axvspan(min(dates), max(dates), alpha=0.5, color="#CCCCCC")
    make_timeseries(
        cumsum_age, "age_group", ax, 0, title="cumsums per imd decile"
    )
    make_timeseries(
        cumsum_imd, "imd_decile", ax, 1, title="cumsums per age group"
    )
    fig.tight_layout()
    plt.savefig(output_folder + "plot_raw_cumsums_timeseries.png")
    plt.close(fig)

    fig, ax = plt.subplots(ncols=2)
    ax[0].axvspan(min(dates), max(dates), alpha=0.5, color="#CCCCCC")
    ax[1].axvspan(min(dates), max(dates), alpha=0.5, color="#CCCCCC")
    make_timeseries(
        cumsum_age / data["N"].sum(dim="imd_decile"),
        "age_group",
        ax,
        0,
        title="cumulative sum as a proportion of population",
    )
    make_timeseries(
        cumsum_imd / data["N"].sum(dim="age_group"),
        "imd_decile",
        ax,
        1,
        title="cumulative sum as a proportion of population",
    )
    fig.tight_layout()
    plt.savefig(output_folder + "plot_raw_cumsums_per_pop_timeseries.png")
    plt.close(fig)

    # incidence - rolling average
    rolling_window = 14
    min_date = "2020-12-01"
    max_date = max(count_age.coords["time"])
    fig, ax = plt.subplots(ncols=2)
    ax[0].axvspan(min(dates), max(dates), alpha=0.5, color="#CCCCCC")
    ax[1].axvspan(min(dates), max(dates), alpha=0.5, color="#CCCCCC")
    count_age_subset = count_age.sel(time=slice(min_date, max_date))
    make_timeseries(
        count_age_subset.rolling(time=rolling_window, center=True)
        .mean()
        .dropna("time"),
        "age_group",
        ax,
        0,
        log="log",
        title="daily incidence",
    )
    count_imd_subset = count_imd.sel(time=slice(min_date, max_date))
    make_timeseries(
        count_imd_subset.rolling(time=rolling_window, center=True)
        .mean()
        .dropna("time"),
        # count_imd,
        "imd_decile",
        ax,
        1,
        log="log",
        title="daily incidence",
    )
    fig.tight_layout()
    plt.savefig(output_folder + "plot_raw_counts_timeseries.png")
    plt.close(fig)

    fig, ax = plt.subplots(ncols=2)
    ax[0].axvspan(min(dates), max(dates), alpha=0.5, color="#CCCCCC")
    ax[1].axvspan(min(dates), max(dates), alpha=0.5, color="#CCCCCC")
    count_age_100k = count_age / data["N"].sum(dim="imd_decile") * 1e5
    count_age_100k = count_age_100k.sel(time=slice(min_date, max_date))
    make_timeseries(
        count_age_100k.rolling(time=rolling_window, center=True)
        .mean()
        .dropna("time"),
        "age_group",
        ax,
        0,
        log="log",
        title="daily incidence per 100k",
    )
    count_imd_100k = count_imd / data["N"].sum(dim="age_group") * 1e5
    count_imd_100k = count_imd_100k.sel(time=slice(min_date, max_date))
    make_timeseries(
        count_imd_100k.rolling(time=rolling_window, center=True)
        .mean()
        .dropna("time"),
        "imd_decile",
        ax,
        1,
        log="log",
        title="daily incidence per 100k",
    )

    fig.tight_layout()
    plt.savefig(output_folder + "plot_raw_counts_per_pop_timeseries.png")
    plt.close(fig)

    # incidence - over fitted and simulated window
    min_date = min(dates) - np.timedelta64(14, "D")
    max_date = max(dates) + np.timedelta64(56, "D")  # max fitted date + 56days
    print()
    fig, ax = plt.subplots(ncols=2, sharex=True)
    ax[0].axvspan(min(dates), max(dates), alpha=0.5, color="#CCCCCC")
    ax[1].axvspan(min(dates), max(dates), alpha=0.5, color="#CCCCCC")
    count_age_subset = count_age.sel(time=slice(min_date, max_date))
    make_timeseries(
        count_age_subset.dropna("time"),
        "age_group",
        ax,
        0,
        log="log",
        title="incidence by IMD decile",
    )
    count_imd_subset = count_imd.sel(time=slice(min_date, max_date))
    make_timeseries(
        count_imd_subset.dropna("time"),
        "imd_decile",
        ax,
        1,
        log="log",
        title="incidence by age group",
    )
    fig.tight_layout()
    plt.savefig(output_folder + "plot_raw_counts_short_timeseries.png")
    plt.close(fig)

    fig, ax = plt.subplots(ncols=2, sharex=True)
    ax[0].axvspan(min(dates), max(dates), alpha=0.5, color="#CCCCCC")
    ax[1].axvspan(min(dates), max(dates), alpha=0.5, color="#CCCCCC")
    count_age_100k = count_age / data["N"].sum(dim="imd_decile") * 1e5
    count_age_100k = count_age_100k.sel(time=slice(min_date, max_date))
    make_timeseries(
        count_age_100k.dropna("time"),
        "age_group",
        ax,
        0,
        log="lin",
        title="daily incidence per 100k",
    )
    count_imd_100k = count_imd / data["N"].sum(dim="age_group") * 1e5
    count_imd_100k = count_imd_100k.sel(time=slice(min_date, max_date))
    make_timeseries(
        count_imd_100k.rolling(time=rolling_window, center=True)
        .mean()
        .dropna("time"),
        "imd_decile",
        ax,
        1,
        log="lin",
        title="daily incidence per 100k",
    )

    fig.tight_layout()
    plt.savefig(output_folder + "plot_raw_counts_per_pop_short_timeseries.png")
    plt.close(fig)

    print(
        "date range: "
        + min(dates).strftime("%Y-%m-%d")
        + " to "
        + max(dates).strftime("%Y-%m-%d")
        + "\n"
    )


if __name__ == "__main__":

    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument(
        "data_file",
        type=str,
        help="path to inferencedata.nc",
    )
    parser.add_argument(
        "cumsums_file",
        type=str,
        help="path to sumsums.nc",
    )
    parser.add_argument(
        "counts_file",
        type=str,
        help="path to counts.nc",
    )
    parser.add_argument(
        "output_folder",
        type=str,
        help="path to output folder ending in /",
    )
    args = parser.parse_args()

    plot_raw_data(
        args.data_file,
        args.cumsums_file,
        args.counts_file,
        args.output_folder,
    )


"""
poetry run python -m covid19uk.posterior.plot_raw_data \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/inferencedata.nc \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/cumsums.nc \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/counts.nc \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/
"""
