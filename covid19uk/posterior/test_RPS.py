# poetry run python -m covid19uk.posterior.check_fit_test

import datetime
import dhaconfig
import geopandas as gp
import json
import numpy as np
import pandas as pd
import xarray

from statsmodels.distributions.empirical_distribution import (
    ECDF,
)  ### used only for testing

from covid19uk.posterior.make_dha import make_age_imd_col

from pathlib import Path

from covid19uk.data.util import imdage_idx
from covid19uk.model_spec import STOICHIOMETRY

from gemlib.util import compute_state

utils = dhaconfig.utilities()
dha = dhaconfig.dha()

q = [0.05, 0.5, 0.95]


def xarray2summarydf(arr):
    mean = arr.mean(dim="iteration").to_dataset(name="value")
    q = np.arange(start=0.05, stop=1.0, step=0.05)
    quantiles = arr.quantile(q=q, dim="iteration").to_dataset(dim="quantile")
    ds = mean.merge(quantiles).rename_vars({qi: f"{qi:.2f}" for qi in q})
    return ds.to_dataframe().reset_index()


def insample_tidy(df, dp, num_days, cis, cases):
    """
    :param df: (dataframe)
    :param dp: (int) number of decimal places
    :param num_days: (int) number of weeks
    :param cis: (list) list of quantile values e.g. [0.05, 0.95] or None
    :param cases: (dataframe) actual number of recorded cases per day
    :return: (dataframe)
    """
    df = df.loc[
        :, ["age_imd_coords", "time", "value", str(cis[0]), str(cis[1])]
    ]
    df = df.round({"value": dp, str(cis[0]): dp, str(cis[1]): dp})
    times = [min(df.time) + datetime.timedelta(days=x) for x in range(num_days)]
    df = df[df["time"].isin(times)]
    return pd.merge(
        pd.DataFrame(df),
        pd.DataFrame(cases),
        how="left",
        on=["age_imd_coords", "time"],
    )


def make_age_imd_index_for_geo(df, name):
    df["age"] = df["age_imd"].str.split("_").str[0]
    df["age"] = df["age"].str.replace("+", "plus-", regex=False)
    df["age_init"] = df["age"].str.split("-").str[0]
    df["age_init"] = df["age_init"].str.replace("plus", "", regex=False)
    df["age_init"] = df["age_init"] + "plus"
    df["imd"] = df["age_imd"].str.split("_").str[1]
    df[name] = df[["imd", "age_init"]].agg("_".join, axis=1)
    df = df.drop(columns=["age_init", "age", "imd"])
    return df


def make_geodf(df_geo, df):
    """
    :param df_geo: (geodataframe)
    :param df: (dataframe)
    :return: (geodataframe)
    """
    spdf = df_geo[df_geo["LAD code"].isin(np.array(df["LAD code"]))]
    spdf = spdf.sort_values(by="LAD code")
    spdf = spdf.merge(df, on="LAD code")
    spdf["LAD code"] = spdf["LAD code"].str.replace(",", "_")
    return spdf  # .rename({"lad19cd": "LAD code", "lad19nm": "LAD name"}, axis="columns")


def insample_write_json(df, web_folder_data, ci_list, name):
    """
    :param df: (dataframe)
    :param web_folder_data: (str) output folder for geojson files e.g. "z:/dha_website_root/data/"
    :param ci_list: (list) list of quantile values e.g. [0.05, 0.95] or None
    :parma name: (str) name of layer
    """
    lst = (
        '{"labels":'
        + json.dumps(
            (
                pd.DatetimeIndex(list(df.loc[:, "time"].unique())).strftime(
                    "%d %b"
                )
            ).to_list()
        )
        + ","
    )
    lst = (
        lst
        + '"dateFrom":"'
        + min(df.loc[:, "time"]).strftime("%d %b %Y")
        + '",'
    )
    locations = df["LAD code"].unique()
    for x in locations:
        dd = df[df["LAD code"] == str(x)]
        lst = (
            lst + '"' + x + '": {'
            '"LADname":"' + dd.iloc[0]["LAD name"] + '",'
            '"mean":' + str(dd["value"].tolist()) + ","
            '"quantLo":' + str(dd[str(ci_list[0])].tolist()) + ","
            '"quantUp":' + str(dd[str(ci_list[1])].tolist()) + ","
            '"cases":' + str(dd["cases"].tolist())
        )
        lst_line_end = "}" if x == locations[len(locations) - 1] else "},"
        lst = lst + lst_line_end
    lst = lst + "}"
    lst = lst.replace(", ", ",")
    utils.write_text_file(web_folder_data, name + ".json", lst)


def case_exceedance(input_files, lag):
    """Calculates case exceedance probabilities,
       i.e. Pr(pred[lag:] < observed[lag:])

    :param input_files: [data pickle, prediction pickle]
    :param lag: the lag for which to calculate the exceedance
    """

    data_file, prediction_file = input_files

    data = xarray.open_dataset(data_file, group="observations")["cases"]
    data = imdage_idx(data).transpose()

    prediction = xarray.open_dataset(prediction_file, group="predictions")[
        "events"
    ]

    modelled_cases = np.sum(prediction[..., :lag, -1], axis=-1)
    observed_cases = np.sum(data[:, -lag:].values, axis=-1)
    exceedance = np.mean(modelled_cases < observed_cases, axis=0)

    return exceedance


##################################################
output_folder = "results/model1l_long/"
loc = "results/model1l_long/"
files = [
    "inferencedata.nc",
    "insample7.nc",
    "insample14.nc",
    "medium_term.nc",
    "reproduction_number.nc",
    "insample28.nc",
    "insampleAll.nc",  # new
]
input_files = [loc + s for s in files]  # new but ignore
ci_list = [0.05, 0.95]


data = xarray.open_dataset(input_files[0], group="constant_data")  # covars

N = imdage_idx(data["N"])
N = N.to_dataframe().reset_index()
N = make_age_imd_col(N)

cases = xarray.open_dataset(input_files[0], group="observations")["cases"]
cases = imdage_idx(cases)  # .transpose()
cases = cases.to_dataframe().reset_index()
cases = make_age_imd_col(cases)

# dha_format = dha_format_dict()
layers = {}
output_folders = {
    "web_folder_data": Path(output_folder) / "data",
    "web_folder_js": Path(output_folder) / "js",
}
for k, v in output_folders.items():
    v.mkdir(parents=True, exist_ok=True)

# geopackage: load and clean
geo = gp.read_file("data/default.geojson")
geo = geo.drop(columns=["0", "0.L", "0.U"])


# Case exceedance - used by Insample
exceed7 = case_exceedance((input_files[0], input_files[1]), 7)
exceed14 = case_exceedance((input_files[0], input_files[2]), 14)
exceed28 = case_exceedance((input_files[0], input_files[5]), 28)

case_exceed = pd.DataFrame(
    {
        "Pr(pred<obs)_7": exceed7,
        "Pr(pred<obs)_14": exceed14,
        "Pr(pred<obs)_28": exceed28,
    },
    index=exceed7.coords["age_imd_coords"],
)
case_exceed = case_exceed.reset_index()
# case_exceed["time"] = default_time


####################### check fit

name = "RPS"
insample = xarray.open_dataset(input_files[6], group="predictions")

insample_df = xarray2summarydf(
    insample["events"][..., 2].reset_coords(drop=True)
)
insample_df_tidy = insample_tidy(
    insample_df,
    2,
    insample.coords["time"].values.shape[0],
    ci_list,
    cases.rename(columns={"age_imd": "age_imd_coords"}),
)  # data for timeseries plot
startdate = pd.DatetimeIndex(insample_df_tidy["time"])[0].strftime(" %d %b")
insample_df_tidy_geo = make_age_imd_index_for_geo(
    insample_df_tidy.rename(columns={"age_imd_coords": "age_imd"}),
    "LAD code",
)
geodf = make_geodf(geo, insample_df_tidy_geo)
insample_write_json(
    geodf.drop(columns="geometry"),
    output_folders["web_folder_data"],
    ci_list,
    name,
)


def ranked_prob_score(input_files):
    """Calculates rank prob score (CRPS) from empirical CDF
    RPS = int( ( F(x) - H(x>obs) )**2, x=-inf..inf )
        where H=Heaviside, obs=observation and F(x)=eCDF=y
        Note that prediction_pickle is of size [samples, meta-pop, time, events]
             and events[S>E, E->I, I>R] where I>R=predicted_case_incidence
    :param input_files: [data_pickle, prediction_pickle]
    """

    df, pf = input_files
    cases = xarray.open_dataset(df, group="observations")["cases"]
    cases = imdage_idx(cases).transpose()[
        :, 0:89
    ]  # ****************************************** REMOVE [:,0:89] *************************************

    pred = xarray.open_dataset(pf, group="predictions")["events"][..., -1]

    # eCDF
    eCDF_x = np.sort(pred, axis=-3)  # iterations=-3
    eCDF_y = np.arange(1, eCDF_x.shape[0] + 1) / float(eCDF_x.shape[0])  # F(x)
    eCDF_y_ = np.tile(eCDF_y, (pred.shape[1], pred.shape[2], 1)).transpose(
        2, 0, 1
    )

    # TEST:
    # print(eCDF_y.shape)
    # print(pred.shape)
    # c = np.tile(eCDF_y, (pred.shape[1], pred.shape[2], 1)).transpose(2, 0, 1)
    # print(sum((c[:, 3, 4] - (eCDF_y)) ** 2)) # should equal zero
    # print(sum((c[:, 10, 40] - (eCDF_y)) ** 2)) # should equal zero

    # TEST:
    # from statsmodels.distributions.empirical_distribution import (ECDF);
    # cdf = ECDF(pred[:, 55, 55, -1])
    # print(eCDF_x[:, 55, 55])
    # print(cdf.x[1 : len(cdf.x)])
    # print(cdf.x[1 : len(cdf.x)] - eCDF_x[:, 55, 55])
    # print(eCDF_y)
    # print(cdf.y[1 : len(cdf.y)])
    # print(cdf.y[1 : len(cdf.y)] - eCDF_y)
    # print(sum(cdf.y[1 : len(cdf.y)] - eCDF_y))

    Hy = 1 * (eCDF_x > cases.values)  # Heaviside(eCDF_x-case_value)

    integrand = (eCDF_y_ - Hy) ** 2

    RPS_m_t = np.trapz(integrand, x=eCDF_x, axis=-3)  # per meta-pop per time
    RPS_m = np.sum(RPS_m_t, axis=-1)  # CRPS per meta-pop
    RPS_t = np.sum(RPS_m_t, axis=-2)  # CRPS per time
    RPS = np.sum(RPS_m_t)  # overall CRPS

    return [RPS_m_t, RPS_m, RPS_t, RPS]


RPS_m_t, RPS_m, RPS_t, RPS = ranked_prob_score((input_files[0], input_files[6]))

RPS_m_df = pd.DataFrame(
    {"value": RPS_m},
    index=exceed7.coords["age_imd_coords"],  # same coords as exceed
).reset_index()
# RPS_m_df["time"] = default_time


# layers[name] = do_dha_things(
#        RPS_m_df.rename(columns={"age_imd_coords": "age_imd"}),
#        0,
#        None,
#        geo,
#        output_folders["web_folder_data"],
#        name,
#        dha_format[name],
#        2,
#        url,
#        insample_df_tidy,
# )


# cases = xarray.open_dataset(input_files[0], group="observations")["cases"]
# imd_age_names = imdage_idx_names(cases)
# cases = imdage_idx(cases)
# print("cases", cases)
#
# samples = read_pkl(input_files[6])["seir"][..., 2]  # I->R
# prediction = xarray.DataArray(
#    samples,
#    coords=[
#        list(range(samples.shape[0])),
#        imd_age_names,
#        cases.coords["time"].values,
#    ],
#    dims=["iteration", "age_imd", "time"],
# )
#
# qs = prediction.quantile([q[0], q[2]], dim="iteration").to_dataset(
#    dim="quantile"
# )
# mu = prediction.mean(dim="iteration").to_dataset(name="value")
# ds = mu.merge(qs).to_dataframe().reset_index()
# print(read_pkl(input_files[6])["seir"][3, ..., 2])
# print(cases)
#    mean = arr.mean(dim="iteration").to_dataset(name="value")
#    q = np.arange(start=0.05, stop=1.0, step=0.05)
#    quantiles = arr.quantile(q=q, dim="iteration").to_dataset(dim="quantile")
#    ds = mean.merge(quantiles).rename_vars({qi: f"{qi:.2f}" for qi in q})
#    return ds.to_dataframe().reset_index()
#
# pred_mean = pred.mean(axis=0)
# pred_quantiles = np.quantile(pred, [q[0], q[2]], axis=0)
# print("pred ", pred_quantiles.shape)
# xx = pred_quantiles.reshape((2, 92 * 80), order="C")
# print("pred ", xx.shape)
