import CRPS.CRPS as pscore
import datetime
import dhaconfig
import geopandas as gp
import json
import numpy as np
import pandas as pd
import xarray

from pathlib import Path
from itertools import product

from covid19uk.data.util import imdage_idx
from covid19uk.model_spec import STOICHIOMETRY

from gemlib.util import compute_state


utils = dhaconfig.utilities()
dha = dhaconfig.dha()

q = [0.05, 0.5, 0.95]


def dha_format_dict():
    colour_scheme_a = [
        "#fff7db",
        "#ffeda0",
        "#fed976",
        "#fd8d3c",
        "#fc4e2a",
        "#e31a1c",
        "#b10026",
        "#800026",
    ]
    colour_scheme_b = [
        "#181A49",
        "#313695",
        "#4575b4",
        "#74add1",
        "#e0f3f8",
        "#ffffb2",
        "#fed976",
        "#feb24c",
        "#fd8d3c",
        "#fc4e2a",
        "#e31a1c",
        "#b10026",
        "#72001a",
    ]
    userDefined_scheme_b = [
        0,
        0.6,
        0.7,
        0.8,
        0.9,
        1,
        1.1,
        1.2,
        1.3,
        1.4,
        1.5,
        1.6,
        1.7,
        1.8,
    ]
    colour_scheme_c = ["#5e4fa2", "#66c2a5", "#ffffbf", "#fdae61", "#d7191c"]
    userDefined_scheme_c = [0, 0.03, 0.05, 0.95, 0.97, 1]
    colour_scheme_d = [
        "#000073",
        "#302986",
        "#4b4c99",
        "#6270ac",
        "#7895be",
        "#8ebbd1",
        "#b9dfdf",
        "#c4e7d3",
        "#9bccac",
        "#77b085",
        "#54955f",
        "#32793a",
        "#005f13",
    ]
    userDefined_scheme_d = [
        0,
        25,
        50,
        75,
        100,
        125,
        150,
        175,
        200,
        225,
        250,
        275,
        300,
        325,
    ]
    dha_format = {
        "Daily_case_incidence": {
            "colgrades_colours": colour_scheme_a,
            "colgrades_Ints": "75",
            "colgrades_userDefined": "undefined",
            "colgrades_legtitle": "undefined",
            "timeData_timeseries": "true",
            "timePlot_ymax": "undefined",
            "popupBox": "false",
        },
        "Cumulative_case_incidence": {
            "colgrades_colours": colour_scheme_a,
            "colgrades_Ints": "1000",
            "colgrades_userDefined": "undefined",
            "colgrades_legtitle": "undefined",
            "timeData_timeseries": "true",
            "timePlot_ymax": "undefined",
            "popupBox": "false",
        },
        "Daily_case_incidence_per_100k": {
            "colgrades_colours": colour_scheme_a,
            "colgrades_Ints": "15",
            "colgrades_userDefined": "undefined",
            "colgrades_legtitle": "undefined",
            "timeData_timeseries": "true",
            "timePlot_ymax": "undefined",
            "popupBox": "false",
        },
        "Prevalence_per_100k": {
            "colgrades_colours": colour_scheme_a,
            "colgrades_Ints": "100",
            "colgrades_userDefined": "undefined",
            "colgrades_legtitle": "undefined",
            "timeData_timeseries": "true",
            "timePlot_ymax": "undefined",
            "popupBox": "false",
        },
        "Reproduction_number_Rt": {
            "colgrades_colours": colour_scheme_b,
            "colgrades_Ints": "0.1",
            "colgrades_userDefined": userDefined_scheme_b,
            "colgrades_legtitle": "undefined",
            "timeData_timeseries": "true",
            "timePlot_ymax": "undefined",
            "popupBox": "false",
        },
        "Prob_Rt_exceeds_1_0": {
            "colgrades_colours": colour_scheme_c,
            "colgrades_Ints": "0.1",
            "colgrades_userDefined": userDefined_scheme_c,
            "colgrades_legtitle": "Pr(R<sub>t</sub> &#62; 1.0)",
            "timeData_timeseries": "true",
            "timePlot_ymax": "1",
            "popupBox": "false",
        },
        "Insample_7_days": {
            "colgrades_colours": colour_scheme_c,
            "colgrades_Ints": "0.1",
            "colgrades_userDefined": userDefined_scheme_c,
            "colgrades_legtitle": "Pr(pred &#60; obs)",
            "timeData_timeseries": "false",
            "timePlot_ymax": "1",
            "popupBox": "true",
        },
        "Insample_14_days": {
            "colgrades_colours": colour_scheme_c,
            "colgrades_Ints": "0.1",
            "colgrades_userDefined": userDefined_scheme_c,
            "colgrades_legtitle": "Pr(pred &#60; obs)",
            "timeData_timeseries": "false",
            "timePlot_ymax": "1",
            "popupBox": "true",
        },
        "Insample_28_days": {
            "colgrades_colours": colour_scheme_c,
            "colgrades_Ints": "0.1",
            "colgrades_userDefined": userDefined_scheme_c,
            "colgrades_legtitle": "Pr(pred &#60; obs)",
            "timeData_timeseries": "false",
            "timePlot_ymax": "1",
            "popupBox": "true",
        },
        "RPS": {
            "colgrades_colours": colour_scheme_d,
            "colgrades_Ints": "0.1",
            "colgrades_userDefined": userDefined_scheme_d,
            "colgrades_legtitle": "RPS",
            "timeData_timeseries": "false",
            "timePlot_ymax": "1",
            "popupBox": "true",
        },
    }
    return dha_format


def xarray2summarydf(arr):
    mean = arr.mean(dim="iteration").to_dataset(name="value")
    q = np.arange(start=0.05, stop=1.0, step=0.05)
    quantiles = arr.quantile(q=q, dim="iteration").to_dataset(dim="quantile")
    ds = mean.merge(quantiles).rename_vars({qi: f"{qi:.2f}" for qi in q})
    return ds.to_dataframe().reset_index()


def prevalence(prediction, popsize):
    prev = compute_state(
        prediction["initial_state"], prediction["events"], STOICHIOMETRY
    )
    prev = xarray.DataArray(
        prev.numpy(),
        coords=[
            np.arange(prev.shape[0]),
            prediction.coords["age_imd"],
            prediction.coords["time"],
            np.arange(prev.shape[-1]),
        ],
        dims=["iteration", "age_imd", "time", "state"],
    )
    prev_per_1e5 = (
        prev[..., 1:3].sum(dim="state").reset_coords(drop=True)
        / np.array(popsize["N"])[np.newaxis, :, np.newaxis]
        * 100000
    )
    return xarray2summarydf(prev_per_1e5)


def case_exceedance(input_files, lag):
    """Calculates case exceedance probabilities,
       i.e. Pr(pred[lag:] < observed[lag:])

    :param input_files: [data pickle, prediction pickle]
    :param lag: the lag for which to calculate the exceedance
    """

    data_file, prediction_file = input_files

    data = xarray.open_dataset(data_file, group="observations")["cases"]
    data = imdage_idx(data).transpose()

    prediction = xarray.open_dataset(prediction_file, group="predictions")[
        "events"
    ]

    modelled_cases = np.sum(prediction[..., :lag, -1], axis=-1)
    observed_cases = np.sum(data[:, -lag:].values, axis=-1)
    exceedance = np.mean(modelled_cases < observed_cases, axis=0)

    return exceedance


def df_split_index(dd):
    dd[["age_group", "imd_decile"]] = dd["age_imd"].str.split(
        "_", n=1, expand=True
    )
    dd["imd_decile"] = dd["imd_decile"].astype(int)
    return dd.set_index(["age_group", "imd_decile"])


def crps_slow(data, pred):
    """
    Continuous rank probability score
    :param data observations with format xarray dataset where data is called 'cases'
    :param pred predictions with format xarray dataset where data is called 'events'
    """

    time = data.coords["time"].values
    space = data.coords["age_imd_coords"].values
    crps = np.zeros([time.shape[0], space.shape[0]])
    index_ordering = []

    seq = product(range(time.shape[0]), range(space.shape[0]))
    for i, j in seq:
        if i == 0:
            index_ordering.append(space[j])
        obs = data.loc[dict(time=time[i], age_imd_coords=space[j])]
        obs_ = np.squeeze(obs["cases"].values)
        prd = pred.loc[dict(time=time[i], age_imd_coords=space[j])]
        prd_ = np.squeeze(prd["events"].values)
        crps[i, j], fcrps, acrps = pscore(prd_, obs_).compute()

    return index_ordering, crps


def crps_wrapper(input_files):
    df, pf = input_files
    data = xarray.open_dataset(df, group="observations")["cases"]
    data = data.to_dataframe().reset_index()
    data["imd_decile"] = data["imd_decile"].astype(int)
    data = data.sort_values(["age_group", "imd_decile", "time"])
    # data["imd_decile"] = data["imd_decile"].astype(str).str.zfill(2)
    data["age_imd_coords"] = (
        data["age_group"] + "_" + data["imd_decile"].astype(str)
    )
    data = data.drop(columns=["imd_decile", "age_group"]).rename(
        {"count_age_imd": "cases"}, axis="columns"
    )
    data = data.set_index(["age_imd_coords", "time"])
    data = data.to_xarray()  # dataset

    pred = xarray.open_dataset(pf, group="predictions")["events"][..., -1]
    pred = pred.to_dataset()

    index_ordering, crps_m_t = crps_slow(data, pred)
    crps_m = np.sum(crps_m_t, axis=-2)  # CRPS per meta-pop
    crps_m_median = np.quantile(crps_m_t, 0.5, axis=-2)  # CRPS per meta-pop
    crps_t = np.sum(crps_m_t, axis=-1)  # CRPS per time
    crps = np.sum(crps_m_t)  # overall CRPS
    return [index_ordering, crps_m_t, crps_m, crps_m_median, crps_t, crps]


# def ranked_prob_score(input_files):
#
#   Broken version of vectorised CRPS - DON'T USE WITHOUT FIRST FIXING IT!
#
#    """Calculates rank prob score (CRPS) from empirical CDF
#    RPS = int( ( F(x) - H(x>obs) )**2, x=-inf..inf )
#        where H=Heaviside, obs=observation and F(x)=eCDF=y
#        Note that prediction_pickle is of size [samples, meta-pop, time, events]
#             and events[S>E, E->I, I>R] where I>R=predicted_case_incidence
#    :param input_files: [data_pickle, prediction_pickle]
#    """
#
#    df, pf = input_files
#    cases = xarray.open_dataset(df, group="observations")["cases"]
#    cases = imdage_idx(cases).transpose()
#    pred = xarray.open_dataset(pf, group="predictions")["events"][..., -1]
#
#    eCDF_x = np.sort(pred, axis=-3)  # iterations=-3
#    eCDF_y = np.arange(1, eCDF_x.shape[0] + 1) / float(eCDF_x.shape[0])  # F(x)
#    eCDF_y_ = np.tile(eCDF_y, (pred.shape[1], pred.shape[2], 1)).transpose(
#        2, 0, 1
#    )
#
#    H_y = 1 * (eCDF_x > cases.values)  # Heaviside(eCDF_x-case_value)
#
#    integrand = (eCDF_y_ - H_y) ** 2
#
#    RPS_m_t = np.trapz(integrand, x=eCDF_x, axis=-3)  # per meta-pop per time
#    RPS_m = np.sum(RPS_m_t, axis=-1)  # CRPS per meta-pop
#    RPS_t = np.sum(RPS_m_t, axis=-2)  # CRPS per time
#    RPS = np.sum(RPS_m_t)  # overall CRPS
#
#    return [RPS_m_t, RPS_m, RPS_t, RPS]


def summarydf_tidy(df, dp, num_weeks, cis):
    """
    :param df: (dataframe)
    :param dp: (int) number of decimal places
    :param num_weeks: (int) number of weeks
    :param cis: (list) list of quantile values e.g. [0.05, 0.95] or None
    :return: (dataframe)
    """
    if cis is None:
        df = df.loc[:, ["LAD code", "time", "value"]]
        df = df.round({"value": dp})
        df = df.rename({"value": "mean"}, axis="columns")
    else:
        df = df.loc[:, ["LAD code", "time", "value", str(cis[0]), str(cis[1])]]
        df = df.round({"value": dp, str(cis[0]): dp, str(cis[1]): dp})
        df = df.rename(
            {"value": "mean", str(cis[0]): ".L", str(cis[1]): ".U"},
            axis="columns",
        )
    df["idx"] = df["time"]
    if len(df["time"].unique()) > 1:
        start_date = min(df.time) + datetime.timedelta(days=1)
        times = [
            start_date + datetime.timedelta(days=x)
            for x in range(0, 7 * (num_weeks + 1), 7)
        ]
        df = df[df["time"].isin(times)]
    else:
        times = [df.iloc[0]["time"]]
    di = dict(zip(times, list(range(len(times)))))
    df = df.replace({"idx": di})
    return df


def insample_tidy(df, dp, num_days, cis, cases):
    """
    :param df: (dataframe)
    :param dp: (int) number of decimal places
    :param num_days: (int) number of days
    :param cis: (list) list of quantile values e.g. [0.05, 0.95] or None
    :param cases: (dataframe) actual number of recorded cases per day
    :return: (dataframe)
    """
    df = df.loc[
        :, ["age_imd_coords", "time", "value", str(cis[0]), str(cis[1])]
    ]
    df = df.round({"value": dp, str(cis[0]): dp, str(cis[1]): dp})
    times = [min(df.time) + datetime.timedelta(days=x) for x in range(num_days)]
    df = df[df["time"].isin(times)]
    return pd.merge(
        pd.DataFrame(df),
        pd.DataFrame(cases),
        how="left",
        on=["age_imd_coords", "time"],
    )


def dflong2wide_subset_df(dx, col_name, postfix):
    dy = dx.loc[:, ["LAD code", "idx", col_name]]
    dy["idx"] = dy["idx"].astype(str) + postfix
    dy = dy.rename({col_name: "value"}, axis="columns")
    dy = dy.groupby(["LAD code", "idx"]).sum().unstack("idx")
    dy.columns = dy.columns.droplevel()
    return dy.reset_index()


def dflong2wide(df, cis):
    """
    :param df: (dataframe) wide format
    :param cis: (list) list of quantile values e.g. [0.05, 0.95] or None
    :return: (dataframe) long format
    """
    dd = dflong2wide_subset_df(df, "mean", "")
    if cis is not None:
        dl = dflong2wide_subset_df(df, ".L", ".L")
        du = dflong2wide_subset_df(df, ".U", ".U")
        dj = dl.join(du.set_index("LAD code"), "LAD code")
        dd = dd.join(dj.set_index("LAD code"), "LAD code")
    dd = dd.sort_values("LAD code")
    return dd


def make_geodf(df_geo, df):
    """
    :param df_geo: (geodataframe)
    :param df: (dataframe)
    :return: (geodataframe)
    """
    spdf = df_geo[df_geo["LAD code"].isin(np.array(df["LAD code"]))]
    spdf = spdf.sort_values(by="LAD code")
    spdf = spdf.merge(df, on="LAD code")
    spdf["LAD code"] = spdf["LAD code"].str.replace(",", "_")
    return spdf


def write_csv(x, cis, folder, file_name):
    """
    :param x: (dataframe) thing to be saved as csv
    :param cis: (list) list of quantile values e.g. [0.05, 0.95] or None
    :param folder: (str) output folder for geojson files e.g. "z:/dha_website_root/data/"
    :param file_name: (str) file name of csv
    """
    if cis is None:
        y = x.loc[:, ["LAD code", "time", "mean"]]
        y = y.rename({"mean": "value"}, axis="columns")
    else:
        y = x.loc[:, ["LAD code", "time", "mean", ".L", ".U"]]
        y = y.rename(
            {".L": str(cis[0]) + " quantile", ".U": str(cis[1]) + " quantile"},
            axis="columns",
        )
    y = pd.DataFrame(y)
    y = y.rename(columns={"LAD code": "imd_age"})
    y.to_csv(Path(folder) / f"{file_name}.csv")


def write_xls(df1, df2, web_folder_data, name):
    """
    :param df1: (dataframe) insample dataframe
    :param df2: (dataframe) case exceedance dataframe
    :param web_folder_data: (str) output folder for geojson files e.g. "z:/dha_website_root/data/"
    :parma name: (str) name of layer
    """
    writer = pd.ExcelWriter(
        Path(web_folder_data) / f"{name}.xlsx", engine="openpyxl"
    )
    df1 = df1.rename(columns={"age_imd_coords": "age_imd"})
    df2 = df2.drop(columns=["LAD code"])
    df1.to_excel(writer, sheet_name="Insample")
    df2.to_excel(writer, sheet_name="Pr(pred<obs)")
    writer.save()


def insample_write_json(df, web_folder_data, ci_list, name):
    """
    :param df: (dataframe)
    :param web_folder_data: (str) output folder for geojson files e.g. "z:/dha_website_root/data/"
    :param ci_list: (list) list of quantile values e.g. [0.05, 0.95] or None
    :parma name: (str) name of layer
    """
    lst = (
        '{"labels":'
        + json.dumps(
            (
                pd.DatetimeIndex(list(df.loc[:, "time"].unique())).strftime(
                    "%d %b"
                )
            ).to_list()
        )
        + ","
    )
    lst = (
        lst
        + '"dateFrom":"'
        + min(df.loc[:, "time"]).strftime("%d %b %Y")
        + '",'
    )
    locations = df["LAD code"].unique()
    for x in locations:
        dd = df[df["LAD code"] == str(x)]
        lst = (
            lst + '"' + x + '": {'
            '"LADname":"' + dd.iloc[0]["LAD name"] + '",'
            '"mean":' + str(dd["value"].tolist()) + ","
            '"quantLo":' + str(dd[str(ci_list[0])].tolist()) + ","
            '"quantUp":' + str(dd[str(ci_list[1])].tolist()) + ","
            '"cases":' + str(dd["cases"].tolist())
        )
        lst_line_end = "}" if x == locations[len(locations) - 1] else "},"
        lst = lst + lst_line_end
    lst = lst + "}"
    lst = lst.replace(", ", ",")
    utils.write_text_file(web_folder_data, name + ".json", lst)


def make_layer(web_folder_data, name, dates, cis, pars, xlsx, url):
    """
    :param web_folder_data: (str) output folder for geojson files e.g. "z:/dha_website_root/data/"
    :parma name: (str) name of layer
    :param dates: (list) list of dates
    :param cis: (list) list of quantiles e.g. [0.05, 0.95]
    :parma pars: (dict) dictionary of dha parameters: for details see dhaconfig at https://gitlab.com/achale/dhaconfig.git#egg=dhaconfig
    :param xlsx: (boolean) when False downloadData is a csv otherwise xlsx
    :param url: (str) url of json and geojson data if it is external to the web server
    :return: (str) dha map layer as dictionary
    """
    downloadData_ext = ".xlsx" if xlsx else ".csv"
    ci_names = (
        " " if cis is None else str(cis[0]) + "-" + str(cis[1]) + " quantiles"
    )
    layer = dha.build_single_layer(
        geoJsonFile=url + "data/" + name + ".geojson",
        friendlyName=name.replace("_", " ").replace("1 0", "1.0"),
        radioButtonValue=name.replace("_", " ").replace("1 0", "1.0")
        + dates[0].strftime(" %d %b"),
        layerName=str(
            (
                name.replace("_", " ").replace("1 0", "1.0")
                + dates.strftime(" %d %b")
            ).to_list()
        ),
        geojsonName=str(list(map(str, range(len(dates))))),
        geojsonGeom="LAD name",
        geojsonExtraInfo="LAD code",
        mapPosition_centerLatLng=str([55.5, -2.7]),
        mapPosition_zoom="5.5",
        regionNames_country="Click region in UK to view its graph",
        regionNames_area="LAD name",
        colgrades_colours=str(pars["colgrades_colours"]),
        colgrades_legtitle=pars["colgrades_legtitle"],
        colgrades_Ints=pars["colgrades_Ints"],
        colgrades_Inis="0",
        colgrades_Num="undefined",
        colgrades_userDefined=str(pars["colgrades_userDefined"]),
        legend="true",
        sliderlabel="undefined",
        mapStyles_weight="1",
        mapStyles_opacity="undefined",
        mapStyles_color="#000",
        mapStyles_fillOpacity="0.8",
        mapStyles_smoothFactor="1",
        mapStyles_radius="6",
        noDataColour="rgba(0, 0, 0, 0.3)",
        featurehltStyle_weight="3",
        featurehltStyle_color="#000",
        timeData_xlabs=str((dates.strftime("%d %b")).to_list()),
        timeData_timeseries=pars["timeData_timeseries"],
        timeData_CIname=ci_names,
        timeData_highlight="undefined",
        timeData_timeseriesMin="0",
        timeData_timeseriesMax=str(len(dates) - 1),
        timeData_timeseriesStep="1",
        meandata=(str(["null" for x in range(len(dates))])).replace("'", ""),
        timePlot_Background1Colour="#FFF",
        timePlot_Line1Colour="#FFF",
        timePlot_Background2Colour="#007ac3",
        timePlot_Line2Colour="#007ac3",
        timePlot_Background3Colour="undefined",
        timePlot_Line3Colour="undefined",
        timePlot_MarkerSize="3",
        timePlot_HighlightColour="#b41019",
        timePlot_HighlightSize="5",
        timePlot_ymax=pars["timePlot_ymax"],
        timePlot_beginYAtZero="true",
        layerMarker="undefined",
        mapBoundary="undefined",
        units_html=name.replace("_", " ").replace("1 0", "1.0"),
        units_unicode="predicted "
        + name.replace("_", " ").lower().replace("1 0", "1.0"),
        units_xlab="date from " + dates[0].strftime("%d %b %Y"),
        downloadData=url + "data/" + name + downloadData_ext,
        popupBox=pars["popupBox"],
    )
    return layer


def geo_round(match):
    return "{:.4f}".format(float(match.group()))


def make_age_imd_index_for_geo(df, name):
    df["age"] = df["age_imd"].str.split("_").str[0]
    df["age"] = df["age"].str.replace("+", "plus-", regex=False)
    df["age_init"] = df["age"].str.split("-").str[0]
    df["age_init"] = df["age_init"].str.replace("plus", "", regex=False)
    df["age_init"] = df["age_init"] + "plus"
    df["imd"] = df["age_imd"].str.split("_").str[1]
    df[name] = df[["imd", "age_init"]].agg("_".join, axis=1)
    df = df.drop(columns=["age_init", "age", "imd"])
    return df


def make_age_imd_col(df):
    df["age_imd"] = (
        df["age_group"].astype(str) + "_" + df["imd_decile"].astype(str)
    )
    df = df.drop(["age_group", "imd_decile"], axis=1)
    return df


def do_dha_things(
    df,
    num_weeks,
    cis,
    geo,
    web_folder_data,
    name,
    dha_format,
    dp,
    url,
    extra_df=None,
):
    """
    :param df: (dataframe)
    :param num_weeks: (int) number of weeks into the future for predictions: currently only works if num_weeks<10, if num_weeks>9 dataframe will be sorted incorrectly in dfwide2long()
    :param cis: (list) list of quantiles e.g. [0.05, 0.95]
    :geo: (geodataframe)
    :param web_folder_data: (str) output folder for geojson files e.g. "z:/dha_website_root/data/"
    :parma name: (str) name of layer
    :parma dha_format: (dict) dictionary of dha parameters: for details see dhaconfig at https://gitlab.com/achale/dhaconfig.git#egg=dhaconfig
    :param dp: (int) number of decimal places to keep
    :param url: (str) url of json and geojson data if it is external to the web server
    :param extra_df: (dataframe) additional dataframe for xlsx
    :return: (str) dha map layer as dictionary
    """
    df = make_age_imd_index_for_geo(df, "LAD code")
    df_tidy = summarydf_tidy(df, dp, num_weeks, cis)
    df_wide = dflong2wide(df_tidy, cis)
    geodf = make_geodf(geo, df_wide)
    utils.write_geojson(
        web_folder_data, name + ".geojson", geodf
    )  # remove bbox from geojson?
    if extra_df is None:
        write_csv(df_tidy, cis, web_folder_data, name)
        xlsx = False
    else:
        extra_df = extra_df.rename({"value": "mean"}, axis="columns")
        write_xls(extra_df, df, web_folder_data, name)
        xlsx = True
    dates = pd.DatetimeIndex(df_tidy["time"].unique())
    return make_layer(web_folder_data, name, dates, cis, dha_format, xlsx, url)


def summary_dha(input_files, output_folder, num_weeks, ci_list, config, url=""):
    """Draws together pipeline results into files for DHA

    :param input_files: (list) filename list [inferencedata_nc,
                                              insample7_nc
                                              insample14_nc,
                                              medium_term_nc,
                                              reproduction_number_nc,
                                              insample28_nc,
                                              insampleAll_nc]
    :param output_folder: (str) output folder e.g. "Z:/folder/"
    :param num_weeks: (int) number of weeks for predictions (untested for over 9 so beware ordering might break)
    :param ci_list: (list) list of quantiles e.g. [0.05, 0.95]
    :param config: SummaryGeopackage configuration information
    :param url: (str) url of json and geojson data if it is external to DHA host web server.
    """

    # initialise
    # shapely.speedups.disable()  # this line can be omitted on Linux
    data = xarray.open_dataset(input_files[0], group="constant_data")  # covars

    N = imdage_idx(data["N"])
    N = N.to_dataframe().reset_index()
    N = make_age_imd_col(N)

    cases = xarray.open_dataset(input_files[0], group="observations")["cases"]
    cases = imdage_idx(cases)  # .transpose()
    cases = cases.to_dataframe().reset_index()
    cases = make_age_imd_col(cases)

    dha_format = dha_format_dict()
    layers = {}
    output_folders = {
        "web_folder_data": Path(output_folder) / "data",
        "web_folder_js": Path(output_folder) / "js",
    }
    for k, v in output_folders.items():
        v.mkdir(parents=True, exist_ok=True)

    # geopackage: load and clean
    geo = gp.read_file("data/default.geojson")
    geo = geo.drop(columns=["0", "0.L", "0.U"])

    # Medium term absolute incidence
    name = default_name = "Daily_case_incidence"
    medium_term = xarray.open_dataset(input_files[3], group="predictions")
    medium_term = medium_term.rename({"age_imd_coords": "age_imd"})
    medium_df = xarray2summarydf(
        medium_term["events"][..., 2].reset_coords(drop=True)
    )
    layers[name] = do_dha_things(
        medium_df,
        num_weeks,
        ci_list,
        geo,
        output_folders["web_folder_data"],
        name,
        dha_format[name],
        1,
        url,
    )

    # Cumulative cases
    name = "Cumulative_case_incidence"
    medium_df = xarray2summarydf(
        medium_term["events"][..., 2].cumsum(dim="time").reset_coords(drop=True)
    )
    layers[name] = do_dha_things(
        medium_df,
        num_weeks,
        ci_list,
        geo,
        output_folders["web_folder_data"],
        name,
        dha_format[name],
        1,
        url,
    )

    # Medium term incidence per 100k
    name = "Daily_case_incidence_per_100k"
    medium_df = xarray2summarydf(
        (
            medium_term["events"][..., 2].reset_coords(drop=True)
            / np.array(N["N"])[np.newaxis, :, np.newaxis]
        )
        * 100000
    )
    layers[name] = do_dha_things(
        medium_df,
        num_weeks,
        ci_list,
        geo,
        output_folders["web_folder_data"],
        name,
        dha_format[name],
        1,
        url,
    )

    # Medium term prevalence
    name = "Prevalence_per_100k"
    prev_df = prevalence(medium_term, N)
    layers[name] = do_dha_things(
        prev_df,
        num_weeks,
        ci_list,
        geo,
        output_folders["web_folder_data"],
        name,
        dha_format[name],
        1,
        url,
    )

    # Rt
    name = "Reproduction_number_Rt"
    r_it = xarray.open_dataset(input_files[4], group="posterior_predictive")[
        "R_it"
    ]
    rt_summary = xarray2summarydf(r_it.isel(time=-1))
    rt_summary["time"] = r_it.coords["time"].data[-1] + np.timedelta64(1, "D")
    default_time = rt_summary.iloc[0]["time"]
    layers[name] = do_dha_things(
        rt_summary,
        0,
        ci_list,
        geo,
        output_folders["web_folder_data"],
        name,
        dha_format[name],
        2,
        url,
    )

    # Prob(Rt>1)
    name = "Prob_Rt_exceeds_1_0"
    rt = r_it.isel(time=-1).drop("time")
    rt_exceed = np.mean(rt > 1.0, axis=0)
    rt_exceed = (
        rt_exceed.to_dataframe()
        .reset_index()
        .rename({"R_it": "value"}, axis="columns")
        .round({"value": 2})
    )
    rt_exceed["time"] = default_time
    layers[name] = do_dha_things(
        rt_exceed,
        0,
        None,
        geo,
        output_folders["web_folder_data"],
        name,
        dha_format[name],
        2,
        url,
    )

    # Case exceedance - used by Insample
    exceed7 = case_exceedance((input_files[0], input_files[1]), 7)
    exceed14 = case_exceedance((input_files[0], input_files[2]), 14)
    exceed28 = case_exceedance((input_files[0], input_files[5]), 28)

    case_exceed = pd.DataFrame(
        {
            "Pr(pred<obs)_7": exceed7,
            "Pr(pred<obs)_14": exceed14,
            "Pr(pred<obs)_28": exceed28,
        },
        index=exceed7.coords["age_imd_coords"],
    )
    case_exceed = case_exceed.reset_index()
    case_exceed["time"] = default_time

    # Insample predictive incidence
    name = "Insample_7_days"
    insample = xarray.open_dataset(
        input_files[1], group="predictions"
    )  # insample7_Cases
    insample_df = xarray2summarydf(
        insample["events"][..., 2].reset_coords(drop=True)
    )
    insample_df_tidy = insample_tidy(
        insample_df,
        1,
        7,
        ci_list,
        cases.rename(columns={"age_imd": "age_imd_coords"}),
    )
    startdate7 = pd.DatetimeIndex(insample_df_tidy["time"])[0].strftime(
        " %d %b"
    )
    insample_df_tidy_geo = make_age_imd_index_for_geo(
        insample_df_tidy.rename(columns={"age_imd_coords": "age_imd"}),
        "LAD code",
    )
    geodf = make_geodf(geo, insample_df_tidy_geo)
    insample_write_json(
        geodf.drop(columns="geometry"),
        output_folders["web_folder_data"],
        ci_list,
        name,
    )
    case_exceed7 = case_exceed.drop(
        ["Pr(pred<obs)_14", "Pr(pred<obs)_28"], 1
    ).rename({"Pr(pred<obs)_7": "value"}, axis="columns")
    layers[name] = do_dha_things(
        case_exceed7.rename(columns={"age_imd_coords": "age_imd"}),
        0,
        None,
        geo,
        output_folders["web_folder_data"],
        name,
        dha_format[name],
        2,
        url,
        insample_df_tidy,
    )

    name = "Insample_14_days"
    insample = xarray.open_dataset(
        input_files[2], group="predictions"
    )  # insample14_Cases
    insample_df = xarray2summarydf(
        insample["events"][..., 2].reset_coords(drop=True)
    )
    insample_df_tidy = insample_tidy(
        insample_df,
        1,
        14,
        ci_list,
        cases.rename(columns={"age_imd": "age_imd_coords"}),
    )
    startdate14 = pd.DatetimeIndex(insample_df_tidy["time"])[0].strftime(
        " %d %b"
    )
    insample_df_tidy_geo = make_age_imd_index_for_geo(
        insample_df_tidy.rename(columns={"age_imd_coords": "age_imd"}),
        "LAD code",
    )
    geodf = make_geodf(geo, insample_df_tidy_geo)
    insample_write_json(
        geodf.drop(columns="geometry"),
        output_folders["web_folder_data"],
        ci_list,
        name,
    )
    case_exceed14 = case_exceed.drop(
        ["Pr(pred<obs)_7", "Pr(pred<obs)_28"], 1
    ).rename({"Pr(pred<obs)_14": "value"}, axis="columns")
    layers[name] = do_dha_things(
        case_exceed14.rename(columns={"age_imd_coords": "age_imd"}),
        0,
        None,
        geo,
        output_folders["web_folder_data"],
        name,
        dha_format[name],
        2,
        url,
        insample_df_tidy,
    )

    name = "Insample_28_days"
    insample = xarray.open_dataset(
        input_files[5], group="predictions"
    )  # insample28_Cases
    insample_df = xarray2summarydf(
        insample["events"][..., 2].reset_coords(drop=True)
    )
    insample_df_tidy = insample_tidy(
        insample_df,
        1,
        28,
        ci_list,
        cases.rename(columns={"age_imd": "age_imd_coords"}),
    )
    startdate28 = pd.DatetimeIndex(insample_df_tidy["time"])[0].strftime(
        " %d %b"
    )
    insample_df_tidy_geo = make_age_imd_index_for_geo(
        insample_df_tidy.rename(columns={"age_imd_coords": "age_imd"}),
        "LAD code",
    )
    geodf = make_geodf(geo, insample_df_tidy_geo)
    insample_write_json(
        geodf.drop(columns="geometry"),
        output_folders["web_folder_data"],
        ci_list,
        name,
    )
    case_exceed28 = case_exceed.drop(
        ["Pr(pred<obs)_7", "Pr(pred<obs)_14"], 1
    ).rename({"Pr(pred<obs)_28": "value"}, axis="columns")
    layers[name] = do_dha_things(
        case_exceed28.rename(columns={"age_imd_coords": "age_imd"}),
        0,
        None,
        geo,
        output_folders["web_folder_data"],
        name,
        dha_format[name],
        2,
        url,
        insample_df_tidy,
    )

    name = "RPS"
    (
        index_ordering,
        crps_m_t,
        crps_m,
        crps_m_median,
        crps_t,
        crps,
    ) = crps_wrapper((input_files[0], input_files[6]))
    # RPS_m_t, RPS_m, RPS_t, RPS = ranked_prob_score((input_files[0], input_files[6]))
    crps_m_df = pd.DataFrame(
        {"value": crps_m_median, "age_imd": index_ordering},
    )
    crps_m_df = df_split_index(crps_m_df).reset_index()
    crps_m_df = crps_m_df.sort_values(["age_group", "imd_decile"])
    crps_m_df = crps_m_df.set_index(["age_group", "imd_decile"])
    crps_m_df["time"] = default_time

    insample = xarray.open_dataset(
        input_files[6], group="predictions"
    )  # insample for all case time points
    insample_df = xarray2summarydf(
        insample["events"][..., 2].reset_coords(drop=True)
    )
    insample_df_tidy = insample_tidy(
        insample_df,
        2,
        insample.coords["time"].values.shape[0],
        ci_list,
        cases.rename(columns={"age_imd": "age_imd_coords"}),
    )  # data for timeseries plot
    # startdate = pd.DatetimeIndex(insample_df_tidy["time"])[0].strftime(" %d %b")
    insample_df_tidy_geo = make_age_imd_index_for_geo(
        insample_df_tidy.rename(columns={"age_imd_coords": "age_imd"}),
        "LAD code",
    )
    geodf = make_geodf(geo, insample_df_tidy_geo)
    insample_write_json(
        geodf.drop(columns="geometry"),
        output_folders["web_folder_data"],
        ci_list,
        name,
    )
    layers[name] = do_dha_things(
        crps_m_df,  # .rename(columns={"age_imd_coords": "age_imd"}),
        0,
        None,
        geo,
        output_folders["web_folder_data"],
        name,
        dha_format[name],
        2,
        url,
        insample_df_tidy,
    )

    # build config file
    postfix = pd.DatetimeIndex([default_time])[0].strftime(" %d %b")
    config_data = dha.event_listener(
        default_name.replace("_", " ") + postfix,
        postfix=postfix,
        startdate7=startdate7,
        startdate14=startdate14,  # startdate28 not added to event listener
    )
    config_data += dha.global_vars(
        radioNotDropdown="false",
        InitialMapCenterLatLng="[55.5, -2.7]",
        InitialmapZoom="5.5",
        mapUnits="false",
        secondMap="true",
        userDefinedBaseMaps="true",
    )
    config_data += dha.build_list_of_layers(
        [
            layers["Reproduction_number_Rt"],
            layers["Prob_Rt_exceeds_1_0"],
            layers["Prevalence_per_100k"],
            layers["Daily_case_incidence"],
            layers["Daily_case_incidence_per_100k"],
            layers["Cumulative_case_incidence"],
            layers["Insample_7_days"],
            layers["Insample_14_days"],
            layers["Insample_28_days"],
            layers["RPS"],
        ]
    )
    utils.write_text_file(
        output_folders["web_folder_js"], "allMetadata.js", config_data
    )

    # write file containing the current time-date
    utils.write_last_updated_time(
        output_folders["web_folder_js"], "lastupdated.js", "last updated: "
    )


if __name__ == "__main__":

    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument(
        "inferencedata",
        type=str,
        help="inference data case and covar data",
    )
    parser.add_argument(
        "insample7",
        type=str,
        help="insample 7 predictions",
    )
    parser.add_argument(
        "insample14",
        type=str,
        help="insample 14 predictions",
    )
    parser.add_argument(
        "insample28",
        type=str,
        help="insample 28 predictions",
    )
    parser.add_argument(
        "insampleAll",
        type=str,
        help="insample for all predictions",
    )
    parser.add_argument(
        "medium_term",
        type=str,
        help="medium term predictions",
    )
    parser.add_argument(
        "reproduction_number",
        type=str,
        help="reproduction number prediction",
    )
    parser.add_argument(
        "output_folder",
        type=str,
        help="root of web folder",
    )
    parser.add_argument(
        "num_weeks",
        type=int,
        help="number of forecast weeks",
    )
    parser.add_argument(
        "low_quantile",
        type=float,
        help="lower quantile",
    )
    parser.add_argument(
        "upper_quantile",
        type=float,
        help="upper quantile",
    )
    parser.add_argument(
        "config",
        type=str,
        help="config",
    )
    parser.add_argument(
        "url",
        type=str,
        help="url of web files if on a different server to the DHA",
    )
    args = parser.parse_args()

    summary_dha(
        input_files=[
            args.inferencedata,
            args.insample7,
            args.insample14,
            args.medium_term,
            args.reproduction_number,
            args.insample28,  # needs to be here othewise input_files[...] will be incorrect
            args.insampleAll,
        ],
        output_folder=args.output_folder,
        num_weeks=args.num_weeks,
        ci_list=[args.low_quantile, args.upper_quantile],
        config=args.config,
        url=args.url,
    )

"""
summary_dha(
    input_files=[
        "results/model1l_long/inferencedata.nc",
        "results/model1l_long/insample7.nc",
        "results/model1l_long/insample14.nc",
        "results/model1l_long/medium_term.nc",
        "results/model1l_long/reproduction_number.nc",
        "results/model1l_long/insample28.nc,
        "results/model1l_long/insampleAll.nc,
    ],
    output_folder="../website/",
    num_weeks=8,
    ci_list=[0.05, 0.95],
    config="",
    url="",
)

poetry run python -m covid19uk.posterior.make_dha \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/inferencedata.nc \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/insample7.nc \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/insample14.nc \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/insample28.nc \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/insampleAll.nc \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/medium_term.nc \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/reproduction_number.nc \
    "results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/" \
    8 \
    0.05 \
    0.95 \
    "" \
    ""

"""
