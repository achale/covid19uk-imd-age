import h5py
import matplotlib.pylab as pylab
import matplotlib.pyplot as plt
import numpy as np

from statsmodels.graphics.tsaplots import plot_acf

params = {
    "figure.figsize": (12, 7),
    # "figure.titlesize": "small",
    # "axes.labelsize": "xx-small",
    "axes.titlesize": "small",
    # "xtick.labelsize": "xx-small",
    # "ytick.labelsize": "xx-small",
}
pylab.rcParams.update(params)


def plot_samples(
    run_type,
    burnin,
    thin,
    alpha_t_time,
    samples,
    output_folder,
    thing,
    age_bands,
):
    if run_type == "quickrun_config.yaml":
        burnin = 2000
        thin = 1
    elif run_type == "config.yaml":
        burnin = 20000
        thin = 30
    elif run_type == "demo_config.yaml":
        burnin = 2000
        thin = 30
    else:
        pass  # use parameters passed into function

    post = h5py.File(samples, "r")
    sample_keys = list(post["samples"].keys())
    print(sample_keys)
    title_txt = "imd-model: "

    #
    # traces and autocorr
    #
    colors = plt.rcParams["axes.prop_cycle"]()
    fig, ax = plt.subplots(
        nrows=post["samples/" + thing][:].shape[1], ncols=2, sharex="col"
    )
    fig.suptitle(
        "sample traces and autocorrelation"
        + " - thinned by "
        + str(thin)
        + " samples"
    )
    nsam = len(post["samples/" + thing][:])
    print(post["samples/" + thing][:].shape)
    for i in range(post["samples/" + thing][:].shape[1]):
        print(i)
        c = next(colors)["color"]
        mean = np.mean(post["samples/" + thing][burnin:, i])
        init = post["samples/" + thing][0, i]
        print(thing, i, "(0) = ", init)
        y = post["samples/" + thing][burnin:, i][::thin]
        ax[i, 0].plot(range(burnin, nsam, thin), y, linewidth=0.5, color=c)
        ax[i, 0].set(ylabel=r"$" + "\\" + thing + "_{" + age_bands[i] + "}$")
        ax[i, 0].set_title(
            r"$mean("
            + "\\"
            + thing
            + "_{"
            + age_bands[i]
            + "}"
            + ") = "
            + str(round(np.mean(y), 4))
            + "$"
        )
        plot_acf(y, ax=ax[i, 1], color=c, markersize=3)
        ax[i, 1].set_title("")
        ax[i, 1].set(ylabel=r"$" + "\\" + thing + "_{" + age_bands[i] + "}$")
    print("i=", i)
    ax[i, 0].set(xlabel="samples")
    ax[i, 1].set(xlabel="lag")
    plt.tight_layout(rect=[0, 0, 1, 1])
    fig.savefig(
        output_folder
        + "diagnostic_traces_"
        + thing
        + "_burnin_"
        + str(burnin)
        + ".png"
    )
    plt.close(fig)


if __name__ == "__main__":

    import yaml
    import argparse

    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument(
        "config_fn",
        type=str,
        help="config file path to and filename",
    )
    parser.add_argument(
        "run_type",
        type=str,
        help="either quickrun_config.yaml or config.yaml overrules burnin, thin and alpha_t_time",
    )
    parser.add_argument(
        "burnin",
        type=int,
        help="number of burnin samples",
    )
    parser.add_argument(
        "thin",
        type=int,
        help="amount to thin samples - only applies timeseries figure",
    )
    parser.add_argument(
        "alpha_t_time",
        type=int,
        help="select a time point, if -1 mean(alpha_t) is plotted instead",
    )
    parser.add_argument(
        "posterior",
        type=str,
        help="posterior.hd5 predictions",
    )
    parser.add_argument(
        "output_folder",
        type=str,
        help="path to output folder ending in /",
    )

    parser.add_argument(
        "thing",
        type=str,
        help="name of parameter which is a vector e.g. a=[a1,a2,...,an]",
    )

    args = parser.parse_args()

    with open(args.config_fn, "r") as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    plot_samples(
        args.run_type,
        args.burnin,
        args.thin,
        args.alpha_t_time,
        args.posterior,
        args.output_folder,
        args.thing,
        config["ProcessData"]["CasesData"]["age_bands"],
    )


"""

poetry run python -m covid19uk.posterior.plot_samples_rho_and_psi \
    results/model7_21-08-16_Polymod_imd_suscept_rho_per_age_psi_per_age/model/config.yaml \
    none \
    0 \
    1 \
    50 \
    results/model7_21-08-16_Polymod_imd_suscept_rho_per_age_psi_per_age/posterior.hd5 \
    results/model7_21-08-16_Polymod_imd_suscept_rho_per_age_psi_per_age/ \
    rho

poetry run python -m covid19uk.posterior.plot_samples_rho_and_psi \
    results/model7_21-08-16_Polymod_imd_suscept_rho_per_age_psi_per_age/model/config.yaml \
    config.yaml \
    0 \
    0 \
    0 \
    results/model7_21-08-16_Polymod_imd_suscept_rho_per_age_psi_per_age/posterior.hd5 \
    results/model7_21-08-16_Polymod_imd_suscept_rho_per_age_psi_per_age/ \
    psi


"""
