""" Simlates imd switch using given model and previously fitted parameters """

import matplotlib.dates as mdates
import matplotlib.pylab as pylab
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import pandas as pd
import pickle as pkl
from pyparsing import col
import tensorflow as tf
import time
import xarray

from datetime import timedelta
from matplotlib.dates import DateFormatter

from covid19uk import model_spec
from covid19uk.data.util import imdage_idx
from covid19uk.data.util import imdage_idx_names
from covid19uk.posterior.make_dha import xarray2summarydf
from covid19uk.posterior.make_dha import crps_slow
from covid19uk.posterior.plot_fitted_results import df_split_index
from covid19uk.posterior.plot_fitted_results import plot_CI
from covid19uk.posterior.plot_raw_data import make_timeseries
from covid19uk.posterior.predict import predicted_incidence
from covid19uk.util import copy_nc_attrs

from gemlib.util import compute_state

# print(pylab.rcParams.keys())
params = {
    "figure.figsize": (14, 6),
    "legend.loc": "upper left",
    # "xtick.labelsize": "xx-small",
    # "ytick.labelsize": "xx-small",
    # "font.size": "12",  # comment out for supp. material
}
pylab.rcParams.update(params)


# def rms_error_numpy(x, y):
#    # RMS using Frobenius norm from np.linalg
#    return np.linalg.norm(x - y) / np.sqrt(len(x))


# def rms_error_xarray(x, y):
#    # RMS: sqrt(mean(abs(x-y)**2)) # can drop abs as no complex numbers here!
#    s = (x - y) ** 2
#    rms_breakdown = (s.mean(dim="time")) ** (1 / 2)
#    rms_total = (s.mean()) ** (1 / 2)
#    return rms_breakdown, rms_total


def xarray2summarydf_grouped(arr, thing, group):
    dd = arr.to_dataframe().reset_index()
    dd = df_split_index(dd).reset_index().drop(columns=["age_imd"])
    dd = dd.set_index(["iteration", "age_group", "imd_decile", "time"])
    dd = dd.to_xarray()["events"]
    dd = dd.sum(dim=thing)
    dd = xarray2summarydf(dd)
    dd = dd[[group, "time", "value", "0.05", "0.95"]]
    dd = dd.set_index([group, "time"]).to_xarray()
    return dd


def to_100k(data, N, thing, group):
    return data[thing] / N.sum(dim=group) * 1e5


def plot_simulations(
    count_imd,
    count_age,
    simulation_file,
    origin_date_str,
    N_xarray,
    output_folder,
    bias,
    log,
    subaxis,
    assortative,
    smoothed="smoothed_0",
):
    #
    # incidence per 100k
    #

    N = imdage_idx(N_xarray)
    N = N.to_dataframe().reset_index()
    N_xarray = N.set_index(["age_group", "imd_decile"]).to_xarray()["N"]

    sim = xarray.open_dataset(simulation_file, group="predictions")
    sim = sim.rename({"age_imd_coords": "age_imd"})
    sim_imd = xarray2summarydf_grouped(
        sim["events"][..., 2].reset_coords(drop=True), "age_group", "imd_decile"
    )
    sim_age = xarray2summarydf_grouped(
        sim["events"][..., 2].reset_coords(drop=True), "imd_decile", "age_group"
    )
    sim_imd_100k = to_100k(sim_imd, N_xarray, "value", "age_group")

    count_imd_100k = (
        count_imd.sel(
            time=slice(min(sim_imd["time"].values), max(sim_imd["time"].values))
        )
        / N_xarray.sum(dim="age_group")
        * 1e5
    )
    count_age_100k = (
        count_age.sel(
            time=slice(min(sim_imd["time"].values), max(sim_imd["time"].values))
        )
        / N_xarray.sum(dim="imd_decile")
        * 1e5
    )

    fig, ax = plt.subplots(ncols=2, sharex=True)
    make_timeseries(
        count_age_100k,
        "age_group",
        ax,
        0,
        log,
        linestyle=(0, (2, 2)),
    )
    make_timeseries(
        to_100k(sim_age, N_xarray, "value", "imd_decile"),
        "age_group",
        ax,
        0,
        log,
    )
    plot_CI(
        to_100k(sim_age, N_xarray, "0.05", "imd_decile"),
        to_100k(sim_age, N_xarray, "0.95", "imd_decile"),
        "age_group",
        ax,
        0,
        "#777777",
        0.2,
    )

    # X = to_100k(sim_imd, N_xarray, "value", "age_group")
    # rms_per_decile, rms_total = rms_error_xarray(count_imd_100k, X)

    make_timeseries(
        count_imd_100k,
        "imd_decile",
        ax,
        1,
        log,
        linestyle=(0, (2, 2)),
        smoothed=smoothed,
    )
    make_timeseries(
        sim_imd_100k,
        "imd_decile",
        ax,
        1,
        log,
        smoothed=smoothed,
    )
    plot_CI(
        to_100k(sim_imd, N_xarray, "0.05", "age_group"),
        to_100k(sim_imd, N_xarray, "0.95", "age_group"),
        "imd_decile",
        ax,
        1,
        "#BBBBBB",
        0.2,
        smoothed=smoothed,
    )

    if subaxis == "subaxis":
        start = min(sim_imd["time"].values)
        sub_axes_1 = plt.axes(
            [0.85, 0.42, 0.05, 0.5]
            # [0.61, 0.42, 0.05, 0.5]
        )
        detail = sim_imd_100k.sel(
            time=slice(start, start + np.timedelta64(1, "D"))
        )
        detail.plot.line(
            hue="imd_decile", ax=sub_axes_1, add_legend=False, marker="o"
        )
        sub_axes_1.axes.yaxis.set_visible(False)
        date_form = DateFormatter("%m-%d")
        sub_axes_1.xaxis.set_major_locator(mdates.DayLocator(interval=1))
        sub_axes_1.xaxis.set_major_formatter(date_form)
        sub_axes_1.set(xlabel=None)
        sub_axes_1.tick_params(axis="both", which="major", labelsize=12)

        sub_axes_2 = plt.axes(
            [0.93, 0.42, 0.05, 0.5]
            # [0.69, 0.42, 0.05, 0.5]
        )
        detail = sim_imd_100k.sel(
            time=slice(
                start + np.timedelta64(22, "D"),
                start + np.timedelta64(23, "D"),
                # start + np.timedelta64(45, "D"),
                # start + np.timedelta64(46, "D"),
            )
        )
        detail.plot.line(
            hue="imd_decile", ax=sub_axes_2, add_legend=False, marker="o"
        )
        sub_axes_2.axes.yaxis.set_visible(False)
        date_form = DateFormatter("%m-%d")
        sub_axes_2.xaxis.set_major_locator(mdates.DayLocator(interval=1))
        sub_axes_2.xaxis.set_major_formatter(date_form)
        sub_axes_2.set(xlabel=None)
        sub_axes_2.tick_params(axis="both", which="major", labelsize=12)

    ax[0].annotate(
        "data",
        xytext=(470, 405),
        xy=(440, 408),
        textcoords="figure points",
        xycoords="figure points",
        arrowprops=dict(arrowstyle="-", color="black", lw=1.5, ls=(0, (2, 2))),
    )
    ax[1].annotate(
        "prediction",
        xytext=(550, 405),
        xy=(520, 408),
        textcoords="figure points",
        xycoords="figure points",
        arrowprops=dict(arrowstyle="-", color="black", lw=1.5),
    )

    # fig.autofmt_xdate()
    fig.tight_layout()
    fig.autofmt_xdate()
    if assortative == "original":
        assortative = ""
    else:
        assortative = ""  # assortative = "_" + assortative
    plt.savefig(
        output_folder
        + "plot_simulated_timeseries_incidence_100k_bias_"
        + str(bias)
        + assortative
        + ".png"
    )
    plt.show()
    plt.close(fig)


def read_pkl(filename):
    with open(filename, "rb") as f:
        return pkl.load(f)


def simulate(
    data,
    posterior_samples,
    cumsums_file,
    counts_file,
    output_file,
    initial_step,
    num_steps,
    stratum_size,
    case_size,
    output_folder,
    bias,
    log,
    recovered_size,
    subaxis,
    sim_imd_contact_rates=[[1.0]],
    omega=-1.0,
    out_of_sample=False,
    assortative="original",
    smoothed="smoothed_0",  # no smoothing
    shrink_rho=-1.0,
    boost_beta=1.0,
):  # all time steps are used if initial_step = num_steps = 0

    if len(sim_imd_contact_rates) == 1:
        sim_imd_contact_rates = np.expand_dims(
            np.array(sim_imd_contact_rates), axis=1
        )
    else:
        tmp = np.zeros((len(sim_imd_contact_rates), len(sim_imd_contact_rates)))
        xx = np.zeros((len(sim_imd_contact_rates), len(sim_imd_contact_rates)))
        for a in range(0, len(sim_imd_contact_rates)):
            for b in range(0, len(sim_imd_contact_rates)):
                if b == a:
                    tmp[a, b] = sim_imd_contact_rates[b]
                if b > a:
                    if sim_imd_contact_rates[b] < sim_imd_contact_rates[a]:
                        tmp[a, b] = sim_imd_contact_rates[b]
                    else:
                        tmp[a, b] = sim_imd_contact_rates[a]
        np.fill_diagonal(xx, sim_imd_contact_rates)  # updates xx with diag els
        sim_imd_contact_rates = tmp + tmp.T - xx

        # original
        # sim_imd_contact_rates = np.triu(np.array(np.array(sim_imd_contact_rates)), 1).T + np.triu(np.array(np.array(sim_imd_contact_rates)))

        if assortative == "assortative":
            sim_imd_contact_rates = np.diag(np.diag(sim_imd_contact_rates))
        elif assortative == "not_assortative":
            sim_imd_contact_rates = sim_imd_contact_rates - np.diag(
                np.diag(sim_imd_contact_rates)
            )
        else:
            pass  # leave mixing matrix unchanged

        print("sim_imd_contact_rates:\n", sim_imd_contact_rates)

    sim_imd_contact_rates = sim_imd_contact_rates.tolist()

    cumsum_age_imd = xarray.open_dataset(cumsums_file, group="cumsum_age_imd")[
        "cumsum_age_imd"
    ].fillna(0)
    count_imd = xarray.open_dataset(counts_file, group="count_imd")["count_imd"]
    count_age = xarray.open_dataset(counts_file, group="count_age")["count_age"]
    count_age_imd = xarray.open_dataset(counts_file, group="count_age_imd")[
        "count_age_imd"
    ]

    covar_data = xarray.open_dataset(data, group="constant_data")
    cases = xarray.open_dataset(data, group="observations")

    samples = read_pkl(posterior_samples)
    initial_state_orig = samples["initial_state"]
    del samples["initial_state"]

    if initial_step == 0 and num_steps == 0:
        num_steps = samples["seir"].shape[-2]
        initial_step = -num_steps

    if initial_step < 0:
        initial_step = samples["seir"].shape[-2] + initial_step

    origin_date = np.array(cases.coords["time"][0])
    dates = np.arange(
        origin_date,
        origin_date + np.timedelta64(initial_step + num_steps, "D"),
        np.timedelta64(1, "D"),
    )
    print("initial_step", initial_step)
    print("num_steps", num_steps)
    print(initial_step + num_steps)

    covar_data["weekday"] = xarray.DataArray(
        (pd.to_datetime(dates).weekday < 5).astype(model_spec.DTYPE),
        coords=[dates],
        dims=["prediction_time"],
    )

    ###
    ### new stuff
    ###

    init_sim_date = origin_date + np.timedelta64(initial_step, "D")
    dim_imd = covar_data.coords["imd_decile"].shape[0]
    dim_age = covar_data.coords["age_group"].shape[0]
    age_group_coords = np.array(covar_data.coords["age_group"].values).astype(
        object
    )
    age_group = np.repeat(age_group_coords, dim_imd)
    imd_decile = np.tile(range(1, dim_imd + 1), dim_age)

    if stratum_size != 0:
        covar_data["N"] = (
            pd.DataFrame(
                {
                    "imd_decile": imd_decile,
                    "age_group": age_group,
                    "N": np.ones(dim_imd * dim_age) * stratum_size,
                }
            )
            .set_index(["imd_decile", "age_group"])
            .to_xarray()
        )["N"]

    if bias == 1:  # highest current incidence imd==10
        init_imd = np.tile(
            range(case_size, (dim_imd + 1) * case_size, case_size), dim_age
        )
    if bias == 2:  # highest current incidence imd==1
        init_imd = np.tile(range(dim_imd * case_size, 0, -case_size), dim_age)

    if recovered_size > 0:
        if bias == 1:  # highest recovered in imd 10
            recover_imd = np.tile(
                range(
                    recovered_size,
                    (dim_imd + 1) * recovered_size,
                    recovered_size,
                ),
                dim_age,
            )
        if bias == 2:  # highest recovered in imd 1
            recover_imd = np.tile(
                range(
                    dim_imd * recovered_size,
                    0,
                    -recovered_size,
                ),
                dim_age,
            )
    elif recovered_size == 0:
        recover_imd = np.zeros(dim_age * dim_imd)
    elif recovered_size < 0:
        recover_imd = (
            imdage_idx(
                cumsum_age_imd.sel(
                    time=str(np.datetime_as_string(init_sim_date, unit="D"))
                )
            )
            .to_dataframe()
            .reset_index()["cumsum_age_imd"]
            .to_numpy()
        )
        print("init_sim_date", init_sim_date)

    if recovered_size >= 0:
        initial_state = np.array(
            [
                np.ones(dim_age * dim_imd) * (stratum_size - 2 * init_imd)
                - recover_imd,
                np.ones(dim_age * dim_imd) * init_imd,
                np.ones(dim_age * dim_imd) * init_imd,
                recover_imd,
            ]
        ).transpose()
        initial_state = np.tile(
            initial_state, (samples["seir"].shape[-4], 1, 1)
        )
    elif recovered_size < 0:
        posterior_state = compute_state(
            initial_state_orig,
            samples["seir"],
            model_spec.STOICHIOMETRY,
        )
        estimated_initial_state = posterior_state[..., initial_step, :]
        remove_cumsum = np.array(
            [
                -recover_imd * (-recovered_size),
                np.zeros(dim_age * dim_imd),
                np.zeros(dim_age * dim_imd),
                recover_imd * (-recovered_size),
            ]
        ).transpose()
        if recovered_size < -1000:
            remove_cumsum = np.zeros_like(remove_cumsum)
        initial_state = estimated_initial_state + remove_cumsum

    ###
    ### END new stuff
    ###

    N = imdage_idx(covar_data["N"])
    covar_data_ = {
        "C": covar_data["C"],
        "N": N,
        "weekday": covar_data["weekday"],
        "IMD": covar_data.coords["imd_decile"].astype(model_spec.DTYPE),
    }

    user_init_state = True  # always use True for simulations in this function
    with tf.device("CPU"):  # TODO: work out effect GPU solution
        estimated_init_state, predicted_events = predicted_incidence(
            samples,
            initial_state,
            covar_data_,
            initial_step,
            num_steps,
            out_of_sample,
            user_init_state=user_init_state,
            sim_imd_contact_rates=sim_imd_contact_rates,
            t_shift=initial_step,  # for t=0,1,...,T in model spec
            omega=omega,
            shrink_rho=shrink_rho,
            boost_beta=boost_beta,
        )

    age_imd_coords = imdage_idx_names(covar_data)
    prediction = xarray.DataArray(
        predicted_events.numpy(),
        coords=[
            np.arange(predicted_events.shape[0]),
            age_imd_coords,
            dates[initial_step:],
            np.arange(predicted_events.shape[3]),
        ],
        dims=("iteration", "age_imd_coords", "time", "event"),
    )

    if user_init_state == False:
        estimated_init_state = estimated_init_state.numpy()

    estimated_init_state = xarray.DataArray(
        estimated_init_state,
        coords=[
            np.arange(estimated_init_state.shape[0]),
            age_imd_coords,
            np.arange(estimated_init_state.shape[-1]),
        ],
        dims=("iteration", "age_imd_coords", "state"),
    )
    ds = xarray.Dataset(
        {"events": prediction, "initial_state": estimated_init_state}
    )
    ds.to_netcdf(output_file, group="predictions")
    ds.close()
    copy_nc_attrs(data, output_file)

    plot_simulations(
        count_imd,
        count_age,
        output_file,
        str(np.datetime_as_string(init_sim_date, unit="D")),
        covar_data["N"],
        output_folder,
        bias,
        log,
        subaxis,
        assortative,
        smoothed,
    )

    def crps_wrapper(count_age_imd, pred):
        data = imdage_idx(count_age_imd).transpose()
        data = data.sel(
            time=slice(
                min(pred.coords["time"].values),
                max(pred.coords["time"].values),
            )
        )
        data = data.to_dataframe().reset_index()
        data["imd_decile"] = data["imd_decile"].astype(int)
        data = data.sort_values(["age_group", "imd_decile", "time"])
        # data["imd_decile"] = data["imd_decile"].astype(str).str.zfill(2)
        data["age_imd_coords"] = (
            data["age_group"] + "_" + data["imd_decile"].astype(str)
        )
        data = data.drop(columns=["imd_decile", "age_group"]).rename(
            {"count_age_imd": "cases"}, axis="columns"
        )
        data = data.set_index(["age_imd_coords", "time"])
        data = data.to_xarray()  # dataset

        pred = pred["events"][..., -1]  # array
        pred = pred.to_dataset()

        index_ordering, crps_m_t = crps_slow(data, pred)

        return crps_m_t

    crps_m_t = crps_wrapper(count_age_imd, ds)
    print(
        "CRPS (0.25, 0.5, 0.75) quantiles: ",
        np.round(np.quantile(crps_m_t, [0.25, 0.5, 0.75]), 1),
    )


if __name__ == "__main__":

    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument(
        "-i", "--initial-step", type=int, default=0, help="Initial step"
    )
    parser.add_argument(
        "-n", "--num-steps", type=int, default=1, help="Number of steps"
    )
    parser.add_argument(
        "-o",
        "--out-of-sample",
        action="store_true",
        help="Out of sample prediction (sample alpha_t)",
    )
    parser.add_argument("data_pkl", type=str, help="Covariate data nc")
    parser.add_argument(
        "cumsums_file",
        type=str,
        help="cumsums nc",
    )
    parser.add_argument(
        "counts_file",
        type=str,
        help="path to counts.nc",
    )
    parser.add_argument(
        "posterior_samples_pkl",
        type=str,
        help="Posterior samples pickle",
    )
    parser.add_argument(
        "output_file",
        type=str,
        help="Output pkl file",
    )
    parser.add_argument(
        "output_folder",
        type=str,
        help="Output folder for figure",
    )
    parser.add_argument(
        "stratum_size", type=int, default=1000, help="stratum_size"
    )
    parser.add_argument(
        "recovered_size",
        type=int,
        default=0,
        help="number of recovered individuals",
    )
    parser.add_argument("case_size", type=int, default=1, help="case_size")
    parser.add_argument(
        "log",
        type=str,
        help="log or lin scale for figs",
    )
    parser.add_argument(
        "subaxis",
        type=str,
        help="put subaxis on figs",
    )
    parser.add_argument("sim_imd_contact_rates", nargs="+", type=float)
    parser.add_argument("omega", nargs="+", type=float)
    parser.add_argument(
        "bias",
        type=int,
        default=1,
        help="imd bias towards low or high IMD deciles",
    )
    parser.add_argument(
        "assortative",
        type=str,
        help="make completely 'assortative' (diag only), make completely 'not_assortative' (remove diag), otherwise 'original' whatever mixing matrix is defined in code",
    )
    parser.add_argument(
        "smoothed",
        type=str,
        help="smoothing on figs. If 'smoothed_0' no smoothing else 'smoothed_number' using sma with window of length 'number'",
    )
    parser.add_argument("shrink_rho", nargs="+", type=float)
    parser.add_argument("boost_beta", nargs="+", type=float)
    args = parser.parse_args()

    simulate(
        args.data_pkl,
        args.posterior_samples_pkl,
        args.cumsums_file,
        args.counts_file,
        args.output_file,
        args.initial_step,
        args.num_steps,
        args.stratum_size,
        args.case_size,
        args.output_folder,
        args.bias,
        args.log,
        args.recovered_size,
        args.subaxis,
        args.sim_imd_contact_rates,
        args.omega,
        args.out_of_sample,
        args.assortative,
        args.smoothed,
        args.shrink_rho,
        args.boost_beta,
    )  # run one simulation


"""
### Example of switching imd 10 from low to high through depletion of susceptibles
### note if there's no depletion of susceptibles this effect is not seen
poetry run python -m covid19uk.posterior.simulate_imd_switch \
    -i 0 \
    -n 42 \
    -o \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/inferencedata.nc \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/cumsums.nc \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/thin_samples.pkl \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/simulation.nc \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/ \
    800000 \
    20000 \
    15 \
    lin \
    '' \
    1.0 \
    -1.0 \
    2
    original

# same as above but end of posterior samples i.e. -i -1
poetry run python -m covid19uk.posterior.simulate_imd_switch \
    -i -1 \
    -n 42 \
    -o \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/inferencedata.nc \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/cumsums.nc \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/thin_samples.pkl \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/simulation.nc \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/ \
    800000 \
    20000  \
    15 \
    lin \
    '' \
    1.0 \
    -1.0 \
    2
    original

### use actual population sizes and predicted samples posterior (from thin_samples.pkl)
### use actual cumsum of cases from begining of pandemic to compute depletion of susceptibles
### increase depletion of susceptibles
poetry run python -m covid19uk.posterior.simulate_imd_switch \
    -i -42 \
    -n 42 \
    -o \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/inferencedata.nc \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/cumsums.nc \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/thin_samples.pkl \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/simulation.nc \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/ \
    0 \
    -4 \
    15 \
    lin \
    subaxis \
    1.0 \
    -1.0 \
    2
# -42      : initial-step : starting point of simulation relative to end of predicted samples (from thin_samples.pkl)
# 42       : num-steps : number in days to simulation forward
# 0        : stratum_size : use actual stratum population sizes when stratum_size=0
# -4       : recovered_size : increase depletion of susceptibles (from cumsum) by factor of 3 (nb. recovered_size is multiplied by -1 to make a factor of 3)
# 15       : case_size : artifical case size 15 isn't used
# lin      : log : linear scale on plot
# subaxis  : subaxis : 'subaxis' means include inset plots on subaxes
# increase mixing of upper imd bands  1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 - not used here
# 0.0      : time shift relavent to time-varying IMD mixing dynamics
# -1.0     : negative values give no time-varying dynamics to IMD mixing matrix
# 2        : bias : direction of artificial case size bias isn't used (i.e. initially more cases with IMD 1)
# original : mixing matrix as defined in code above
# smoothed_x: smooth plotted figures by x days, for no smoothing use smoothed_0
# shrink_rho: default = -1.0, used for time varying rho
# boost_beta: default = 1.0 increase S->E hazard rate by this amount

###############################################################


### used in paper - depletion of susceptibles
    poetry run python -m covid19uk.posterior.simulate_imd_switch     -i -1     -n 28     -o     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/inferencedata.nc     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/cumsums.nc  results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/counts.nc   results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/thin_samples.pkl     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/simulation.nc     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/     0     -5     15     lin     no_subaxis     1.0  -1.0   2 original  smoothed_0 -1.0  1.0


### used in paper -  differential imd-structured social mixing - full matrix: M = U + U^T - diag(U)
    ### full matrix - with cumsum (not used!)
        poetry run python -m covid19uk.posterior.simulate_imd_switch     -i -1     -n 56     -o     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/inferencedata.nc     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/cumsums.nc   results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/counts.nc  results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/thin_samples.pkl     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/simulation.nc     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/     0     -1     15     lin     subaxis     12.0 13 15 20 25 32 39 49 75 180   0.00123   2 original   smoothed_14 -1.0  1.0
    ### full matrix - without cumsum (used in paper)
        poetry run python -m covid19uk.posterior.simulate_imd_switch     -i -1     -n 56     -o     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/inferencedata.nc     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/cumsums.nc   results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/counts.nc  results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/thin_samples.pkl     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/simulation.nc     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/     0     -1111     15     lin     subaxis     6 8 11 16 23 35 41 55 75 141   0.00085   2 original   smoothed_14 -1.0  1.0

    ### NOTE THAT IN PAPER plot_simulations() IS EDITED - SEE COMMENTS IN THIS FUN AT TOP OF THIS SCRIPT
    ### NOTE that time lag of 10 days is presently hard coded(!) into model_spec.py

### used in supp matt - differential imd-structured social mixing
    ### assortative - diag only else 0 i.e. M = diag(U)
        ### without cumsum
        poetry run python -m covid19uk.posterior.simulate_imd_switch     -i -1     -n 56     -o     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/inferencedata.nc     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/cumsums.nc  results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/counts.nc   results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/thin_samples.pkl     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/simulation.nc     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/     0     -1111     15     lin     no_subaxis     65 85 115 150 190 250 270 300 330 370   0.00085   2 assortative   smoothed_0 -1.0  1.0

    ### disassortative - diag=0 else usual numbers i.e. M = U + U^T - 2 * diag(U)
        ### without cumsum
        poetry run python -m covid19uk.posterior.simulate_imd_switch     -i -1     -n 56     -o     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/inferencedata.nc     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/cumsums.nc  results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/counts.nc   results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/thin_samples.pkl     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/simulation.nc     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/     0     -1111     15     lin     no_subaxis     6 8 11 17 26 40 53 73 169 169   0.00085   2 not_assortative   smoothed_0 -1.0  1.0
        ### note that u_{10,10} = u_{9,9} this is a pecularity of the algorithm that sets up the mixing matrix because in actuality u_{10,10}=0


### used in paper - differential behavioural using Chi term
    ### without boosting beta (used in supp matt)
        poetry run python -m covid19uk.posterior.simulate_imd_switch     -i -1     -n 56     -o     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/inferencedata.nc     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/cumsums.nc  results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/counts.nc   results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/thin_samples.pkl     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/simulation.nc     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/     0     -1111     15     lin     no_subaxis     1.0  -1.0   2 original   smoothed_0  0.017  1.0
    ### with boosting beta (used in paper)
        poetry run python -m covid19uk.posterior.simulate_imd_switch     -i -1     -n 56     -o     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/inferencedata.nc     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/cumsums.nc  results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/counts.nc   results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/thin_samples.pkl     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/simulation.nc     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/     0     -1111     15     lin     subaxis     1.0  -1.0   2 original   smoothed_14  0.017  1.265

### used in paper
    ### no effects - default settings - just forward simulating from fitted model - plot is in "Model predictions" section 
        poetry run python -m covid19uk.posterior.simulate_imd_switch     -i -1     -n 56     -o     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/inferencedata.nc     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/cumsums.nc  results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/counts.nc   results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/thin_samples.pkl     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/simulation.nc     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/     0     -1111     15     lin     no_subaxis     1.0  -1.0   2 original   smoothed_0  -1.0  1.0
    ### no effects - but with a 26.5% boost - plotted in supp matterial
        poetry run python -m covid19uk.posterior.simulate_imd_switch     -i -1     -n 56     -o     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/inferencedata.nc     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/cumsums.nc  results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/counts.nc   results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/thin_samples.pkl     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/simulation.nc     results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/     0     -1111     15     lin     no_subaxis     1.0  -1.0   2 original   smoothed_0  -1.0  1.265

###



"""
