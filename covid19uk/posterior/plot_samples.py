import h5py
import matplotlib.pylab as pylab
import matplotlib.pyplot as plt
import numpy as np

from statsmodels.graphics.tsaplots import plot_acf

params = {
    "figure.figsize": (12, 7),
    # "figure.titlesize": "small",
    # "axes.labelsize": "xx-small",
    "axes.titlesize": "small",
    # "xtick.labelsize": "xx-small",
    # "ytick.labelsize": "xx-small",
}
pylab.rcParams.update(params)


def plot_samples(run_type, burnin, thin, alpha_t_time, samples, output_folder):
    if run_type == "quickrun_config.yaml":
        burnin = 2000
        thin = 1
        alpha_t_time = 50
    elif run_type == "config.yaml":
        burnin = 20000
        thin = 30
        alpha_t_time = 50
    elif run_type == "demo_config.yaml":
        burnin = 2000
        thin = 30
        alpha_t_time = 15
    else:
        pass  # use parameters passed into function

    post = h5py.File(
        samples, "r"
    )  # "r" for readonly and post.keys() to list keys
    sample_keys = list(post["samples"].keys())
    title_txt = "imd-model: "
    if alpha_t_time == -1:
        alpha_t_time = None

    #
    # traces and autocorr
    #
    remove_list = [-1]
    for i in range(len(sample_keys) - 1):
        k = sample_keys[i]
        if k == "alpha_t":
            pass
        else:
            y = post["samples/" + k][burnin:][::thin]
            if y.ndim > 1:
                remove_list.append(i)

    colors = plt.rcParams["axes.prop_cycle"]()
    fig, ax = plt.subplots(
        nrows=len(sample_keys) - len(remove_list), ncols=2, sharex="col"
    )
    fig.suptitle(
        "sample traces and autocorrelation"
        + " - thinned by "
        + str(thin)
        + " samples"
    )
    j = 0
    for i in range(len(sample_keys) - 1):
        c = next(colors)["color"]
        k = sample_keys[i]
        nsam = len(post["samples/" + k][:])
        # mean = np.mean(post["samples/" + k][burnin:])
        # print("mean(", k, ") = ", mean)
        init = post["samples/" + k][0]
        print(k, "(0) = ", init)
        if k == "alpha_t":
            if alpha_t_time is None:  # plot alpha at t=alpha_t_time
                y = np.mean(post["samples/alpha_t"][burnin:], axis=-1)[::thin]
                ax[j, 0].set(ylabel=r"$mean(" + "\\" + k + ")$")
                ax[j, 1].set(ylabel=r"$mean(" + "\\" + k + ")$")
            else:  # plot mean alpha
                y = post["samples/alpha_t"][burnin:, alpha_t_time][::thin]
                ax[j, 1].set(
                    ylabel=r"$" + "\\" + k + "$" + " for t=" + str(alpha_t_time)
                )
                ax[j, 0].set(
                    ylabel=r"$" + "\\" + k + "$" + " for t=" + str(alpha_t_time)
                )
            ax[j, 0].plot(range(burnin, nsam, thin), y, linewidth=0.3, color=c)
            ax[j, 0].set_title(
                r"$mean("
                + "\\"
                + k
                + ") = "
                + str(round(np.mean(y), 4))
                + "$"
                + " for t="
                + str(alpha_t_time)
            )
            plot_acf(y, ax=ax[j, 1], color=c, markersize=3)
            ax[j, 1].set_title("")
            j += 1
        elif i not in remove_list:
            print("\nj=", j)
            print("k=", k, "\n")
            y = post["samples/" + k][burnin:][::thin]
            ax[j, 0].plot(
                range(burnin, nsam, thin),
                y,
                linewidth=0.3,
                color=c,
            )
            if "gamma" in k:
                k = k[:-1] + "_" + k[-1]
            ax[j, 0].set(ylabel=r"$" + "\\" + k + "$")
            ax[j, 0].set_title(
                r"$mean(" + "\\" + k + ") = " + str(round(np.mean(y), 4)) + "$"
            )
            plot_acf(y, ax=ax[j, 1], color=c, markersize=3)
            ax[j, 1].set_title("")
            ax[j, 1].set(ylabel=r"$" + "\\" + k + "$")
            j += 1
    ax[j - 1, 0].set(xlabel="samples")
    ax[j - 1, 1].set(xlabel="lag")
    plt.tight_layout(rect=[0, 0, 1, 1])
    fig.savefig(
        output_folder + "diagnostic_traces_burnin" + str(burnin) + ".png"
    )
    plt.close(fig)

    #
    # pair plots
    #
    colors = plt.rcParams["axes.prop_cycle"]()
    fig, ax = plt.subplots(
        nrows=len(sample_keys) - 1, ncols=len(sample_keys) - 1
    )
    fig.suptitle(title_txt + "pair plot")
    for i in range(len(sample_keys) - 1):
        c = next(colors)["color"]
        ki = sample_keys[i]
        if ki == "alpha_t":
            if alpha_t_time is None:
                x = np.mean(
                    post["samples/alpha_t"][burnin:], axis=-1
                )  # [::thin]
            else:
                x = post["samples/alpha_t"][burnin:, alpha_t_time]  # [::thin]
        else:
            x = post["samples/" + ki][burnin:]  # [::thin]
        for j in range(len(sample_keys) - 1):
            kj = sample_keys[j]
            if i == j:
                ax[i, j].axis("off")
            else:
                if kj == "alpha_t":
                    if alpha_t_time is None:
                        y = np.mean(
                            post["samples/alpha_t"][burnin:], axis=-1
                        )  # [::thin]
                    else:
                        y = post["samples/alpha_t"][
                            burnin:, alpha_t_time
                        ]  # [::thin]
                else:
                    y = post["samples/" + kj][burnin:]  # [::thin]
                if x.ndim > 1 or y.ndim > 1:
                    continue
                ax[i, j].scatter(x, y, s=0.01, color=c)
                ax[i, j].set(xlabel=ki, ylabel=kj)
                ax[i, j].set_aspect("equal")
    plt.tight_layout(rect=[0, 0, 1, 1])
    fig.savefig(
        output_folder
        + "diagnostic_correlation_pairwise_burnin"
        + str(burnin)
        + ".png"
    )
    plt.close(fig)

    #
    # alpha_t
    #
    CI_low = np.quantile(post["samples/alpha_t"][:], 0.025, axis=-2)
    CI_upp = np.quantile(post["samples/alpha_t"][:], 0.975, axis=-2)
    y = np.mean(post["samples/alpha_t"][:], axis=-2)
    x = range(post["samples/alpha_t"][:].shape[1])
    plt.figure()
    plt.plot(y, marker=".", label="mean")
    plt.fill_between(
        x, CI_low, CI_upp, color="b", alpha=0.2, label="95% credible interval"
    )
    plt.xlabel("time in days over training data period")
    plt.ylabel(r"$\alpha_t$")
    plt.legend()
    plt.savefig(
        output_folder + "diagnostic_alpha_t_burnin" + str(burnin) + ".png"
    )
    plt.close(fig)


if __name__ == "__main__":

    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument(
        "run_type",
        type=str,
        help="either quickrun_config.sh or config.sh overrules burning, thin and alpha_t_time",
    )
    parser.add_argument(
        "burnin",
        type=int,
        help="number of burnin samples",
    )
    parser.add_argument(
        "thin",
        type=int,
        help="amount to thin samples - only applies timeseries figure",
    )
    parser.add_argument(
        "alpha_t_time",
        type=int,
        help="select a time point, if -1 mean(alpha_t) is plotted instead",
    )
    parser.add_argument(
        "posterior",
        type=str,
        help="posterior.hd5 predictions",
    )
    parser.add_argument(
        "output_folder",
        type=str,
        help="path to output folder ending in /",
    )

    args = parser.parse_args()

    plot_samples(
        args.run_type,
        args.burnin,
        args.thin,
        args.alpha_t_time,
        args.posterior,
        args.output_folder,
    )


"""

poetry run python -m covid19uk.posterior.plot_samples \
    config.yaml \
    0 \
    30 \
    50 \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/posterior.hd5 \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/

poetry run python -m covid19uk.posterior.plot_samples \
    config.yaml \
    20000 \
    30 \
    50 \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/posterior.hd5 \
    results/model7_21-08-30_Polymod_imd_suscept_rho_per_age_psi_per_age/

"""
