"""Covid data adaptors and support code"""

from covid19uk.data.case_data import CasesData
from covid19uk.data.polymod import read_polymod
from covid19uk.data.pop_by_imd_age import read_population

__all__ = [
    "CasesData",
    "read_polymod",
    "read_population",
]
