import pandas as pd
import xarray


def read_polymod(path):
    """Reads socialmixr CSV containing polymod data
    :param path: CSV file
    :returns: xarray with age_group and contact_age_group coordiantes
    """
    polymod = pd.read_csv(path)
    polymod = polymod.rename({"Unnamed: 0": "age_group"}, axis=1)
    polymod = polymod.set_index("age_group")
    polymod.index = polymod.index.map(lambda x: str(x)[1:-1])
    polymod = polymod.rename(index={"0": "70+"})
    polymod = polymod.rename(columns=lambda x: str(x)[19:-1])
    polymod = polymod.rename(columns={"0": "70+"})

    # normalise such that polymod.sum().sum()==1.0
    # normalisation_constant = polymod.to_numpy().sum()
    # polymod = polymod.multiply(1.0 / normalisation_constant)

    return xarray.DataArray(
        polymod,
        dims=["age_group", "contact_age_group"],
        name="polymod",
    )
