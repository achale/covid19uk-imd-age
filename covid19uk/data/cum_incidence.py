import pandas as pd

from covid19uk.data.case_data import CasesData


def get_cum_incidence(config, output_folder):

    settings = config["CasesData"]

    fn = settings["address"]
    df = CasesData.getCSV(fn)

    pillars = settings["pillars"]
    age_bins = settings["age_bins"]
    age_bands = settings["age_bands"]

    # Clean missing values
    df = df[["specimen_date", "lab_report_date", "pillar", "age", "imd_decile"]]
    df.dropna(inplace=True)

    # Clean time formats
    df["specimen_date"] = pd.to_datetime(df["specimen_date"], dayfirst=True)
    min_date = min(df["specimen_date"])
    max_date = max(df["specimen_date"])

    # Clean pillar values - different versions of data use different convensions!
    df["pillar"] = df["pillar"].str.replace("PILLAR1", "Pillar 1")
    df["pillar"] = df["pillar"].str.replace("PILLAR2", "Pillar 2")

    filters = df["pillar"].isin(pillars)
    df = df[filters]
    df = df.drop(columns="pillar")  # No longer need pillar column

    # make 10 year age bands
    df["age_group"] = pd.cut(
        df["age"], bins=age_bins, labels=age_bands, right=True
    )
    df = df.drop(columns="age")  # No longer need age column

    # tidy column
    df = df.dropna(subset=["age_group", "imd_decile"])
    df = df.astype({"imd_decile": int})

    # compute cumsum
    def cumsums(
        min_date, max_date, dd, thing, drop, indexes, name, mulitIndex=True
    ):
        dd = dd.drop(drop, axis=1)
        dd = dd.groupby([thing, "specimen_date"]).count()
        dd = dd.rename(columns={"lab_report_date": "cases"})
        dd.index.names = [thing, "time"]
        dd = dd.sort_index()

        if mulitIndex == True:
            dates = pd.date_range(min_date, max_date, closed="left")
            multi_indexes = pd.MultiIndex.from_product(
                [indexes, dates],
                names=[thing, "time"],
            )
            dd = dd["cases"].reindex(multi_indexes, fill_value=0)
            dd = dd.fillna(0)  # remove remaining nan's!
            dd = dd.sort_index()

        dd = dd.reset_index()
        dd[name] = dd.groupby([thing])["cases"].cumsum()
        dd = dd.drop(["cases"], axis=1)
        return dd

    cumsum_imd = cumsums(
        min_date,
        max_date,
        df,
        "imd_decile",
        "age_group",
        list(range(1, 11)),
        "cumsum_imd",
    )
    cumsum_imd = cumsum_imd.set_index(["imd_decile", "time"])
    cumsum_imd = cumsum_imd.sort_index()

    cumsum_age = cumsums(
        min_date,
        max_date,
        df,
        "age_group",
        "imd_decile",
        age_bands,
        "cumsum_age",
    )
    cumsum_age = cumsum_age.set_index(["age_group", "time"])
    cumsum_age = cumsum_age.sort_index()

    df["age_imd"] = (
        df["age_group"].astype(str) + "_" + df["imd_decile"].astype(str)
    )
    cumsum_age_imd = cumsums(
        min_date,
        max_date,
        df,
        "age_imd",
        ["age_group", "imd_decile"],
        df["age_imd"],
        "cumsum_age_imd",
        False,
    )
    cumsum_age_imd[["age_group", "imd_decile"]] = cumsum_age_imd[
        "age_imd"
    ].str.split("_", expand=True)
    cumsum_age_imd = cumsum_age_imd.drop("age_imd", axis=1)
    cumsum_age_imd = cumsum_age_imd.astype({"imd_decile": int})
    cumsum_age_imd = cumsum_age_imd.set_index(
        ["age_group", "imd_decile", "time"]
    )
    cumsum_age_imd = cumsum_age_imd.sort_index()
    cumsum_age_imd = cumsum_age_imd.fillna(0)

    cumsum_imd = cumsum_imd.to_xarray()
    cumsum_age = cumsum_age.to_xarray()
    cumsum_age_imd = cumsum_age_imd.to_xarray()

    fn = output_folder + "cumsums.nc"
    cumsum_imd.to_netcdf(fn, group="cumsum_imd", mode="w")
    cumsum_age.to_netcdf(fn, group="cumsum_age", mode="a")
    cumsum_age_imd.to_netcdf(fn, group="cumsum_age_imd", mode="a")


if __name__ == "__main__":

    from argparse import ArgumentParser
    import yaml

    parser = ArgumentParser(description="get data cumsum data")
    parser.add_argument("config_file", help="Global config file")
    parser.add_argument("output_folder", help="output file for cumsum data")
    args = parser.parse_args()

    with open(args.config_file, "r") as f:
        global_config = yaml.load(f, Loader=yaml.FullLoader)

    get_cum_incidence(global_config["ProcessData"], args.output_folder)


"""
poetry run python -m covid19uk.data.cum_incidence \
    config.yaml \
    results/model7_21-09-27_Polymod_imd_suscept_rho_per_age_psi_per_age/
"""
