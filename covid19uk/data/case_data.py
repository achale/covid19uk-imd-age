"""Loads COVID-19 case data"""

import time
from warnings import warn
import requests
import json
import pandas as pd

from covid19uk.data.util import (
    invalidInput,
    get_date_low_high,
)


class CasesData:
    def get(config):
        """
        Retrieve a pandas DataFrame containing the cases/line list data.
        """
        settings = config["CasesData"]
        if settings["input"] == "url":
            df = CasesData.getURL(settings["address"], config)
        elif settings["input"] == "csv":
            print(
                "Reading case data from local CSV file at", settings["address"]
            )
            df = CasesData.getCSV(settings["address"])
        elif settings["input"] == "processed":
            print(
                "Reading case data from preprocessed CSV at",
                settings["address"],
            )
            df = pd.read_csv(settings["address"], index_col=0)
        else:
            invalidInput(settings["input"])

        return df

    def getURL(url, config):
        """
        Placeholder, in case we wish to interface with an API.
        """
        max_tries = 5
        secs = 5
        for i in range(max_tries):
            try:
                print("Attempting to download...", end="", flush=True)
                response = requests.get(url)
                content = json.loads(response.content)
                df = pd.DataFrame.from_dict(content["body"])
                print("Success", flush=True)
                return df
            except (requests.ConnectionError, requests.RequestException) as e:
                print("Failed", flush=True)
                print(e)
                time.sleep(secs * 2**i)

        raise ConnectionError(
            f"Data download timed out after {max_tries} attempts"
        )

    def getCSV(file):
        """
        Format as per linelisting
        """
        dfs = pd.read_csv(
            file, chunksize=50000, iterator=True, low_memory=False
        )
        df = pd.concat(dfs)
        return df

    def adapt(df, config):
        """
        Adapt the line listing data to the desired dataframe format.
        """
        # Extract the yaml config settings
        date_low, date_high = get_date_low_high(config)
        settings = config["CasesData"]
        pillars = settings["pillars"]
        age_bins = settings["age_bins"]
        age_bands = settings["age_bands"]
        measure = settings["measure"].casefold()

        if settings["input"] == "processed":
            return df

        if settings["format"].lower() == "phe":
            df = CasesData.adapt_phe(
                df,
                date_low,
                date_high,
                pillars,
                age_bins,
                age_bands,
                measure,
            )

        return df

    def adapt_phe(
        df, date_low, date_high, pillars, age_bins, age_bands, measure
    ):
        """
        Adapt the line listing data to the desired dataframe format.
        Note that age bins and bands should match socialmixr.csv data e.g.
            age_bins = [-1, 9, 19, 29, 39, 49, 59, 69, 120]
            age_bands = ["0-9","10-19","20-29","30-39","40-49","50-59","60-69","70+"]
        """

        # Clean missing values
        df = df[
            ["specimen_date", "lab_report_date", "pillar", "age", "imd_decile"]
        ]
        df.dropna(inplace=True)

        # Clean time formats
        df["specimen_date"] = pd.to_datetime(df["specimen_date"], dayfirst=True)
        df["lab_report_date"] = pd.to_datetime(
            df["lab_report_date"], dayfirst=True
        )

        # Clean pillar values - different versions of data use different convensions!
        df["pillar"] = df["pillar"].str.replace("PILLAR1", "Pillar 1")
        df["pillar"] = df["pillar"].str.replace("PILLAR2", "Pillar 2")

        # filters for pillars, date ranges
        filters = df["pillar"].isin(pillars)
        if measure == "specimen":
            filters &= (date_low <= df["specimen_date"]) & (
                df["specimen_date"] < date_high
            )
        else:
            filters &= (date_low <= df["lab_report_date"]) & (
                df["lab_report_date"] < date_high
            )
        df = df[filters]
        df = df.drop(columns="pillar")  # No longer need pillar column

        # make 10 year age bands
        df["age_group"] = pd.cut(
            df["age"], bins=age_bins, labels=age_bands, right=True
        )
        df = df.drop(columns="age")  # No longer need age column

        # tidy column
        df = df.dropna(subset=["age_group", "imd_decile"])
        df = df.astype({"imd_decile": int})

        # Aggregate counts
        if measure == "specimen":
            df = df.groupby(
                ["age_group", "imd_decile", "specimen_date"]
            ).count()
            df = df.rename(columns={"lab_report_date": "cases"})
        else:
            df = df.groupby(
                ["age_group", "imd_decile", "lab_report_date"]
            ).count()
            df = df.rename(columns={"specimen_date": "cases"})

        df.index.names = ["age_group", "imd_decile", "time"]
        df = df.sort_index()

        # Fill in all dates, and add 0s for empty counts
        dates = pd.date_range(date_low, date_high, closed="left")
        deciles = list(range(1, 11))
        multi_indexes = pd.MultiIndex.from_product(
            [age_bands, deciles, dates],
            names=["age_group", "imd_decile", "time"],
        )
        df = df["cases"].reindex(multi_indexes, fill_value=0)
        results = df.fillna(0)  # remove nan's as line above doesn't do this!
        return results.sort_index()

    def process(config):
        df = CasesData.get(config)
        df = CasesData.adapt(df, config)
        return df
