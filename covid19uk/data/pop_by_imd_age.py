import pandas as pd
import xarray


def read_population(path):
    """Reads population CSV
    :param path: CSV file
    :returns: xarray with age_band and imd_decile coordiantes
    """
    pop = pd.read_csv(path, index_col="imd_decile")
    pop = pop.rename({"70plus": "70+"}, axis=1)

    return xarray.DataArray(
        pop,
        dims=["imd_decile", "age_group"],
        name="population_by_imd_age",
    )
