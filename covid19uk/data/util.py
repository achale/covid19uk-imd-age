"""Utility functions for COVID19 UK data"""

import os
import re
import datetime
import numpy as np
import pandas as pd


def prependDate(filename):
    now = datetime.now()  # current date and time
    date_time = now.strftime("%Y-%m-%d")
    return date_time + "_" + filename


def prependID(filename, config):
    return config["Global"]["prependID_Str"] + "_" + filename


def format_input_filename(filename, config):
    # prepend with a set string
    # to load a specific date, this should be in the string
    p, f = os.path.split(filename)
    if config["Global"]["prependID"]:
        f = prependID(f, config)
    filename = p + "/" + f
    return filename


def format_output_filename(filename, config):
    p, f = os.path.split(filename)
    if config["Global"]["prependID"]:
        f = prependID(f, config)
    if config["Global"]["prependDate"]:
        f = prependDate(f)
    filename = p + "/" + f
    return filename


def get_date_low_high(config):
    date_range = [np.datetime64(x) for x in config["date_range"]]
    return tuple(date_range)


def check_date_format(df):
    df = df.reset_index()

    if (
        not pd.to_datetime(df["date"], format="%Y-%m-%d", errors="coerce")
        .notnull()
        .all()
    ):
        raise ValueError("Invalid date format")

    return True


def check_date_bounds(df, date_low, date_high):
    if not ((date_low <= df["date"]) & (df["date"] < date_high)).all():
        raise ValueError("Date out of bounds")
    return True


def invalidInput(input):
    raise NotImplementedError(f'Input type "{input}" mode not implemented')


def imdage_idx(df):
    """create multi index"""
    return df.stack(age_imd=("age_group", "imd_decile"))


def imdage_idx_names(data):
    """create multi index"""
    ages = data.coords["age_group"].values
    imds = data.coords["imd_decile"].values
    imds = list(map(str, imds))
    age_imd_coords = [x + "_" + y for x in ages for y in imds]
    return age_imd_coords
