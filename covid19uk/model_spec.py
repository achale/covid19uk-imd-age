"""Implements the COVID SEIR model as a TFP Joint Distribution"""

import pandas as pd
import geopandas as gp
import numpy as np
import xarray
import tensorflow as tf
import tensorflow_probability as tfp

from gemlib.distributions import DiscreteTimeStateTransitionModel

from covid19uk.util import impute_previous_cases

from covid19uk.data import CasesData
from covid19uk.data import read_polymod
from covid19uk.data import read_population
from covid19uk.data.util import imdage_idx

tfd = tfp.distributions
tfd_e = tfp.experimental.distributions

DTYPE = np.float64

STOICHIOMETRY = np.array([[-1, 1, 0, 0], [0, -1, 1, 0], [0, 0, -1, 1]])
TIME_DELTA = 1.0
NU = tf.constant(0.28, dtype=DTYPE)  # E->I rate assumed known.
GAMMA0 = tf.math.log(tf.convert_to_tensor(0.25, dtype=DTYPE))
XI = tf.convert_to_tensor(0.3, dtype=DTYPE)


def gather_data(config):
    """Loads covariate data

    :param paths: a dictionary of paths to data with keys {'polymod_csv',
                  'population_csv'}
    :returns: a dictionary of covariate information to be consumed by the model
              {'C': polymod, 'N': population_size}
    """

    date_low = np.datetime64(config["date_range"][0])
    date_high = np.datetime64(config["date_range"][1])
    polymod = read_polymod(config["polymod_csv"])
    popsize = read_population(config["population_csv"])
    # imd_contacts = np.ones((10,10),  dtype=DTYPE)

    dates = pd.date_range(*config["date_range"], closed="left")

    weekday = xarray.DataArray(
        dates.weekday < 5,
        name="weekday",
        dims=["time"],
        coords=[dates.to_numpy()],
    )

    cases = CasesData.process(config).to_xarray()

    return (
        xarray.Dataset(
            dict(
                C=polymod.astype(DTYPE),
                N=popsize.astype(DTYPE),
                weekday=weekday.astype(DTYPE),
            )
        ),
        xarray.Dataset(dict(cases=cases)),
    )


def impute_censored_events(cases, ei_initial_rate=0.5, ir_initial_rate=0.25):
    """Imputes censored S->E and E->I events using geometric
       sampling algorithm in `impute_previous_cases`

    There are application-specific magic numbers hard-coded below,
    which reflect experimentation to get the right lag between EI and
    IR events, and SE and EI events respectively.  These were chosen
    by experimentation and examination of the resulting epidemic
    trajectories.

    :param cases: a MxT matrix of case numbers (I->R)
    :returns: a MxTx3 tensor of events where the first two indices of
              the right-most dimension contain the imputed event times.
    """
    ei_events, lag_ei = impute_previous_cases(cases, ir_initial_rate)
    se_events, lag_se = impute_previous_cases(ei_events, ei_initial_rate)
    ir_events = np.pad(cases, ((0, 0), (lag_ei + lag_se - 2, 0)))
    ei_events = np.pad(ei_events, ((0, 0), (lag_se - 1, 0)))
    return tf.stack([se_events, ei_events, ir_events], axis=-1)


def conditional_gp(gp, observations, new_index_points):

    param = gp.parameters
    param["observation_index_points"] = param["index_points"]
    param["observations"] = observations
    param["index_points"] = new_index_points

    return tfd.GaussianProcessRegressionModel(**param)


def CovidUK(
    covariates,
    initial_state,
    initial_step,
    num_steps,
    sim_imd_contact_rates=[[1.0]],
    t_shift=0.0,
    omega=-1.0,
    shrink_rho=-1.0,
    boost_beta=1.0,
):
    """
    :param sim_imd_contact_rates: contact rates between imd groups
                                  This param only ever has values other than one when running covid19uk.simulate_imd_switch.simulate() otherwise this param is never used/set
                                  In simulate() it is used to increase in contact rates between IMD groups

    """
    # def phi():
    #    """power in M such that M = exp(-(.-.)^2/phi)"""
    #    return tfd.Gamma(
    #        concentration=tf.constant(3.0, dtype=DTYPE),
    #        rate=tf.constant(1.0 / 10.0, dtype=DTYPE),
    #    )

    def psi():
        #    return tfd.Beta(
        #        concentration1=tf.constant(1.0, dtype=DTYPE),
        #        concentration0=tf.constant(1.0, dtype=DTYPE),
        #        force_probs_to_zero_outside_support=True,
        #    )
        return tfd.Independent(
            tfd.Beta(
                concentration1=tf.ones(shape=(8,), dtype=DTYPE),
                concentration0=tf.ones(shape=(8,), dtype=DTYPE),
                force_probs_to_zero_outside_support=True,
            ),
            reinterpreted_batch_ndims=1,
        )

    def rho():
        # return tfd.Beta(
        #    concentration1=tf.constant(1.0, dtype=DTYPE),
        #    concentration0=tf.constant(1.0, dtype=DTYPE),
        #    force_probs_to_zero_outside_support=True,
        # )
        return tfd.Independent(
            tfd.Beta(
                concentration1=tf.ones(shape=(8,), dtype=DTYPE),
                concentration0=tf.ones(shape=(8,), dtype=DTYPE),
                force_probs_to_zero_outside_support=True,
            ),
            reinterpreted_batch_ndims=1,
        )

    # def gamma0():
    #    return tfd.Normal(
    #        loc=tf.constant(0.0, dtype=DTYPE),
    #        scale=tf.constant(100.0, dtype=DTYPE),
    #    )

    def gamma1():
        return tfd.Normal(
            loc=tf.constant(0.0, dtype=DTYPE),
            scale=tf.constant(100.0, dtype=DTYPE),
        )

    def alpha_0():
        return tfd.Normal(
            loc=tf.constant(0.0, dtype=DTYPE),
            scale=tf.constant(10.0, dtype=DTYPE),
        )

    def alpha_t():
        """Time-varying force of infection"""
        return tfd.MultivariateNormalDiag(
            loc=tf.constant(0.0, dtype=DTYPE),
            scale_diag=tf.fill(
                [num_steps - 1],
                tf.constant(0.005, dtype=DTYPE),  # default 0.005
            ),
        )

    def seir(
        # phi,
        psi,
        rho,
        # gamma0,
        gamma1,
        alpha_0,
        alpha_t,
    ):
        gamma0 = GAMMA0
        xi_age = XI
        xi_imd = XI
        # phi = tf.convert_to_tensor(phi, DTYPE)
        psi = tf.convert_to_tensor(psi, DTYPE)
        rho = tf.convert_to_tensor(rho, DTYPE)
        # gamma0 = tf.convert_to_tensor(gamma0, DTYPE)
        gamma1 = tf.convert_to_tensor(gamma1, DTYPE)
        alpha_0 = tf.convert_to_tensor(alpha_0, DTYPE)
        alpha_t = tf.convert_to_tensor(alpha_t, DTYPE)

        C = tf.convert_to_tensor(covariates["C"], dtype=DTYPE)
        C = tf.transpose(
            C
        )  # is this correct in that C needs transposing? Yes, cols are be groups and rows contacts
        N = tf.convert_to_tensor(covariates["N"], dtype=DTYPE)
        IMD = tf.convert_to_tensor(covariates["IMD"], dtype=DTYPE)
        IMD = IMD - tf.reduce_mean(IMD, axis=-1)
        AGE = tf.convert_to_tensor(np.arange(C.shape[0]), dtype=DTYPE)
        AGE = AGE - tf.reduce_mean(AGE, axis=-1)

        weekday = tf.convert_to_tensor(covariates["weekday"], DTYPE)
        weekday = weekday - tf.reduce_mean(weekday, axis=-1)

        def transition_rate_fn(t, state):

            weekday_idx = tf.clip_by_value(
                tf.cast(t, tf.int64), 0, weekday.shape[0] - 1
            )
            weekday_t = tf.gather(weekday, weekday_idx)

            with tf.name_scope("Pick_alpha_t"):
                b_t = alpha_0 + tf.cumsum(alpha_t)
                alpha_t_idx = tf.cast(t, tf.int64)
                alpha_t_ = tf.where(
                    alpha_t_idx == 0,
                    alpha_0,
                    tf.gather(
                        b_t,
                        tf.clip_by_value(
                            alpha_t_idx - 1,
                            clip_value_min=0,
                            clip_value_max=alpha_t.shape[0] - 1,
                        ),
                    ),
                )

            # C_fun = C ** (2 * psi) # 87.43180111305476 normalisation constant for C
            C_fun = C  # C_fun = tf.ones_like(C)  # null model
            C_ = tf.linalg.LinearOperatorFullMatrix(C_fun)

            # M_amp = 1 + (rho / 2) * tf.math.tanh(
            #    xi * (tf.reduce_mean(IMD, axis=-1) - IMD)
            # )
            # M_amp = tf.broadcast_to(
            #    tf.expand_dims(M_amp, 1), [M_amp.shape[0], M_amp.shape[0]]
            # )  # cols tf.expand_dims(M_amp, 0) same vals - rows tf.expand_dims(M_amp, 1 ) same vals
            M_fun_i = tf.ones(
                (
                    IMD.shape[0],
                    IMD.shape[0],
                ),
                dtype=DTYPE,
            )
            time_lagged = (
                tf.cast(t, dtype=DTYPE)
                - tf.constant(t_shift, dtype=DTYPE)
                - tf.constant(10, dtype=DTYPE)
            )  # subtract further 10 days to account for lag between increase in mixing and cases
            time = tf.where(
                time_lagged < 0.0, tf.constant(0, dtype=DTYPE), time_lagged
            )
            # tf.print(t - t_shift)
            omega_idx = tf.cast(omega, tf.int64)

            ### linear increase
            M_mixing_factor = tf.where(
                omega_idx == -1,
                tf.constant(
                    sim_imd_contact_rates, dtype=DTYPE
                ),  # step function increase in mixing if sim_imd_contact_rates != [[1.0]]
                1.0
                + tf.constant(omega, dtype=DTYPE)
                * (time)
                * (
                    tf.constant(sim_imd_contact_rates, dtype=DTYPE)
                ),  # increase wrt time
            )
            # tf.print(t - t_shift, "  : ", M_mixing_factor)
            # tf.print(tf.constant(sim_imd_contact_rates, dtype=DTYPE))
            M_fun = M_fun_i * M_mixing_factor
            # tf.print("M_fun", M_fun)

            # M_fun = tf.reduce_sum(M_fun_i) * (M_fun_j / tf.reduce_sum(M_fun_j)) # rescale if needed

            # arbitrary = 5.5
            # M_dist_fn = tf.math.exp(
            #    -((IMD[:, tf.newaxis] - IMD[tf.newaxis, :] - arbitrary) ** 2)
            #    / (phi ** 2)
            # )
            # M_dist_sum = tf.expand_dims(tf.reduce_sum(M_dist_fn, axis=-1), 1)
            # M_dist = M_dist_fn / M_dist_sum
            # M_dist = psi
            # M_fun = M_amp  # M_amp * M_dist  # M_fun = tf.ones((10, 10), dtype=DTYPE)

            M_ = tf.linalg.LinearOperatorFullMatrix(M_fun)

            K = tf.linalg.LinearOperatorKronecker([C_, M_])

            # X_age_fun = 0.5 + (psi - 0.5) * tf.math.tanh(
            #    xi_age * (tf.reduce_mean(AGE, axis=-1) - AGE)
            # )
            # X_age = tf.repeat(X_age_fun, repeats=[IMD.shape[0]])
            X_age_fun = 2.0 * (1.0 + psi - tf.reduce_mean(psi))
            X_age = tf.repeat(X_age_fun, repeats=[IMD.shape[0]])

            # X_imd_fun = 0.5 + (rho - 0.5) * tf.math.tanh(
            #    xi_imd * (tf.reduce_mean(IMD, axis=-1) - IMD)
            # )
            # X_imd = tf.tile(X_imd_fun, multiples=[AGE.shape[0]])

            IMD_long = tf.tile(IMD, multiples=[AGE.shape[0]])

            ########### start 11 Oct 2022
            # shrink_rho = 0.017
            # boost_beta = 1.265
            shrink_rhos = tf.constant(shrink_rho, dtype=DTYPE)
            rhos = tf.where(
                shrink_rhos > 0.0,
                tf.clip_by_value(
                    rho
                    - shrink_rhos
                    * (
                        tf.cast(t, dtype=DTYPE)
                        - tf.constant(t_shift, dtype=DTYPE)
                    ),
                    clip_value_min=0,
                    clip_value_max=1,
                ),
                rho,
            )
            rho_long = tf.repeat(rhos, repeats=[IMD.shape[0]])
            ########### end 11 Oct 2022

            X_imd_fun = 2.0 * (
                0.5
                + (rho_long - 0.5)
                * tf.math.tanh(
                    xi_imd * (tf.reduce_mean(IMD, axis=-1) - IMD_long)
                )
            )
            X_imd = X_imd_fun  # X_imd = tf.ones_like(X_imd_fun)

            X_ = (X_imd + X_age) * tf.constant(
                boost_beta, dtype=DTYPE
            )  # X_ = tf.ones_like(X_imd_fun)  # null model
            prevalence = state[..., 2] / N
            infec_rate = (
                tf.math.exp(alpha_t_)
                * X_
                * tf.linalg.matvec(K, prevalence)
                * 1
                / N
            )

            infec_rate = infec_rate + 0.000000001  # Vector of length nc

            ei = tf.broadcast_to(
                [NU], shape=[state.shape[0]]
            )  # Vector of length nc
            ir = tf.broadcast_to(
                [tf.math.exp(gamma0 + gamma1 * weekday_t)],
                shape=[state.shape[0]],
            )  # Vector of length nc

            return [infec_rate, ei, ir]

        return DiscreteTimeStateTransitionModel(
            transition_rates=transition_rate_fn,
            stoichiometry=STOICHIOMETRY,
            initial_state=initial_state,
            initial_step=initial_step,
            time_delta=TIME_DELTA,
            num_steps=num_steps,
        )

    return tfd.JointDistributionNamed(
        dict(
            # phi=phi,
            psi=psi,
            rho=rho,
            # gamma0=gamma0,
            gamma1=gamma1,
            alpha_0=alpha_0,
            alpha_t=alpha_t,
            seir=seir,
        )
    )


def next_generation_matrix_fn(
    covar_data,
    param,
    sim_imd_contact_rates=[[1.0]],
    t_shift=0.0,
    omega=-1.0,
):
    """The next generation matrix calculates the force of infection from
    individuals in metapopulation i to all other metapopulations j during
    a typical infectious period (1/gamma).
    :param covar_data: a dictionary of covariate data
    :param param: a dictionary of parameters
    :param sim_imd_contact_rates: contact rates between imd groups - this param is never used when calling next_generation_matrix_fn and is only included here for completeness
    :returns: a function taking arguments `t` and `state` giving the time and
              epidemic state (SEIR) for which the NGM is to be calculated.  This
              function in turn returns an MxM next generation matrix.
    """

    def fn(t, state):
        gamma0 = GAMMA0
        # gamma0 = param["gamma0"]
        xi_age = XI
        xi_imd = XI

        C = tf.convert_to_tensor(covar_data["C"], dtype=DTYPE)
        C = tf.transpose(C)
        Nx = imdage_idx(covar_data["N"])
        N = tf.convert_to_tensor(Nx, dtype=DTYPE)
        IMD = tf.convert_to_tensor(
            covar_data.coords["imd_decile"].astype(float), dtype=DTYPE
        )
        IMD = IMD - tf.reduce_mean(IMD, axis=-1)
        AGE = tf.convert_to_tensor(np.arange(C.shape[0]), dtype=DTYPE)
        AGE = AGE - tf.reduce_mean(AGE, axis=-1)

        b_t = param["alpha_0"] + tf.cumsum(param["alpha_t"])
        alpha_t_ = tf.where(
            t == 0,
            param["alpha_0"],
            tf.gather(
                b_t,
                tf.clip_by_value(
                    t,
                    clip_value_min=0,
                    clip_value_max=param["alpha_t"].shape[-1] - 1,
                ),
            ),
        )

        C_fun = C  # C_fun = tf.ones_like(C)  # null model
        C_ = tf.linalg.LinearOperatorFullMatrix(C_fun)

        # M_amp = 1 + (param["rho"] / 2) * tf.math.tanh(
        #    xi * (tf.reduce_mean(IMD, axis=-1) - IMD)
        # )
        # M_amp = tf.broadcast_to(
        #    tf.expand_dims(M_amp, 1), [M_amp.shape[0], M_amp.shape[0]]
        # )
        # M_fun = M_amp
        M_fun_i = tf.ones(
            (
                IMD.shape[0],
                IMD.shape[0],
            ),
            dtype=DTYPE,
        )
        time_lagged = (
            tf.cast(t, dtype=DTYPE)
            - tf.constant(t_shift, dtype=DTYPE)
            - tf.constant(10, dtype=DTYPE)
        )  # subtract further 10 days to account for lag between increase in mixing and cases
        time = tf.where(
            time_lagged < 0.0, tf.constant(0, dtype=DTYPE), time_lagged
        )
        omega_idx = tf.cast(omega, tf.int64)
        M_mixing_factor = tf.where(
            omega_idx == -1,
            tf.constant(
                sim_imd_contact_rates, dtype=DTYPE
            ),  # step function increase in mixing if not equal to [[1.0]]
            1.0
            + tf.constant(omega, dtype=DTYPE)
            * (time)
            * (
                tf.constant(sim_imd_contact_rates, dtype=DTYPE)
            ),  # increase wrt time
        )

        M_fun = M_fun_i * M_mixing_factor
        # M_fun = tf.reduce_sum(M_fun_i) * (M_fun_j / tf.reduce_sum(M_fun_j)) # rescale if needed
        M_ = tf.linalg.LinearOperatorFullMatrix(M_fun)

        K = tf.linalg.LinearOperatorKronecker([C_, M_])

        # X_age_fun = 0.5 + (param["psi"] - 0.5) * tf.math.tanh(
        #    xi_age * (tf.reduce_mean(AGE, axis=-1) - AGE)
        # )
        # X_age = tf.repeat(X_age_fun, repeats=[IMD.shape[0]])
        X_age_fun = 2.0 * (1.0 + param["psi"] - tf.reduce_mean(param["psi"]))
        X_age = tf.repeat(X_age_fun, repeats=[IMD.shape[0]])

        # X_imd_fun = 0.5 + (param["rho"] - 0.5) * tf.math.tanh(
        #    xi_imd * (tf.reduce_mean(IMD, axis=-1) - IMD)
        # )
        # X_imd = tf.tile(X_imd_fun, multiples=[AGE.shape[0]])

        IMD_long = tf.tile(IMD, multiples=[AGE.shape[0]])
        rho_long = tf.repeat(param["rho"], repeats=[IMD.shape[0]])
        X_imd_fun = 2.0 * (
            0.5
            + (rho_long - 0.5)
            * tf.math.tanh(xi_imd * (tf.reduce_mean(IMD, axis=-1) - IMD_long))
        )
        X_imd = X_imd_fun  # X_imd = tf.ones_like(X_imd_fun)

        X_ = X_imd + X_age  # X_ = tf.ones_like(X_imd_fun)  # null model

        ### orig by Ali
        # prevalence = (
        #    tf.eye(state[..., 2].shape[0], dtype=state.dtype) / N[tf.newaxis, :]
        # )
        # infec_rate = (
        #    tf.math.exp(alpha_t_)
        #    * X_
        #    * tf.linalg.matmul(
        #        K, prevalence
        #    )  # materialises K/N due to indentity matrix in prevalence
        #    * 1
        #    / N[:, tf.newaxis]
        # )
        ### update after conversation with Chris however this is mathematically equal to 'orig by Ali' provided N and state[...,2] both have shape (anything, )
        prevalence = (
            tf.ones(state[..., 2].shape[0], dtype=state.dtype)
            / N[tf.newaxis, :]  # Nj's
        )
        infec_rate = (
            tf.math.exp(alpha_t_)
            * X_
            * tf.linalg.matmul(
                tf.eye(C_fun.shape[0] * M_fun.shape[0], dtype=DTYPE), K
            )  # materialises Kij's
            * prevalence
            * 1
            / N[:, tf.newaxis]  # Ni's
        )
        infec_prob = 1.0 - tf.math.exp(-infec_rate)

        expected_new_infec = infec_prob * state[..., 0][..., tf.newaxis]
        expected_infec_period = 1.0 / (1.0 - tf.math.exp(-tf.math.exp(gamma0)))
        # expected_infec_period = 1.0 / (1.0 - tf.math.exp(-tf.math.exp(param["gamma0"])))
        ngm = expected_new_infec * expected_infec_period
        return ngm

    return fn


def chi_matrix(imd, age, param):
    """The next generation matrix calculates the force of infection from
    individuals in metapopulation i to all other metapopulations j during
    a typical infectious period (1/gamma).
    :param covar_data: a dictionary of covariate data
    :param param: a dictionary of parameters
    :returns: a function taking arguments `t` and `state` giving the time and
              epidemic state (SEIR) for which the NGM is to be calculated.  This
              function in turn returns an MxM next generation matrix.
    """

    xi_imd = XI

    IMD = tf.convert_to_tensor(imd.astype(float), dtype=DTYPE)
    IMD = IMD - tf.reduce_mean(IMD, axis=-1)

    X_age_fun = 2.0 * (1.0 + param["psi"] - tf.reduce_mean(param["psi"]))
    X_age = tf.repeat(X_age_fun, repeats=[IMD.shape[0]])

    IMD_long = tf.tile(IMD, multiples=[age.shape[0]])
    rho_long = tf.repeat(param["rho"], repeats=[IMD.shape[0]])
    X_imd_fun = 2.0 * (
        0.5
        + (rho_long - 0.5)
        * tf.math.tanh(xi_imd * (tf.reduce_mean(IMD, axis=-1) - IMD_long))
    )
    X_imd = X_imd_fun  # X_imd = tf.ones_like(X_imd_fun)  # null model

    X_ = X_imd + X_age  # X_ = tf.ones_like(X_imd_fun)  # null model

    return X_
