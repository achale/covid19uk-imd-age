"""MCMC Test Rig for COVID-19 UK model"""
# pylint: disable=E402

import sys

import h5py
import xarray
import tqdm
import yaml
import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp

from tensorflow_probability.python.internal import unnest
from tensorflow_probability.python.internal import dtype_util
from tensorflow_probability.python.experimental.stats import sample_stats

from gemlib.util import compute_state
from gemlib.mcmc import Posterior
from gemlib.mcmc import GibbsKernel

from covid19uk.inference.mcmc_kernel_factory import make_hmc_base_kernel
from covid19uk.inference.mcmc_kernel_factory import make_hmc_fast_adapt_kernel
from covid19uk.inference.mcmc_kernel_factory import make_hmc_slow_adapt_kernel
from covid19uk.inference.mcmc_kernel_factory import (
    make_event_multiscan_gibbs_step,
)
from covid19uk.data.util import imdage_idx

import covid19uk.model_spec as model_spec

tfd = tfp.distributions
tfb = tfp.bijectors
DTYPE = model_spec.DTYPE


def get_weighted_running_variance(draws):
    """Initialises online variance accumulator"""

    prev_mean, prev_var = tf.nn.moments(draws[-draws.shape[0] // 2 :], axes=[0])
    num_samples = tf.cast(
        draws.shape[0] / 2,
        dtype=dtype_util.common_dtype([prev_mean, prev_var], tf.float32),
    )
    weighted_running_variance = sample_stats.RunningVariance.from_stats(
        num_samples=num_samples, mean=prev_mean, variance=prev_var
    )
    return weighted_running_variance


def _get_window_sizes(num_adaptation_steps):
    slow_window_size = num_adaptation_steps // 21
    first_window_size = 3 * slow_window_size
    last_window_size = (
        num_adaptation_steps - 15 * slow_window_size - first_window_size
    )
    return first_window_size, slow_window_size, last_window_size


@tf.function
def _fast_adapt_window(
    num_draws,
    joint_log_prob_fn,
    initial_position,
    hmc_kernel_kwargs,
    dual_averaging_kwargs,
    event_kernel_kwargs,
    trace_fn=None,
    seed=None,
):
    """
    In the fast adaptation window, we use the
    `DualAveragingStepSizeAdaptation` kernel
    to wrap an HMC kernel.

    :param num_draws: Number of MCMC draws in window
    :param joint_log_prob_fn: joint log posterior function
    :param initial_position: initial state of the Markov chain
    :param hmc_kernel_kwargs: `HamiltonianMonteCarlo` kernel keywords args
    :param dual_averaging_kwargs: `DualAveragingStepSizeAdaptation` keyword args
    :param event_kernel_kwargs: EventTimesMH and Occult kernel args
    :param trace_fn: function to trace kernel results
    :param seed: optional random seed.
    :returns: draws, kernel results, the adapted HMC step size, and variance
              accumulator
    """
    kernel_list = [
        (
            0,
            make_hmc_fast_adapt_kernel(
                hmc_kernel_kwargs=hmc_kernel_kwargs,
                dual_averaging_kwargs=dual_averaging_kwargs,
            ),
        ),
        (1, make_event_multiscan_gibbs_step(**event_kernel_kwargs)),
    ]

    kernel = GibbsKernel(
        target_log_prob_fn=joint_log_prob_fn,
        kernel_list=kernel_list,
        name="fast_adapt",
    )

    pkr = kernel.bootstrap_results(initial_position)

    @tf.function(jit_compile=True)
    def sample(current_state, previous_kernel_results):
        return tfp.mcmc.sample_chain(
            num_draws,
            current_state=current_state,
            kernel=kernel,
            previous_kernel_results=previous_kernel_results,
            return_final_kernel_results=True,
            trace_fn=trace_fn,
            seed=seed,
        )

    draws, trace, fkr = sample(initial_position, pkr)

    weighted_running_variance = get_weighted_running_variance(draws[0])
    step_size = unnest.get_outermost(fkr.inner_results[0], "step_size")
    return draws, trace, step_size, weighted_running_variance


@tf.function
def _slow_adapt_window(
    num_draws,
    joint_log_prob_fn,
    initial_position,
    initial_running_variance,
    hmc_kernel_kwargs,
    dual_averaging_kwargs,
    event_kernel_kwargs,
    trace_fn=None,
    seed=None,
):
    """In the slow adaptation phase, we adapt the HMC
    step size and mass matrix together.

    :param num_draws: number of MCMC iterations
    :param joint_log_prob_fn: the joint posterior density function
    :param initial_position: initial Markov chain state
    :param initial_running_variance: initial variance accumulator
    :param hmc_kernel_kwargs: `HamiltonianMonteCarlo` kernel kwargs
    :param dual_averaging_kwargs: `DualAveragingStepSizeAdaptation` kwargs
    :param event_kernel_kwargs: EventTimesMH and Occults kwargs
    :param trace_fn: result trace function
    :param seed: optional random seed
    :returns: draws, kernel results, adapted step size, the variance accumulator,
              and "learned" momentum distribution for the HMC.
    """
    kernel_list = [
        (
            0,
            make_hmc_slow_adapt_kernel(
                initial_running_variance,
                hmc_kernel_kwargs,
                dual_averaging_kwargs,
            ),
        ),
        (1, make_event_multiscan_gibbs_step(**event_kernel_kwargs)),
    ]

    kernel = GibbsKernel(
        target_log_prob_fn=joint_log_prob_fn,
        kernel_list=kernel_list,
        name="slow_adapt",
    )

    pkr = kernel.bootstrap_results(initial_position)

    @tf.function(jit_compile=True)
    def sample(current_state, previous_kernel_results):
        return tfp.mcmc.sample_chain(
            num_draws,
            current_state=current_state,
            kernel=kernel,
            previous_kernel_results=pkr,
            return_final_kernel_results=True,
            trace_fn=trace_fn,
        )

    draws, trace, fkr = sample(initial_position, pkr)
    step_size = unnest.get_outermost(fkr.inner_results[0], "step_size")
    momentum_distribution = unnest.get_outermost(
        fkr.inner_results[0], "momentum_distribution"
    )

    weighted_running_variance = get_weighted_running_variance(draws[0])

    return (
        draws,
        trace,
        step_size,
        weighted_running_variance,
        momentum_distribution,
    )


def make_fixed_window_sampler(
    num_draws,
    joint_log_prob_fn,
    hmc_kernel_kwargs,
    event_kernel_kwargs,
    trace_fn=None,
    seed=None,
    jit_compile=False,
):
    """Fixed step size and mass matrix HMC.

    :param num_draws: number of MCMC iterations
    :param joint_log_prob_fn: joint log posterior density function
    :param initial_position: initial Markov chain state
    :param hmc_kernel_kwargs: `HamiltonianMonteCarlo` kwargs
    :param event_kernel_kwargs: Event and Occults kwargs
    :param trace_fn: results trace function
    :param seed: optional random seed
    :returns: (draws, trace, final_kernel_results)
    """
    kernel_list = [
        (0, make_hmc_base_kernel(**hmc_kernel_kwargs)),
        (1, make_event_multiscan_gibbs_step(**event_kernel_kwargs)),
    ]

    kernel = GibbsKernel(
        target_log_prob_fn=joint_log_prob_fn,
        kernel_list=kernel_list,
        name="fixed",
    )

    @tf.function(jit_compile=jit_compile)
    def sample_fn(current_state, previous_kernel_results=None):
        return tfp.mcmc.sample_chain(
            num_draws,
            current_state=current_state,
            kernel=kernel,
            return_final_kernel_results=True,
            previous_kernel_results=previous_kernel_results,
            trace_fn=trace_fn,
            seed=seed,
        )

    return sample_fn, kernel


def trace_results_fn(_, results):
    """Packs results into a dictionary"""
    results_dict = {}
    root_results = results.inner_results

    step_size = tf.convert_to_tensor(
        unnest.get_outermost(root_results[0], "step_size")
    )

    results_dict["hmc"] = {
        "is_accepted": unnest.get_innermost(root_results[0], "is_accepted"),
        "target_log_prob": unnest.get_innermost(
            root_results[0], "target_log_prob"
        ),
        "step_size": step_size,
    }

    def get_move_results(results):
        return {
            "is_accepted": results.is_accepted,
            "target_log_prob": results.accepted_results.target_log_prob,
            "proposed_delta": tf.stack(
                [
                    results.accepted_results.m,
                    results.accepted_results.t,
                    results.accepted_results.delta_t,
                    results.accepted_results.x_star,
                ]
            ),
        }

    res1 = root_results[1].inner_results
    results_dict["move/S->E"] = get_move_results(res1[0])
    results_dict["move/E->I"] = get_move_results(res1[1])
    results_dict["occult/S->E"] = get_move_results(res1[2])
    results_dict["occult/E->I"] = get_move_results(res1[3])

    return results_dict


def draws_to_dict(draws):
    num_locs = draws[1].shape[1]
    num_times = draws[1].shape[2]
    # return {
    #    "phi": draws[0][:, 0],
    #    "psi": draws[0][:, 1],
    #    "rho": draws[0][:, 2],
    #    "gamma0": draws[0][:, 3],
    #    "gamma1": draws[0][:, 4],
    #    "alpha_0": draws[0][:, 5],
    #    "alpha_t": draws[0][:, 6 : (6 + num_times - 1)],
    #    "seir": draws[1],
    # }
    return {
        "psi": draws[0][:, 0:8],
        "rho": draws[0][:, 8:16],
        "gamma1": draws[0][:, 16],
        "alpha_0": draws[0][:, 17],
        "alpha_t": draws[0][:, 18 : (18 + num_times - 1)],
        "seir": draws[1],
    }


def run_mcmc(
    joint_log_prob_fn,
    current_state,
    param_bijector,
    initial_conditions,
    config,
    output_file,
):

    first_window_size = 200
    last_window_size = 50
    slow_window_size = 25
    num_slow_windows = 8  # default=6

    warmup_size = int(
        first_window_size
        + slow_window_size
        * ((1 - 2**num_slow_windows) / (1 - 2))  # sum geometric series
        + last_window_size
    )

    hmc_kernel_kwargs = {
        "step_size": 0.5,  # default=0.1
        "num_leapfrog_steps": 16,
        "momentum_distribution": None,
        "store_parameters_in_results": True,
    }
    dual_averaging_kwargs = {
        "target_accept_prob": 0.75,  # default=0.75 lowering this lowers theta acceptance
        # "decay_rate": 0.80,
    }
    event_kernel_kwargs = {
        "initial_state": initial_conditions,
        "t_range": [
            current_state[1].shape[-2] - 21,
            current_state[1].shape[-2],
        ],
        "config": config,
    }

    # Set up posterior
    print("Initialising output...", end="", flush=True, file=sys.stderr)
    draws, trace, _ = make_fixed_window_sampler(
        num_draws=1,
        joint_log_prob_fn=joint_log_prob_fn,
        hmc_kernel_kwargs=hmc_kernel_kwargs,
        event_kernel_kwargs=event_kernel_kwargs,
        trace_fn=trace_results_fn,
    )[0](current_state)
    posterior = Posterior(
        output_file,
        sample_dict=draws_to_dict(draws),
        results_dict=trace,
        num_samples=warmup_size
        + config["num_burst_samples"] * config["num_bursts"],
    )
    offset = 0
    print("Done", flush=True, file=sys.stderr)

    # Fast adaptation sampling
    print(f"Fast window {first_window_size}", file=sys.stderr, flush=True)
    dual_averaging_kwargs["num_adaptation_steps"] = first_window_size
    draws, trace, step_size, running_variance = _fast_adapt_window(
        num_draws=first_window_size,
        joint_log_prob_fn=joint_log_prob_fn,
        initial_position=current_state,
        hmc_kernel_kwargs=hmc_kernel_kwargs,
        dual_averaging_kwargs=dual_averaging_kwargs,
        event_kernel_kwargs=event_kernel_kwargs,
        trace_fn=trace_results_fn,
    )
    current_state = [s[-1] for s in draws]
    draws[0] = param_bijector.inverse(draws[0])
    posterior.write_samples(
        draws_to_dict(draws),
        first_dim_offset=offset,
    )
    posterior.write_results(trace, first_dim_offset=offset)
    offset += first_window_size

    # Slow adaptation sampling
    hmc_kernel_kwargs["step_size"] = step_size
    for slow_window_idx in range(num_slow_windows):
        window_num_draws = slow_window_size * (2**slow_window_idx)
        dual_averaging_kwargs["num_adaptation_steps"] = window_num_draws
        print(f"Slow window {window_num_draws}", file=sys.stderr, flush=True)
        (
            draws,
            trace,
            step_size,
            running_variance,
            momentum_distribution,
        ) = _slow_adapt_window(
            num_draws=window_num_draws,
            joint_log_prob_fn=joint_log_prob_fn,
            initial_position=current_state,
            initial_running_variance=running_variance,
            hmc_kernel_kwargs=hmc_kernel_kwargs,
            dual_averaging_kwargs=dual_averaging_kwargs,
            event_kernel_kwargs=event_kernel_kwargs,
            trace_fn=trace_results_fn,
        )
        hmc_kernel_kwargs["step_size"] = step_size
        hmc_kernel_kwargs["momentum_distribution"] = momentum_distribution
        current_state = [s[-1] for s in draws]
        draws[0] = param_bijector.inverse(draws[0])
        posterior.write_samples(
            draws_to_dict(draws),
            first_dim_offset=offset,
        )
        posterior.write_results(trace, first_dim_offset=offset)
        offset += window_num_draws

    # Fast adaptation sampling
    print(f"Fast window {last_window_size}", file=sys.stderr, flush=True)
    dual_averaging_kwargs["num_adaptation_steps"] = last_window_size
    draws, trace, step_size, _ = _fast_adapt_window(
        num_draws=last_window_size,
        joint_log_prob_fn=joint_log_prob_fn,
        initial_position=current_state,
        hmc_kernel_kwargs=hmc_kernel_kwargs,
        dual_averaging_kwargs=dual_averaging_kwargs,
        event_kernel_kwargs=event_kernel_kwargs,
        trace_fn=trace_results_fn,
    )
    current_state = [s[-1] for s in draws]
    draws[0] = param_bijector.inverse(draws[0])
    posterior.write_samples(
        draws_to_dict(draws),
        first_dim_offset=offset,
    )
    posterior.write_results(trace, first_dim_offset=offset)
    offset += last_window_size

    # Fixed window sampling
    print("Sampling...", file=sys.stderr, flush=True)
    hmc_kernel_kwargs["step_size"] = tf.reduce_mean(
        trace["hmc"]["step_size"][-last_window_size // 2 :]
    )

    fixed_sample, kernel = make_fixed_window_sampler(
        config["num_burst_samples"],
        joint_log_prob_fn=joint_log_prob_fn,
        hmc_kernel_kwargs=hmc_kernel_kwargs,
        event_kernel_kwargs=event_kernel_kwargs,
        trace_fn=trace_results_fn,
        jit_compile=True,
    )
    pkr = kernel.bootstrap_results(current_state)

    for i in tqdm.tqdm(
        range(config["num_bursts"]),
        unit_scale=config["num_burst_samples"] * config["thin"],
    ):
        draws, trace, pkr = fixed_sample(current_state, pkr)
        current_state = [state_part[-1] for state_part in draws]
        draws[0] = param_bijector.inverse(draws[0])
        posterior.write_samples(
            draws_to_dict(draws),
            first_dim_offset=offset,
        )
        posterior.write_results(
            trace,
            first_dim_offset=offset,
        )
        offset += config["num_burst_samples"]

    return posterior


def mcmc(data_file, output_file, config, use_autograph=False, use_xla=True):
    """Constructs and runs the MCMC"""

    if tf.test.gpu_device_name():
        print("Using GPU")
    else:
        print("Using CPU")

    data = xarray.open_dataset(data_file, group="constant_data")
    cases = xarray.open_dataset(data_file, group="observations")[
        "cases"
    ].astype(DTYPE)
    dates = cases.coords["time"]

    N = imdage_idx(data["N"])
    cases = imdage_idx(cases).transpose()

    # Impute censored events, return cases
    # Take the last week of data, and repeat a further 3 times
    # to get a better occult initialisation.
    extra_cases = tf.tile(cases[:, -7:], [1, 3])
    cases = tf.concat([cases, extra_cases], axis=-1)
    events = model_spec.impute_censored_events(
        cases, config["initial_rates"]["ei"], config["initial_rates"]["ir"]
    ).numpy()

    # Initial conditions are calculated by calculating the state
    # at the beginning of the inference period
    #
    # Imputed censored events that pre-date the first I-R events
    # in the cases dataset are discarded.  They are only used to
    # to set up a sensible initial state.
    state = compute_state(
        initial_state=tf.concat(
            [
                tf.constant(N, DTYPE)[:, tf.newaxis],
                tf.zeros_like(events[:, 0, :]),
            ],
            axis=-1,
        ),
        events=events,
        stoichiometry=model_spec.STOICHIOMETRY,
    )
    start_time = state.shape[1] - cases.shape[1]
    initial_state = state[:, start_time, :]
    events = events[:, start_time:-21, :]  # Clip off the "extra" events
    print("initial_state", initial_state.shape)
    print("events", events.shape)
    print("num_steps ", events.shape[1])
    print('data["C"] ', data["C"])
    print("N ", N)
    print('data["weekday"]', data["weekday"])
    print('data.coords["imd_decile"]', data.coords["imd_decile"].astype(float))

    ########################################################
    # Construct the MCMC kernels #
    ########################################################
    model = model_spec.CovidUK(
        covariates={
            "C": data["C"],
            "N": N,
            "weekday": data["weekday"],
            "IMD": data.coords["imd_decile"].astype(float),
        },
        initial_state=initial_state,
        initial_step=0,
        num_steps=events.shape[1],
    )

    param_bij = tfb.Invert(  # Forward transform unconstrains params
        tfb.Blockwise(
            [
                # tfb.Softplus(low=dtype_util.eps(DTYPE)),  # phi
                tfb.Sigmoid(),  # psi, rho
                tfb.Identity(),  # gamma0, gamma1, alpha_0
                tfb.Identity(),  # alpha_t
            ],
            # block_sizes=[1, 2, 3, events.shape[1] - 1],
            block_sizes=[16, 2, events.shape[1] - 1],
        )
    )

    def joint_log_prob(unconstrained_params, events):
        params = param_bij.inverse(unconstrained_params)
        return model.log_prob(
            # dict(
            #    phi=params[0],
            #    psi=params[1],
            #    rho=params[2],
            #    gamma0=params[3],
            #    gamma1=params[4],
            #    alpha_0=params[5],
            #    alpha_t=params[6 : (6 + events.shape[1] - 1)],
            #    seir=events,
            # )
            dict(
                psi=params[0:8],
                rho=params[8:16],
                gamma1=params[16],
                alpha_0=params[17],
                alpha_t=params[18 : (18 + events.shape[1] - 1)],
                seir=events,
            )
        ) + param_bij.inverse_log_det_jacobian(
            unconstrained_params, event_ndims=1
        )

    # MCMC tracing functions
    ###############################
    # Construct bursted MCMC loop #
    ###############################
    current_chain_state = [  # unconstrained space
        tf.concat(
            [
                # np.array([1.0], dtype=DTYPE),  # phi
                np.array([-1.1] * 8, dtype=DTYPE),  # psi
                np.array([-1.1] * 8, dtype=DTYPE),  # rho
                # np.array([0.0], dtype=DTYPE),  # gamma0
                np.array([0.32], dtype=DTYPE),  # gamma1
                np.array([7.5], dtype=DTYPE),  # alpha_0
                np.array([0.0] * (events.shape[1] - 1), dtype=DTYPE),  # alpha_t
            ],
            axis=0,
        ),
        events,
    ]
    print("events.shape[1] - 1 = ", events.shape[1] - 1)
    print("current_chain_state[0]", current_chain_state[0].shape)
    print("current_chain_state[1]", current_chain_state[1].shape)
    print("Initial logpi:", joint_log_prob(*current_chain_state), flush=True)

    # Output file
    posterior = run_mcmc(
        joint_log_prob_fn=joint_log_prob,
        current_state=current_chain_state,
        param_bijector=param_bij,
        initial_conditions=initial_state,
        config=config,
        output_file=output_file,
    )
    posterior._file.create_dataset("initial_state", data=initial_state)
    posterior._file.create_dataset(
        "time",
        data=np.array(dates).astype(str).astype(h5py.string_dtype()),
    )

    print(f"Acceptance theta: {posterior['results/hmc/is_accepted'][:].mean()}")
    print(
        f"Acceptance move S->E: {posterior['results/move/S->E/is_accepted'][:].mean()}"
    )
    print(
        f"Acceptance move E->I: {posterior['results/move/E->I/is_accepted'][:].mean()}"
    )
    print(
        f"Acceptance occult S->E: {posterior['results/occult/S->E/is_accepted'][:].mean()}"
    )
    print(
        f"Acceptance occult E->I: {posterior['results/occult/E->I/is_accepted'][:].mean()}"
    )

    del posterior


if __name__ == "__main__":

    from argparse import ArgumentParser

    parser = ArgumentParser(description="Run MCMC inference algorithm")
    parser.add_argument(
        "-c", "--config", type=str, help="Config file", required=True
    )
    parser.add_argument(
        "-o", "--output", type=str, help="Output file", required=True
    )
    parser.add_argument("data_file", type=str, help="Data NetCDF file")

    args = parser.parse_args()

    with open(args.config, "r") as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    mcmc(args.data_file, args.output, config["Mcmc"])
