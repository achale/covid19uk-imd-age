# code used to create socialmixr.csv

library('socialmixr')

x = contact_matrix(polymod, countries = "United Kingdom", age.limits = c(0, 10, 20, 30, 40, 50, 60, 70, 120))

print(x)

rownames(x$matrix) = gsub(',','-',rownames(x$matrix))

colnames(x$matrix) = paste0('contact_age_group_', colnames(x$matrix))  # columns are contact age group
colnames(x$matrix) = gsub(',','-',colnames(x$matrix))

write.csv(x$matrix, 'socialmixr.csv', row.names = TRUE)
