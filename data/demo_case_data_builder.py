# Instructions of how to build a demo data file

import numpy as np
import pandas as pd
import tensorflow_probability as tfp
import xarray

import covid19uk.model_spec as model_spec

DTYPE = model_spec.DTYPE


"""
1) With demo_config.yaml pointing towards a raw data file where file name is defined in line CaseData: ...  address: ...
   and also in demo_config.yaml set date range to 2 weeks e.g. date_range: 2010-01-01 - 2010-02-01
   Remove existing file results/demo_data_keep/inferencedata.nc

poetry run python -m covid19uk.data.assemble \
    demo_config.yaml \
    results/demo_data_keep/inferencedata.nc

this outputs a raw data file to results/demo_data_keep/inferencedata.nc
"""

"""
2) run the code below e.g. poetry run python -m data.demo_case_data_builder
"""
# constant_data = xarray.open_dataset(
#    "results/demo_data_keep/inferencedata.nc", group="constant_data"
# )
cases = xarray.open_dataset(
    "results/demo_data_keep/inferencedata.nc", group="observations"
)["cases"].astype(DTYPE)

new_dates = pd.date_range(
    start="2022-08-01", end="2022-09-01", closed="left"
)  # fake dates
cases = cases.assign_coords({"time": new_dates})

# add perturbation to case counts
for i in new_dates:
    slice = cases.sel(time=i)
    dist = tfp.distributions.Binomial(
        slice,
        probs=0.55,
    )
    perturbation = dist.sample().numpy()
    cases.loc[dict(time=i)] = slice - perturbation

df = cases.to_dataframe().reset_index()
df["pillar"] = "Pillar 2"
df = df.rename({"time": "specimen_date"}, axis="columns")
df["lab_report_date"] = df["specimen_date"]
df["cases"] = df["cases"].apply(np.int64)

df.loc[df["age_group"] == "70+", "age_group"] = "70-79"
df["age"] = df["age_group"].str.split("-").str[1]
df = df.drop("age_group", axis=1)
df = df.astype({"age": int})
df["age"] = df["age"].clip(lower=0)  # just to be sure!

# print(df)

df = df[df["cases"] != 0].reset_index(drop=True)

# print(df)

df_long = df.loc[df.index.repeat(df.cases)].reset_index(drop=True)
df_long = df_long.drop(["cases"], axis=1)
df_long.to_csv("data/demo_case_data.csv")


"""
3) update demo_config.yaml with new date range as given by variable new_dates above
4) to prepare data for mcmc see README.md file e.g. poetry run python -m covid19uk.data.assemble demo_config.yaml demo_results/inferencedata.nc
"""
